from django.db import models

# Create your models here.
from django.db import models

# Create your models here.

from django.db import models
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.utils import unquote


class ActionLog(LogEntry):
    request_url = models.CharField(max_length=256, null=False, blank=False)
    request_ip = models.CharField(max_length=20, null=False, blank=False)

    @classmethod
    def get_action_logs(cls, model=None, obj_id=None):
        filters = {}
        if model:
            filters["content_type_id"] = ContentType.objects.get_for_model(model).id
        if obj_id:
            filters["object_id"] = unquote(obj_id)
        return list(cls.objects.filter(**filters).select_related().order_by('action_time'))
