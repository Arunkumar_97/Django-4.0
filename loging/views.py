from . import models
from ..core.base.response import ApiResponse
from ..core.base.views import JSONApi
from ..core.utils.date_time import get_date_time


# Create your views here.

class ActionLogsView(JSONApi):

    @JSONApi.api
    def post(self, request):
        data = request.data
        filters = {}
        if "from_date" in data and data["from_date"]:
            filters["action_time__gte"] = get_date_time(data["from_date"])
        if "to_date" in data and data["to_date"]:
            filters["action_time__lte"] = get_date_time(data["to_date"], False)
        if "content_type_ids" in data and len(data["content_type_ids"]):
            filters["content_type_id__in"] = data["content_type_ids"]
        if "action_types" in data and len(data["action_types"]):
            filters["action_flag__in"] = data["action_types"]
        if "users" in data and len(data["users"]):
            filters["user_id__in"] = data["users"]

        return ApiResponse(data={
            "logs": list(models.LogEntry.objects.filter(**filters)
                         .values("user__username", "action_time", "content_type__model", "content_type__app_label",
                                 "object_repr", "action_flag", "change_message", "actionlog__request_url",
                                 "actionlog__request_ip"))
        })
