from django.db import models
from django.utils.translation import gettext_lazy as _


class Servers(models.TextChoices):
    MW = 'MW', _('minsterwhite')


class Modules(models.TextChoices):
    STOCK = ' STOCK', _('stock')

class MasterSyncHistory(models.Model):
    server = models.CharField(choices=Servers.choices, max_length=20)
    module = models.CharField(choices=Modules.choices, max_length=20)
    last_synced_time = models.DateTimeField()
