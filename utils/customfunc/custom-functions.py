from import_export.admin import ExportMixin


class CustomExportMixin(ExportMixin):
    def has_export_permission(self, request):
        if request.user.groups.filter(name="File Export").exists():
            return True
        return False