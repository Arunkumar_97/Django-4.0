import time
from abc import abstractmethod, ABCMeta

from core.models import CustomUser
from master.models import AttributeKey, AttributeValue, Item, ItemAudit
from inventory.models import Shelf, Batch, SerialNumber, SerialNumberAudit, VirtualShelf, StockPoint, Warehouse, Bay, \
    Stock, StockAudit
from django.http import JsonResponse
import datetime
import pytz

utc = pytz.UTC
import logging

logger = logging.getLogger(__name__)
class SyncInterface(metaclass=ABCMeta):
    def __init__(self, raw_data: list, is_first_batch, request):
        self.raw_data = raw_data
        self.request = request
        self.is_first_batch = is_first_batch
        self.processed_data = list()
        self.count = 0
        self.line = 0
        self.exceptions = list()

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'pre_process') and callable(subclass.pre_process) and
                hasattr(subclass, 'process') and callable(subclass.post_process) and
                hasattr(subclass, 'post_process') and callable(subclass.process) and
                hasattr(subclass, 'sync') and callable(subclass.sync) and
                hasattr(subclass, 'force_sync') and callable(subclass.force_sync) and
                hasattr(subclass, 'partial_sync') and callable(subclass.partial_sync))

    @abstractmethod
    def pre_process(self):
        pass

    @abstractmethod
    def process(self):
        pass

    @abstractmethod
    def post_process(self):
        pass

    @abstractmethod
    def force_sync(self):
        pass

    @abstractmethod
    def partial_sync(self):
        pass

    @abstractmethod
    def sync(self):
        pass


class ItemSyncInterface(SyncInterface):
    def __init__(self, raw_data: list, is_first_batch, request):
        super(ItemSyncInterface, self).__init__(raw_data=raw_data, is_first_batch=is_first_batch, request=request)

    def pre_process(self):
        return super(ItemSyncInterface, self).pre_process()

    def process(self):
        self.pre_process()
        for item in self.raw_data:
            self.line += 1
            processed_item = dict()
            processed_item["sku_code"] = item["sku_code"]
            processed_item["updated_time"] = item['updated_time'] if 'updated_time' in item else time.mktime(
                datetime.datetime.now().timetuple())
            processed_item['image'] = item['image']
            processed_item["attributes"] = list()
            for attributeKey in item["attribute_values"]:
                attribute_key = AttributeKey.objects.get_or_create(name=attributeKey,
                                                                   slug=item["attribute_values"][attributeKey]['slug'])[
                    0]
                attribute_value = AttributeValue.objects.get_or_create(key_id=attribute_key.id,
                                                                       value=item["attribute_values"][attributeKey][
                                                                           'value'])[0]
                processed_item["attributes"].append(attribute_value)
            self.processed_data.append(processed_item)

    def post_process(self):
        return super(ItemSyncInterface, self).post_process()

    def force_sync(self):
        self.process()
        if self.is_first_batch == "true":
            Item.objects.all().delete()
        for item in self.processed_data:
            new_item = Item.objects.create(sku_code=item["sku_code"])
            for attributes in item["attributes"]:
                new_item.attributes.add(attributes)
            new_item.save()
            self.count += 1
        self.post_process()
        return JsonResponse({"status": 200, "data": {"Success count": self.count, "errors": self.exceptions}})

    def partial_sync(self, with_time_stamp=True):
        self.process()
        for item in self.processed_data:
            old_item = Item.objects.get_or_create(sku_code=item["sku_code"])[0]
            item_audit = ItemAudit.objects.get(item_id=old_item.id)
            if with_time_stamp:
                item['updated_time'] = datetime.datetime.fromtimestamp(item['updated_time'], tz=pytz.UTC)
            if (with_time_stamp and item_audit.updated_at.replace(tzinfo=utc) < item["updated_time"].replace(
                    tzinfo=utc)) or not with_time_stamp:
                old_item.image = item["image"]
                old_item.attributes.clear()
                for attributes in item["attributes"]:
                    old_item.attributes.add(attributes)
                old_item.save()
            self.count += 1
        return JsonResponse({"status": 200, "data": {"Success count": self.count, "errors": self.exceptions}})

    def sync(self):
        if "force_sync" in self.request.GET:
            return self.force_sync()
        elif "partial_sync" in self.request.GET:
            if "with_time_stamp" in self.request.GET:
                return self.partial_sync(with_time_stamp=True)
            else:
                return self.partial_sync(with_time_stamp=False)


class SerialSyncInterface(SyncInterface):
    def __init__(self, raw_data: list, is_first_batch, request):
        super(SerialSyncInterface, self).__init__(raw_data=raw_data, is_first_batch=is_first_batch, request=request)

    def pre_process(self):
        return super(SerialSyncInterface, self).pre_process()

    def process(self):
        self.pre_process()
        for item in self.raw_data:
            self.line += 1
            processed_item = dict()
            processed_item["updated_time"] = item['updated_time'] if 'updated_time' in item else time.mktime(
                datetime.datetime.now().timetuple())
            processed_item['hash'] = item['hash']
            sync_user_obj = CustomUser.objects.get_or_create(username='syncer', email='syncer@gmail.com')[0]
            processed_item['batch'] = \
                Batch.objects.get_or_create(remarks='bluefish', name='bluefish', created_by=sync_user_obj)[0]
            processed_item['item'] = Item.objects.get_or_create(sku_code=item['sku_code'])[0]
            processed_item['created_by'] = sync_user_obj
            self.processed_data.append(processed_item)

    def post_process(self):
        return super(SerialSyncInterface, self).post_process()

    def force_sync(self):
        self.process()
        if self.is_first_batch == "true":
            SerialNumber.objects.all().delete()
        for serial in self.processed_data:
            created = SerialNumber.objects.create(batch=serial['batch'], item=serial['item'],
                                                  hash=serial['hash'], created_by=serial['created_by'])
            if created:
                self.count += 1
        self.post_process()
        return JsonResponse({"status": 200, "data": {"Success count": self.count, "errors": self.exceptions}})

    def partial_sync(self, with_time_stamp=True):
        self.process()
        for item in self.processed_data:
            old_serial = SerialNumber.objects.filter(hash=item['hash'])
            if with_time_stamp:
                item['updated_time'] = datetime.datetime.fromtimestamp(item['updated_time'],tz=pytz.UTC)
            if old_serial.exists():
                old_serial = SerialNumber.objects.get(hash=item['hash'])
                serial_number_audit = SerialNumberAudit.objects.get(serial_number_id=old_serial.id)
                if (with_time_stamp and serial_number_audit.updated_at.replace(tzinfo=utc) < item[
                    "updated_time"].replace(tzinfo=utc)) or not with_time_stamp:
                    old_serial.batch = item["batch"]
                    # old_serial.item = item['item']
                    old_serial.save()
            else:
                SerialNumber.objects.create(batch=item['batch'], item=item['item'],
                                            hash=item['hash'], created_by=item['created_by'])
                self.count += 1
        return JsonResponse({"status": 200, "data": {"Success count": self.count, "errors": self.exceptions}})

    def sync(self):
        if "force_sync" in self.request.GET:
            return self.force_sync()
        elif "partial_sync" in self.request.GET:
            if "with_time_stamp" in self.request.GET:
                return self.partial_sync(with_time_stamp=True)
            else:
                return self.partial_sync(with_time_stamp=False)

class StockPointSyncInterface(SyncInterface):
    def __init__(self, raw_data, is_first_batch, request):
        super(StockPointSyncInterface, self).__init__(raw_data=raw_data, is_first_batch=is_first_batch, request=request)

    def pre_process(self):
        pass

    def process(self):
        pass

    def post_process(self):
        return super(StockPointSyncInterface, self).post_process()

    def force_sync(self):
        for stock_point_detail in self.raw_data:
            warehouse = Warehouse.objects.get_or_create(name=stock_point_detail['warehouse__name'])[0]
            if not StockPoint.objects.filter(client_pk_id=stock_point_detail['id']).exists():
                StockPoint.objects.get_or_create(name=stock_point_detail['name'], warehouse=warehouse,
                                                 client_pk_id=stock_point_detail['id'])
                self.count += 1

    def partial_sync(self):
        for stock_point_detail in self.raw_data:
            warehouse = Warehouse.objects.get_or_create(name=stock_point_detail['warehouse__name'])[0]
            if not StockPoint.objects.filter(client_pk_id=stock_point_detail['id']).exists():
                StockPoint.objects.create(name=stock_point_detail['name'], warehouse=warehouse,
                                          client_pk_id=stock_point_detail['id'])
                self.count += 1
            else:
                stock_point = StockPoint.objects.get(client_pk_id=stock_point_detail['id'])
                stock_point.name = stock_point_detail['name']
                stock_point.warehouse = warehouse
                stock_point.save()
                self.count += 1

        return JsonResponse({"status": 200, "data": {"Success count": self.count, "errors": self.exceptions}})

    def sync(self):
        if "force_sync" in self.request.GET:
            return self.force_sync()
        elif "partial_sync" in self.request.GET:
            if "with_time_stamp" in self.request.GET:
                return self.partial_sync()
            else:
                return self.partial_sync()
