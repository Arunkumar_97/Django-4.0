import openpyxl


def excel_extraction(file: str):
    wb = openpyxl.load_workbook(file)
    body = []
    first_row = 1
    sheet = wb.active
    for row in sheet:
        rows = []
        for ro in row:
            rows.append(ro.value)
        if first_row == 1:
            first_row = 0
        else:
            if rows[0] is not None:
                body.append(rows)
            else:
                break
    return body
