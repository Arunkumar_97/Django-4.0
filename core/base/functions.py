def get_fields(model):
    """
    Get all the column names of the model

    :param model: model for which we need to find the column names
    :type model: model
    :rtype list
    """
    return [field.get_attname() for field in getattr(getattr(model, "_meta"), "fields")]


def set_instance_values(model, data, instance):
    """
    Set fillable fields of a model

    :param model: target model
    :param data: request data
    :param instance: validated instance
    :type model: model class
    :type data: dict
    :type instance: model instance
    """
    fillable_fields = get_fields(model)
    for field, value in data.items():
        if field in fillable_fields:
            setattr(instance, field, value)
    return instance
