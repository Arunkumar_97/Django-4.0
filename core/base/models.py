from django.conf import settings
from django.db import models
from datetime import datetime
from django.utils.timezone import make_aware
from django.core.validators import RegexValidator

from core.utils.date_time import get_current_date_time


class AbstractBase(models.Model):
    valid = models.BooleanField(default=True, null=False)

    class Meta:
        abstract = True


class FinancialYearQuerySet(models.QuerySet):
    def financial_year(self, start, end):
        today = get_current_date_time()
        month = today.month
        if month <= 3:
            start_year = today.year - start - 1
            end_year = today.year - end - 1
        else:
            start_year = today.year - start
            end_year = today.year - end
        start_date = make_aware(datetime(start_year, 4, 1))
        end_date = make_aware(datetime(end_year, 4, 1))
        return self.filter(created_at__range=(start_date, end_date))


class FinancialYearManager(models.Manager):
    def get_queryset(self):
        return FinancialYearQuerySet(self.model, using=self._db)

    def current(self):
        return self.get_queryset().financial_year(0, -1)

    def previous(self):
        return self.get_queryset().financial_year(0, 1)


class Audit(AbstractBase):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name="created_%(class)s")
    last_updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, blank=True,
                                        related_name="updated_%(class)s")

    objects = models.Manager()
    year = FinancialYearManager()

    class Meta:
        abstract = True


phone_regex = RegexValidator(
    regex=r'^\+?1?\d{12,12}$',
    message="Phone number must be entered in the format: '+999999999999'."
)
