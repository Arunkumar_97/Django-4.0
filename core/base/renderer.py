from rest_framework.renderers import JSONRenderer
from rest_framework.utils.serializer_helpers import ReturnDict

# from creature_django.notifications.functions import get_notifications


class ApiRenderer(JSONRenderer):

    def render(self, data, accepted_media_type=None, renderer_context=None):
        response_dict = {}
        status = data.get("status", "success")
        message = data.get("message", "")
        errors = data.get("errors", {})
        response_data = data.get("data", {})
        if status == 'success':
            code = 'E200'
        else:
            code = data.get('code', 'E500')
        if code == 422:
            message = "Data Validation Failed"
            if len(errors) and type(errors.__class__ == ReturnDict):
                error_dict = {}
                for field_name, error_list in errors.items():
                    error_dict[field_name] = [str(error) for error in error_list]
                response_data["errors"] = error_dict
            code = "E422"
        elif code == "token_not_valid":
            code = "E403"
            message = "Token Invalid"
        request = renderer_context.get("request")
        user_notifications = []
        if request and request.user and request.user.is_authenticated:
            pass

        response_dict['data'] = response_data
        response_dict['notifications'] = user_notifications
        response_dict['status'] = status
        response_dict['message'] = message
        response_dict['code'] = code
        data = response_dict
        return super().render(data)

