from rest_framework.response import Response


class ApiResponse(Response):
    def __init__(self, data: dict = None, status="success", code="E200", message="", errors=None):
        """
            Format the HTTP response to the client

            :param data Response data
            :param status HTTP status message
            :param code Application code
            :param errors Serializer Errors
            :type data dict
            :type status str
            :type data str
            :type message str
            :type errors dict
            """
        if errors is None:
            errors = {}
        if data is None:
            data = {}
        super().__init__(data={
            "data": data,
            "status": status,
            "code": code,
            "message": message,
            "errors": errors,
        })
