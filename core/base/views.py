from rest_framework import parsers, permissions, views, authentication
from .renderer import ApiRenderer
from .response import ApiResponse
from django.contrib.contenttypes.models import ContentType
import json
from django.db import transaction
from ..models import UserType
from ..utils.logging import get_exception_logger, print_exception
from loging.models import ActionLog
# from ...notifications.functions import notify_action

exception_logger = get_exception_logger()


def get_request_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class JSONApi(views.APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    renderer_classes = (ApiRenderer,)
    parser_classes = (parsers.JSONParser,)

    ui_permissions = []
    model = None
    skip_model_permission = False

    def verify_ui_permissions(self, request):
        if len(self.ui_permissions):
            return request.user.has_perms(self.ui_permissions)
        return True

    def verify_model_permissions(self, request):
        if self.model is not None and not self.skip_model_permission:
            perms_map = {
                'GET': ['%(app_label)s.view_%(model_name)s'] if request.user.user_type == UserType.THIRD_PARTY else [],
                'POST': ['%(app_label)s.add_%(model_name)s'],
                'PUT': ['%(app_label)s.change_%(model_name)s'],
                'PATCH': ['%(app_label)s.change_%(model_name)s'],
                'DELETE': ['%(app_label)s.delete_%(model_name)s'],
            }
            kwargs = {
                'app_label': self.model._meta.app_label,
                'model_name': self.model._meta.model_name
            }
            perms = [perm % kwargs for perm in perms_map[request.method]]
            return request.user.has_perms(perms)
        return True

    def log_action(self, request, action, object_id=None, object_repr=""):
        try:
            content_type = ContentType.objects.get_for_model(self.model)
            ActionLog.objects.create(
                user_id=request.user.id,
                content_type=content_type,
                object_id=str(object_id),
                object_repr=object_repr[:200],
                action_flag=action,
                change_message=json.dumps(request.data),
                request_ip=get_request_ip(request),
                request_url=request.build_absolute_uri()
            )
            # notify_action(action, content_type, request.user, object_id, object_repr)
        except Exception as e:
            exception_logger.exception("Exception: ".format(str(e)))
            print_exception()

    @staticmethod
    def get_base_url(request):
        return "http{}://{}/".format("s" if request.is_secure() else "", request.get_host())

    @staticmethod
    def api(func):
        def wrapper(self, request, *callback_args, **callback_kwargs):
            try:
                if not self.verify_ui_permissions(request):
                    return ApiResponse(message="UI permissions failed!", status="failure")
                if not self.verify_model_permissions(request):
                    return ApiResponse(message="Model permissions failed!", status="failure")
                return func(self, request, *callback_args, **callback_kwargs)
            except Exception as e:
                exception_logger.exception("Exception: ".format(str(e)))
                return ApiResponse(message=str(e), status="failure")

        return wrapper


class MultipartApi(JSONApi):
    parser_classes = (parsers.MultiPartParser,)


class CRUDApi(JSONApi):
    serializer_class = None

    def get_list(self, filters):
        return list(self.model.objects.filter(**filters).values())

    @JSONApi.api
    def get(self, request):
        filters = {}
        for key, value in request.GET.items():
            filters[key] = value
        return ApiResponse(data={"objects": self.get_list(filters)})

    @JSONApi.api
    @transaction.atomic
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            from django.contrib.admin.models import ADDITION
            self.log_action(request, ADDITION, obj.id, str(obj))
            return ApiResponse(data=self.get_list({"id": obj.id})[0])
        return ApiResponse(errors=serializer.errors, status="failure", code=422)

    @JSONApi.api
    @transaction.atomic
    def put(self, request, obj_id):
        instance = self.model.objects.get(pk=obj_id)
        serializer = self.serializer_class(instance, data=request.data)
        if serializer.is_valid():
            obj = serializer.save()
            from django.contrib.admin.models import CHANGE
            self.log_action(request, CHANGE, obj_id, str(obj))
            return ApiResponse(data=self.get_list({"id": obj_id})[0])
        return ApiResponse(errors=serializer.errors, status="failure", code=422)

    @JSONApi.api
    def delete(self, request, obj_id):
        obj = self.model.objects.get(pk=obj_id)
        obj.delete()
        from django.contrib.admin.models import DELETION
        self.log_action(request, DELETION, obj_id, str(obj))
        return ApiResponse(message="Object Deleted Successfully")
