import threading

request_local = threading.local()


def get_current_request():
    return getattr(request_local, 'request', None)


