from datetime import datetime as dt
import calendar
import pytz
from django.conf import settings
from django.db.models import Func
from django.utils import timezone
import time
from collections import namedtuple
from datetime import timedelta, date


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days + 1)):
        yield start_date + timedelta(n)


def convert_to_time(seconds):
    return time.strftime("%H:%M:%S", time.gmtime(seconds))


def convert_24_12(time_value):
    return time_value.strftime("%I:%M %p")


def date_time_func(date, time):
    tz = pytz.timezone(settings.TIME_ZONE)
    return tz.localize(dt.strptime(str(date) + ' ' + str(time), '%Y-%m-%d %H:%M:%S'))


def get_local_time(utctime):
    utc = utctime.replace(tzinfo=pytz.UTC)
    return utc.astimezone(timezone.get_current_timezone())


def get_current_date_time():
    x = dt.now()
    tz = pytz.timezone(settings.TIME_ZONE)
    return tz.localize(x)


def get_date_time(date, start=True):
    """input date format: yyyy-mm-dd"""
    if start:
        date_time = dt.strptime(str(date) + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
    else:
        date_time = dt.strptime(str(date) + ' 23:59:59', '%Y-%m-%d %H:%M:%S')
    tz = pytz.timezone(settings.TIME_ZONE)
    return tz.localize(date_time)


def time_to_date(time):
    date_format = "%m-%d-%Y %H:%M:%S"
    return dt.strptime('08-01-2008 ' + str(time), date_format)


def get_time_interval(time1, time2):
    date_time_1 = time_to_date(time1)
    date_time_2 = time_to_date(time2)
    diff = date_time_1 - date_time_2
    return diff


def get_financial_year(only_number=False):
    today = get_current_date_time()
    year = today.year if today.month >= 4 else today.year - 1
    if only_number:
        return "{}{}".format(str(year)[-2:], str(year + 1)[-2:])
    return "{}/{}".format(str(year)[-2:], str(year + 1)[-2:])


def get_date(value):
    if type(value) is str:
        return dt.strptime(value, "%Y-%m-%d")
    else:
        return value


def get_date_interval(from_date, to_date):
    return int((get_date(to_date) - get_date(from_date)).days) + 1


def number_of_sundays(value_month, value_year):
    return len([1 for i in calendar.monthcalendar(int(value_year), int(value_month)) if i[6] != 0])


def get_number_of_tuesdays(value_month, value_year):
    return len([1 for i in calendar.monthcalendar(int(value_year), int(value_month)) if i[1] != 0])


def number_of_wednesdays(value_month, value_year):
    return len([1 for i in calendar.monthcalendar(int(value_year), int(value_month)) if i[2] != 0])


def number_of_mondays(value_month, value_year):
    return len([1 for i in calendar.monthcalendar(int(value_year), int(value_month)) if i[0] != 0])


def number_of_days_in_month(y, m):
    leap = 0
    if y % 400 == 0:
        leap = 1
    elif y % 100 == 0:
        leap = 0
    elif y % 4 == 0:
        leap = 1
    if m == 2:
        return 28 + leap
    months = [1, 3, 5, 7, 8, 10, 12]
    if m in months:
        return 31
    return 30


def date_overlap_check(date_1_from, date_1_to, date_2_from, date_2_to):
    Range = namedtuple('Range', ['start', 'end'])
    r1 = Range(start=get_date(date_1_from), end=get_date(date_1_to))
    r2 = Range(start=get_date(date_2_from), end=get_date(date_2_to))
    latest_start = max(r1.start, r2.start)
    earliest_end = min(r1.end, r2.end)
    delta = (earliest_end - latest_start).days + 1
    return max(0, delta) == 0


def str_to_month_year(month_year_str):
    temp = month_year_str.split("-")
    return int(temp[1]), int(temp[0])


def ToChar(expression, output=getattr(settings, "SQL_DATETIME_FORMAT", "DD/MM/YYYY HH24:MI")):
    '''
    Custom query to convert timestamp to string.
    Example usage
    queryset.annotate(
        created_date_str=ToChar('created_date', 'DD/MM/YYYY HH25:MI')
        )
    '''

    class ToCharWithoutTZ(Func):
        function = "TO_CHAR"
        template = '%(function)s(%(expressions)s, \'{output}\')'.format(output=output)

    return ToCharWithoutTZ(expression)
