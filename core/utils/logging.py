import logging
import os
import sys
import traceback2

from django.db import transaction


def get_default_logger():
    logger = logging.getLogger('default_logger')
    return logger


def get_exception_logger():
    logger = logging.getLogger('exception_logger')
    return logger


def print_exception(atomic=False):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print(exc_type, fname, exc_tb.tb_lineno, traceback2.format_exc())
    if atomic:
        transaction.set_rollback(rollback=False)
