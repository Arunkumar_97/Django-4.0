from django.apps import apps
from treelib import Tree, Node
from inventory.models import Warehouse, StockPoint


class InventoryTree(object):
    def __init__(self):
        self._tree = Tree()


class CreateWareHouse(InventoryTree):
    def __init__(self, name, manager_id):
        super().__init__()
        self.name = name
        self.manager = manager_id
        warehouse = Warehouse.objects.create(name=self.name, manager_id=self.manager)
        warehouse.save()
        self._tree.create_node(warehouse.name, warehouse.name, data=warehouse)


class CreateStockPoint(InventoryTree):
    def __init__(self, name, warehouse_id, layout, slug):
        super().__init__()
        self.name = name
        self.slug = slug
        self.layout = layout
        self.warehouse = Warehouse.objects.get(id=warehouse_id)
        stock_point = StockPoint.objects.create(name=self.name, slug=self.slug, layout=self.layout,
                                                warehouse_id=self.warehouse.id)
        stock_point.save()
        parent = self._tree.get_node(self.warehouse.name)
        self._tree.create_node(stock_point.name, stock_point.name, parent=parent, data=stock_point)
        print(self._tree.paths_to_leaves())


class GetLeaves(InventoryTree):
    def __init__(self):
        super().__init__()
        print(self._tree.paths_to_leaves())

