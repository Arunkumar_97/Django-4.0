import pytz
import datetime
from django.conf import settings
import os
import json


def get_user_types():
    try:
        x_file = open(os.path.join(settings.BASE_DIR, "config/userTypes.json"), "r")
        data = json.load(x_file)
        return data["userTypes"]
    except FileNotFoundError:
        return []


def transform_date_query(request, start_date_name="start_date", end_date_name="end_date"):
    indian_time = pytz.timezone("Asia/Kolkata")
    return (datetime.datetime.combine(datetime.datetime.strptime(request.GET.get(start_date_name), "%Y-%m-%d"),
                                      datetime.time.min).replace(tzinfo=indian_time),
            datetime.datetime.combine(datetime.datetime.strptime(request.GET.get(end_date_name), "%Y-%m-%d"),
                                      datetime.time.max).replace(tzinfo=indian_time))