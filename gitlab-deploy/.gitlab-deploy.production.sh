#!/bin/bash
GREEN="\e[32m"
RESET="\e[0m"
sudo chmod -R 777 /home/ubuntu/Django-4.0/
cd /home/ubuntu/Django-4.0/
mkdir arun
echo -e "${GREEN}Pulling Change $CI_BUILD_REF_NAME ${RESET}"
git config credential.helper store
git pull origin $CI_BUILD_REF_NAME
echo -e "${GREEN}Starting VENV ${RESET}"
source ../env/bin/activate
echo -e "${GREEN}Installing Requirements ${RESET}"
pip3 install -r requirements.txt
echo -e "${GREEN}Finished installing Requirements ${RESET}"
echo -e "${GREEN}Started Make Migrations ${RESET}"
python3 manage.py makemigrations
echo -e "${GREEN}Finished Make Migrations ${RESET}"
echo -e "${GREEN}Started Migrations ${RESET}"
python3 manage.py migrate
echo -e "${GREEN}Finished Migrations ${RESET}"
echo -e "${GREEN}Exiting VENV ${RESET}"
deactivate
echo -e "${GREEN}Restarting Service ${RESET}"
sudo systemctl restart wms
echo -e "${GREEN}Service Restarted successfully ${RESET}"
