from django.contrib.auth import logout
from django.http import HttpResponseForbidden
from django.urls import get_resolver, resolve
from django.urls import reverse, path
import os

from requests import Response

from WMS.settings import MEDIA_ROOT
from datetime import datetime


def activity_log(request):
    dir_path = f'{MEDIA_ROOT}/production-access-logs'
    date = datetime.today().strftime('%d-%m-%Y %H:%M:%S')
    file_name = f"{datetime.today().strftime('%d-%m-%Y')}-logfile"
    file_path = f'{dir_path}/{file_name}.txt'
    os.makedirs(dir_path, exist_ok=True)
    try:
        if os.path.exists(file_path):
            with open(file_path, 'a') as f:
                params = request.GET if request.method == "GET" else request.body.decode('utf-8')
                content = f'Time {date} User {request.user.username} Path {request.path} Method {request.method} Params {params}'
                f.write(content)
                f.write('\n')
        else:
            with open(file_path, 'w') as f:
                params = request.GET if request.method == "GET" else request.body.decode('utf-8')
                content = f' Time {date}- User {request.user.username}  Path{request.path} Method {request.method} Params {params}'
                f.write(content)
                f.write('\n')
    except IsADirectoryError:
        print(f'Problem with the directory {dir_path}')
        pass
    except FileNotFoundError:
        print(f'Problem with the file {file_path}')
        pass
    except Exception as e:
        print(str(e))
        pass


class AuthMiddleware:
    # Static list for storing all the valid urls in the application
    urls = list()

    def __init__(self, get_response):
        self.get_response = get_response

        for url in get_resolver().reverse_dict.keys():
            # ignore gibberish url objects from resolver
            if not str(url).startswith("<"):
                self.urls.append(url)

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        activity_log(request)
        if 'X-App-Version' in request.headers:
            if request.headers['X-App-Version'] != '109':
                print(request.headers['X-App-Version'])
                return HttpResponseForbidden("forbidden")
        if request.user.is_authenticated:
            return None
        else:
            logout(request)

