from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from fcm_django.models import FCMDevice


@api_view(['POST'])
@permission_classes([AllowAny])
def obtain_auth_token(request):
    user = authenticate(request, username=request.data['username'], password=request.data['password'])
    if user:
        token = Token.objects.get_or_create(user=user)[0]
        data = {"token": str(token)}
        if "fcm_token" in request.data:
            if FCMDevice.objects.filter(user_id=user.id).exists():
                fcm_device = FCMDevice.objects.filter(user_id=user.id)
                fcm_device.delete()
            fcm_device = FCMDevice(registration_id=request.data["fcm_token"], user=user)
            fcm_device.save()
        return Response(status=status.HTTP_200_OK, data=data)
    return Response(status=status.HTTP_400_BAD_REQUEST, data={"messages": ["Invalid Credentials"]})


@login_required
def home(request):
    context = {'token': Token.objects.get_or_create(user=request.user)[0].key,'is_super_user':request.user.is_superuser,'username':request.user.username}
    return render(request, template_name="index.html", context=context)



class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            'username': user.username,
            'is_superuser': user.is_superuser
        })
