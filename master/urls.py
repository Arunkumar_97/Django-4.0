from . import views

from django.urls import path, include
from . import views

urlpatterns = [
    path('select/<app>/<model>/', views.SelectViewSet.as_view({'get': 'list'})),
    path('<str:sku_code>/', views.SkuCode.as_view())
]
