import datetime
from WMS.get_sync_user import get_sync_user
from django.db import models
from core.models import Audit
from django_lifecycle import LifecycleModel, hook, AFTER_CREATE, BEFORE_UPDATE, BEFORE_CREATE

class AttributeKey(models.Model):
    name = models.CharField(max_length=64, unique=True)
    slug = models.CharField(max_length=8, unique=True)

    def __str__(self):
        return self.name


class AttributeValue(models.Model):
    value = models.CharField(max_length=256, null=False, blank=False)
    key = models.ForeignKey(AttributeKey, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('key', 'value')

    def __str__(self):
        return "{} - {}".format(self.key.slug, self.value)


class Item(LifecycleModel):
    sku_code = models.CharField(max_length=16, unique=True)
    image = models.ImageField(null=True,max_length=300)
    attributes = models.ManyToManyField(to=AttributeValue, related_name='item', blank=True)

    def __str__(self):
        return self.sku_code

    def save(self, *args, **kwargs):
        super(Item, self).save(*args, **kwargs)

    @hook(BEFORE_UPDATE, has_changed=True)
    def check_update(self):
        item_audit = ItemAudit.objects.get(item_id=self.id)
        item_audit.updated_by = get_sync_user()
        item_audit.updated_at = datetime.datetime.now()
        item_audit.save()

    @hook(AFTER_CREATE)
    def create_audit(self):
        item_audit = ItemAudit.objects.create(item=self, created_at=datetime.datetime.now(),
                                              updated_at=datetime.datetime.now(), updated_by=get_sync_user(),
                                              created_by=get_sync_user())
        item_audit.save()


class ItemAudit(Audit):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)


class SyncConfig(models.Model):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)