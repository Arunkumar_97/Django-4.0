from django.contrib import admin

from master.models import Item, AttributeKey, AttributeValue, SyncConfig


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'sku_code')
    search_fields = ('sku_code',)


@admin.register(AttributeKey)
class AttributeKeyAdmin(admin.ModelAdmin):
    pass


@admin.register(AttributeValue)
class AttributeValueAdmin(admin.ModelAdmin):
    pass
admin.site.register(SyncConfig)