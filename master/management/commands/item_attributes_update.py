from WMS.settings import MEDIA_ROOT
from inventory.models import SerialNumber, Stock
from master.models import *
from master.models import Item
from utils.excel.extraction import excel_extraction

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Update Stock'

    def handle(self, *args, **options):
        print('Script Started to update item detail')
        data = excel_extraction(MEDIA_ROOT + '/item_attributes.xlsx')
        for row in data:
            count = 0
            item_obj = Item.objects.get_or_create(sku_code=row[0])[0]
            item_obj.attributes.clear()
            column = 1
            for attributeKey in ['size', 'shade', 'description', 'design']:
                attribute_key = AttributeKey.objects.get(name=attributeKey)
                attribute_value = AttributeValue.objects.get_or_create(key_id=attribute_key.id,
                                                                       value=row[column])[0]
                item_obj.attributes.add(attribute_value)
                column += 1
            item_obj.save()
            count += 1
        print('item updated')
