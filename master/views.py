import sys
import operator
from functools import reduce

from rest_framework import viewsets
from django.db.models import Q
from django.apps import apps
from core.base.views import JSONApi
from .models import Item
from django.http import JsonResponse
from dispatch.serializers import *
from inventory.serializers import *


class SelectViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        app = self.kwargs.get('app')
        model = apps.get_model(app, self.kwargs.get('model'))
        filters = {}
        for key, value in self.request.query_params.items():
            if key.find('id') == -1:
                if value == 'all':
                    continue
                filters[f'{key}__icontains'] = value
            else:
                filters[f'{key}'] = value
        return model.objects.filter(**filters).all()

    def get_serializer_class(self):
        model_name = self.kwargs.get('model')
        model_name += 'Serializer'

        def str_to_class(str):
            return getattr(sys.modules[__name__], str)

        return str_to_class(model_name)


class SkuCode(JSONApi):
    @staticmethod
    def get(request, sku_code):
        try:
            item_obj = Item.objects.get(sku_code=sku_code)
            last_scanned_dict = {
                "size": item_obj.attributes.filter(key__name='size').values('value')[0]['value'],
                "shade": item_obj.attributes.filter(key__name='shade').values('value')[0]['value'],
                "image": item_obj.image.name
            }
            return JsonResponse(
                {"success": True, "message": "SKU Validated Successfully", "last_scanned": last_scanned_dict})

        except Exception as e:
            return JsonResponse({"success": False, "message": "SKU Not Found", "last_scanned": {}})


class TableViewSet(viewsets.ModelViewSet):

    def get_queryset(self):
        app = self.kwargs.get('app')
        model = apps.get_model(app, self.kwargs.get('model'))
        search_query = self.request.query_params['search'] if 'search' in self.request.query_params else None
        columns = [("{}__icontains".format(field.name), search_query) for field in Item._meta.get_fields()]
        q_list = [Q(x) for x in columns]
        return model.objects.filter(reduce(operator.or_, q_list)).all()

    def get_serializer_class(self):
        model_name = self.kwargs.get('model')
        model_name += 'Serializer'

        def str_to_class(str):
            return getattr(sys.modules[__name__], str)

        return str_to_class(model_name)
