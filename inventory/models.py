from django.db import models

# Create your models here.
from datetime import datetime
from django.db import models
from django.db.models import Sum, Value, UniqueConstraint, CheckConstraint, Q
from django.db.models.functions import Coalesce
from django.utils.translation import gettext as _
from polymorphic.models import PolymorphicModel, PolymorphicManager
from django_lifecycle import LifecycleModel, BEFORE_UPDATE, AFTER_CREATE, AFTER_UPDATE, AFTER_SAVE

from WMS.get_sync_user import get_sync_user
from core.models import Audit, CustomUser, NameMixin
from mptt.models import MPTTModel
from django_lifecycle import LifecycleModel, hook
from core.utils.get_current_request import get_current_request
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from core.utils.model_utils import transform_date_query
from master.models import Item
from django.db.models.signals import m2m_changed


class Warehouse(NameMixin):
    users = models.ManyToManyField('core.CustomUser', related_name='warehouses')
    manager = models.ForeignKey('core.CustomUser', null=True, on_delete=models.CASCADE,
                                related_name='warehouse_manager')
    slug = models.SlugField(default='')

    def __str__(self):
        return self.name


class StockPoint(NameMixin):
    warehouse = models.ForeignKey('Warehouse', on_delete=models.CASCADE)
    slug = models.SlugField(max_length=4)
    layout_frontend_data = models.JSONField(blank=True, null=True)
    layout_backend_data = models.JSONField(blank=True, null=True)
    layout_temp=models.JSONField(blank=True,null=True)
    app_data = models.JSONField(blank=True, null=True)
    client_pk_id = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = _("Stock Point")

    def create_virtual_bays_and_shelves(self):
        bay_obj, created = \
            Bay.objects.get_or_create(type=Bay.BayType.VIRTUAL, name='Virtual Bay', stock_point_id=self.id)
        if created:
            choices = Shelf.VirtualShelfTypes.choices
            for choice in choices:
                VirtualShelf.objects.create(bay=bay_obj, name=choice[1])

    def save(self, *args, **kwargs):
        if self._state.adding:
            super(StockPoint, self).save(*args, **kwargs)
            self.create_virtual_bays_and_shelves()
        else:
            super(StockPoint, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Bay(models.Model):
    class BayType(models.TextChoices):
        VIRTUAL = 'VI', 'VIRTUAL BAY'
        PHYSICAL = 'PY', 'PHYSICAL BAY'

    stock_point = models.ForeignKey('StockPoint', on_delete=models.CASCADE, related_name='stock_point_stock')
    name = models.CharField(max_length=128, default='')
    type = models.CharField(max_length=2, choices=BayType.choices, default=BayType.PHYSICAL)

    class Meta:
        constraints = [
            UniqueConstraint(fields=['stock_point', 'name'], name='unique_bay'), ]

    def __str__(self):
        return self.name


class BinManager(PolymorphicManager):
    def get_queryset(self):
        return super(BinManager, self).get_queryset()

    def pick_able_shelves(self):
        return self.filter(is_picking_blocked=False)

    def put_able_shelves(self):
        return self.filter(is_put_away_blocked=False)

    def virtual_shelves(self, exclude_filter: dict = None):
        if exclude_filter is None:
            exclude_filter = {}
        return self.filter(bay__type=Bay.BayType.VIRTUAL, is_picking_blocked=False, is_put_away_blocked=False). \
            exclude(**exclude_filter)

    def physical_shelves(self):
        return self.filter(bay__type=Bay.BayType.PHYSICAL, is_picking_blocked=False, is_put_away_blocked=False)


class Shelf(PolymorphicModel):
    class VirtualShelfTypes(models.TextChoices):
        DISPATCHED = 1, _('Dispatched')
        RETURNS = 2, _('Returns')
        NOT_FOUND = 3, _('Not Found')
        LOST = 4, _('Lost')
        TRANSIT = 5, _('Transit')
        WORK_IN_PROGRESS = 6, _('Work In Progress')
        STAGING_AREA = 7, _('Staging Area')
        STOCK_AUDIT = 8,_('Stock Audit')

    mapping = {
        VirtualShelfTypes.DISPATCHED: {'is_picking_blocked': True, 'is_put_away_blocked': False},
        VirtualShelfTypes.RETURNS: {'is_picking_blocked': False, 'is_put_away_blocked': True},
        VirtualShelfTypes.NOT_FOUND: {'is_picking_blocked': False, 'is_put_away_blocked': False},
        VirtualShelfTypes.LOST: {'is_picking_blocked': False, 'is_put_away_blocked': False},
        VirtualShelfTypes.TRANSIT: {'is_picking_blocked': False, 'is_put_away_blocked': False},
        VirtualShelfTypes.WORK_IN_PROGRESS: {'is_picking_blocked': False, 'is_put_away_blocked': False},
        VirtualShelfTypes.STAGING_AREA: {'is_picking_blocked': True, 'is_put_away_blocked': True},
        VirtualShelfTypes.STOCK_AUDIT: {'is_picking_blocked': True, 'is_put_away_blocked': True}
    }
    name = models.CharField(max_length=128, null=False, blank=False)
    bay = models.ForeignKey('Bay', on_delete=models.CASCADE)
    is_picking_blocked = models.BooleanField(default=False, verbose_name="Picking Block")
    is_put_away_blocked = models.BooleanField(default=False, verbose_name="Put Away Block")

    objects = BinManager()

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.__class__.__name__ == 'Virtual Shelf':
            if self.type in self.mapping:
                self.is_picking_blocked = self.mapping[self.type]['is_picking_blocked']
                self.is_put_away_blocked = self.mapping[self.type]['is_put_away_blocked']
        super(Shelf, self).save()

    class Meta:
        constraints = [
            UniqueConstraint(fields=['bay_id', 'name'], name='unique_shelf'), ]
        verbose_name_plural = "Shelves"


class VirtualShelf(Shelf):
    type = models.IntegerField(choices=Shelf.VirtualShelfTypes.choices, null=False, blank=False)

    def __str__(self):
        return f'{self.bay.name} {self.name}'

    def save(self, *args, **kwargs):
        for choice in Shelf.VirtualShelfTypes.choices:
            if self.name == choice[1]:
                self.type = choice[0]
        super(VirtualShelf, self).save('VirtualShelf')

    class Meta:
        verbose_name_plural = "Virtual Shelves"


class PhysicalShelf(Shelf):
    capacity = models.IntegerField(null=False, blank=False, default=9999999)

    def __str__(self):
        return f'{self.bay} - {self.name}'

    class Meta:
        verbose_name_plural = "Physical Shelves"


class StockManager(models.Manager):
    def get_queryset(self):
        if not self.model.shelf:
            return super(StockManager, self).get_queryset().filter(stock_node__isnull=True)
        else:
            return super(StockManager, self).get_queryset()

    def without_virtual_shelf_stock(self, cross_docking=False, order_by=None):
        if order_by is None:
            order_by = []
        grn_shelf_filter = None
        if cross_docking:
            grn_shelf_filter = self.filter(shelf__name='GRN')
        virtual_shelf_filter = self.exclude(VirtualShelf).order_by(*order_by)
        return grn_shelf_filter | virtual_shelf_filter if grn_shelf_filter else virtual_shelf_filter


class Stock(LifecycleModel):
    item = models.ForeignKey(Item, null=False, related_name="stock", on_delete=models.CASCADE)
    shelf = models.ForeignKey('Shelf', null=False, on_delete=models.PROTECT, related_name='stock_bay')
    quantity = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    objects = StockManager()

    class Meta:
        constraints = [UniqueConstraint(fields=['item', 'shelf'], name='unique_item_shelf'),CheckConstraint(check=Q(quantity__gte=0),name='allow_only_postive_stock') ]

    def save(self, *args, **kwargs):
        super(Stock, self).save(*args, **kwargs)

    @hook(BEFORE_UPDATE, has_changed=True)
    def check_update(self):
        stock_audit = StockAudit.objects.get(stock=self.id)
        stock_audit.updated_by = get_sync_user()
        stock_audit.updated_at = datetime.now()
        stock_audit.save()

    @hook(AFTER_CREATE)
    def create_audit(self):
        stock = StockAudit.objects.create(stock=self, created_at=datetime.now(),
                                          updated_at=datetime.now(),
                                          updated_by=get_sync_user(),
                                          created_by=get_sync_user())
        stock.save()


class StockAudit(Audit):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)


class Batch(Audit, NameMixin):
    remarks = models.CharField(max_length=256)

    def __str__(self):
        return self.name

    @staticmethod
    def get_latest():
        return Batch.objects.latest('created_at')


class SerialNumberManager(models.Manager):
    def get_queryset(self):
        return super(SerialNumberManager, self).get_queryset()


class SerialNumber(LifecycleModel, Audit):
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)
    hash = models.CharField(max_length=64, unique=True)
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True)
    shelf = models.ForeignKey('Shelf', null=True, blank=True, on_delete=models.PROTECT, related_name='usns')
    objects = SerialNumberManager()

    @hook(BEFORE_UPDATE, has_changed=True)
    def check_update(self):
        serial_audit = SerialNumberAudit.objects.get(serial_number_id=self.id)
        serial_audit.updated_by = get_sync_user()
        serial_audit.updated_at = datetime.now()
        serial_audit.save()

    @hook(AFTER_CREATE)
    def create_audit(self):
        serial_number = SerialNumberAudit.objects.create(serial_number=self, created_at=datetime.now(),
                                                         updated_at=datetime.now(),
                                                         updated_by=get_sync_user(),
                                                         created_by=get_sync_user())
        serial_number.save()


class SerialNumberAudit(Audit):
    serial_number = models.ForeignKey(SerialNumber, on_delete=models.CASCADE)

    @staticmethod
    def get_latest():
        return SerialNumber.objects.latest('created_at')


class StockAdjustmentType(models.TextChoices):
    GRN = 'GRN', _('Goods Recieved')
    PICKING = 'PICKING', _('Picking')
    PUT_AWAY = 'PUT AWAY', _('Put Away')
    MISCELLANEOUS = 'MISCELLANEOUS', _('Miscellaneous')
    MANUAL = 'MANUAL', _('Manual')
    DC = 'DC', _('Delivery Challan')
    NOT_FOUND = 'NF', _('Not Found')
    DC_RETURN = 'DC RETURN', _('Delivery Challan Revert')
    DESIGN_TRANSFER = 'DESIGN TRANSFER', _('Design Transfer')
    CYCLE_COUNT = 'CYCLE COUNT', _('Cycle Count')

    # todo change to integer choices


class StockMovementInward(Audit):
    item = models.CharField(null=True, blank=True, max_length=32)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    associated_transaction_id = models.CharField(null=True, blank=True, max_length=20)
    associated_transaction = GenericForeignKey('content_type', 'associated_transaction_id')
    quantity = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    serials = models.ManyToManyField(SerialNumber)
    quantity_before_adjust = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    quantity_after_adjust = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    adjustment_type = models.CharField(choices=StockAdjustmentType.choices, max_length=50,
                                       default=StockAdjustmentType.MANUAL)
    from_shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name='in_movement_from_shelf', null=True)
    to_shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name='in_movement_to_shelf', null=True)
    remarks = models.CharField(max_length=200, null=True)

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(quantity__gte='0'), name='quantity_non_negative_constraint_1')]

    @staticmethod
    def apply_filters(request, filter_params):
        if request.GET.get('start_date'):
            filter_params['created_at__range'] = transform_date_query(request)
        return filter_params


class StockMovementOutward(Audit):
    item = models.CharField(null=True, blank=True, max_length=32)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    associated_transaction_id = models.CharField(null=True, blank=True, max_length=20)
    associated_transaction = GenericForeignKey('content_type', 'associated_transaction_id')
    quantity = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    serials = models.ManyToManyField(SerialNumber)
    quantity_before_adjust = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    quantity_after_adjust = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    adjustment_type = models.CharField(choices=StockAdjustmentType.choices, max_length=50,
                                       default=StockAdjustmentType.MANUAL)
    from_shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name='out_movement_from_shelf', null=True)
    to_shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name='out_movement_to_shelf', null=True)
    remarks = models.CharField(max_length=200, null=True)


class Meta:
    constraints = [
        models.CheckConstraint(check=models.Q(quantity__lte='0'), name='quantity_non_positive_constraint_2')]


class PickerConfig(models.Model):
    class PickerAlgorithType(models.TextChoices):
        Distance_Factor = 'DF', 'Distance Factor'
        Ageing_Factory = 'AF', 'Ageing Factor'

    type = models.CharField(max_length=2, choices=PickerAlgorithType.choices)
