from django.urls import path, include
from . import views

urlpatterns = [
    path('scan-usn-for-product-tagging/', views.ScanUSNForProductTagging.as_view()),
    path('product-tagging/', views.SkuSerialMapping.as_view()),
    path('usn/check-for-dispatch/', views.CheckUsnForDispatched.as_view()),
    path('usn/dispatch/', views.MarkUsnDispatched.as_view()),
    path('usn/wip/', views.MarkWIP.as_view()),
    path('sync/', views.SyncApi.as_view()),
    path('layout/', views.LayoutApi.as_view()),
    path('layout/get/', views.LayoutApi.as_view()),
    path('stockpoints/', views.StockPointApi.as_view()),
    path('usn-validate/<str:usn>/', views.ValidateUsn.as_view()),
    path('usn/<str:usn>/validate/', views.ValidateUsn.as_view()),
    path('import/shelf/', views.ImportShelf.as_view()),
    path('get/shelf-usns/', views.ShelfSerials.as_view()),
    path('sync-to-client/', views.StockSyncToClient.as_view()),
    path('usn/validate-for-grn/', views.ValidateUsnForGrn.as_view()),
    path('grn/', views.GRN.as_view()),
    path('import/move-wip/', views.WIPMovement.as_view()),
    path('replacement-usn/', views.ReplacementUsn.as_view()),
    path('design-transfer/', views.DesignTransfer.as_view()),
    path('shelf/<int:warehouse_id>/tree/', views.ShelfTree.as_view()),
    path('fetch-stock-details/', views.GetStockDetails.as_view())
]
