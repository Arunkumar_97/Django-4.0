import datetime

import requests
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import F, Sum, Q, DecimalField, OuterRef, CharField, Subquery
import json

from django.db import transaction, IntegrityError
from django.contrib.contenttypes.models import ContentType
from django.db.models.functions import Coalesce, Cast

from WMS.settings import client_machine_url
from core.base.views import JSONApi, MultipartApi
from core.models import CustomUser
from dispatch.functions import transform_serials_to_item_with_serials
from dispatch.interface.operation import WarehouseProcessType
from dispatch.models import Task, StockReservation
from inventory.function import create_stock_movement_without_reservation, is_item_already_exist_in_shelf_func, \
    sync_stock_to_client
from master.models import Item
from inventory.models import SerialNumber, VirtualShelf, Bay, StockPoint, Warehouse, Shelf, PhysicalShelf, \
    StockAdjustmentType, Stock, StockMovementInward, StockMovementOutward
from django.http import JsonResponse

from utils.excel.extraction import excel_extraction
from utils.sync.sync import StockPointSyncInterface
from utils.sync.sync import SerialSyncInterface, ItemSyncInterface


class ScanUSNForProductTagging(JSONApi):
    @staticmethod
    def get(request):
        data = request.query_params
        usn = data.get('usn', None)
        try:
            if SerialNumber.objects.filter(hash=usn.upper()).exists():
                # USN present and it is valid
                serial_no = SerialNumber.objects.get(hash=usn.upper())
                if serial_no.item:
                    return JsonResponse({"success": False, "message": "Usn Already Exists"})
                else:
                    return JsonResponse({"success": True, "message": "Usn Available For Tagging"})
            else:
                return JsonResponse({"success": False, "message": "USN Not Found"})
        except Exception as e:
            return JsonResponse({"success": False, "message": str(e)})


class SkuSerialMapping(JSONApi):
    @staticmethod
    def post(request):
        try:
            sku_serial_mapping_dict = request.data['sku_serial_mapping_dict']
            for sku, hashes in sku_serial_mapping_dict.items():
                SerialNumber.objects.filter(hash__in=hashes).update(item=Item.objects.get(sku_code=sku))
            return JsonResponse({"success": True, "created": True, "message": "Mapping was successful"})
        except Exception as e:
            return JsonResponse({"success": False, "created": False, "message": "Mapping could not be done"})


class CheckUsnForDispatched(JSONApi):
    @staticmethod
    def get(request):
        try:
            data = request.query_params
            usn = data.get('usn', None)
            sku_code = data.get('sku_code', None)
            stock_point = StockPoint.objects.get(client_pk_id=data.get('stock_point_id'))
            wip_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                          bay__stock_point=stock_point)
            serial_obj = SerialNumber.objects.filter(hash=usn, item__sku_code=sku_code)
            if serial_obj.exists():
                if serial_obj.first().shelf is None:
                    return JsonResponse({"success": False, "message": "Shelf is not Mapped to usn"})
                else:
                    if serial_obj.filter(shelf_id=wip_shelf.id).exists():
                        return JsonResponse({"success": True, "message": "Usn Available"})
                    else:
                        return JsonResponse({"success": False, "message": "Usn Not in Work In Progress"})
            else:
                return JsonResponse({"success": False, "message": "Item and Usn Mapping Invalid"})
        except Exception as e:
            return JsonResponse({"success": False, "message": str(e)})


class MarkUsnDispatched(JSONApi):
    @staticmethod
    def post(request):
        try:
            sku_serial_mapping_dict = request.data['sku_serial_mapping_dict']
            stock_point = StockPoint.objects.get(client_pk_id=request.data['stock_point_id'])
            mark_as_transit = request.data['mark_as_transit']
            if mark_as_transit == 'true' or mark_as_transit == True:
                destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.TRANSIT,
                                                      bay__stock_point=stock_point)
            else:
                destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.DISPATCHED,
                                                      bay__stock_point=stock_point)
            transaction_id = request.data['transaction_id']
            source_shelf = Shelf.objects.get(
                VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS, bay__stock_point=stock_point)
            for item in sku_serial_mapping_dict:
                base_serial_query = SerialNumber.objects.filter(hash__in=item['usns'],
                                                                shelf__virtualshelf__type=Shelf.VirtualShelfTypes.DISPATCHED)
                if base_serial_query.exists():
                    raise Exception('You are trying to scan a piece that has already been dispatched ' + ','.join(
                        base_serial_query.values_list('hash', flat=True)))
            success, exception = create_stock_movement_without_reservation(request=request,
                                                                           items=sku_serial_mapping_dict,
                                                                           movement_type="both",
                                                                           adjustment_type=StockAdjustmentType.DC,
                                                                           source_shelf=source_shelf,
                                                                           destination_shelf=destination_shelf,
                                                                           content_model=None,
                                                                           remarks=transaction_id)

            if success == 0:
                raise Exception(str(exception))
            return JsonResponse({"success": True, "created": True, "message": "USNS Moved to DC Shelf Successfully"})
        except Exception as e:
            return JsonResponse({"success": False, "created": False, "message": str(e)})


class SyncApi(JSONApi):
    @staticmethod
    @transaction.atomic
    def post(request):
        try:
            # raw_data = [{'updated_time': '2022-04-26', 'hash': 'OIUYS78', 'sku_code': "MWC87452", 'warehouse_name':'salem'}]
            data = json.loads(request.data)
            print(data)
            raw_data = data['raw_data']
            sync_type = data['sync_type']
            sync_module = data['sync_module']
            is_first_batch = data['is_first_batch'] if 'is_first_batch' in data else 'true'
            interface_obj = None
            if sync_module == 'Item':
                interface_obj = ItemSyncInterface(raw_data=raw_data, is_first_batch=is_first_batch, request=request)
            elif sync_module == 'Serial':
                interface_obj = SerialSyncInterface(raw_data=raw_data, is_first_batch=is_first_batch, request=request)
            elif sync_module == 'StockPoint':
                interface_obj = StockPointSyncInterface(raw_data=raw_data, is_first_batch=is_first_batch,
                                                        request=request)
            if sync_type == 'partial':
                return interface_obj.partial_sync()
            elif sync_type == 'force':
                return interface_obj.force_sync()
            else:
                raise Exception('Please check the Json Sent {}', request.data)

        except Exception as e:
            return JsonResponse({"status": 500, "data": {"Success count": 0, "errors": str(e), "data":json.loads(request.data)}})


class LayoutApi(JSONApi):

    @staticmethod
    def get(request):
        try:
            stock_point = request.GET.get('stock_point')
            if stock_point is not None:
                if stock_point == 'all':
                    layout = list(
                        StockPoint.objects.annotate(stock_point_id=F('id')).values('stock_point_id',
                                                                                   'layout_frontend_data',
                                                                                   'layout_backend_data',
                                                                                   'layout_temp', 'app_data'))
                else:
                    layout = list(StockPoint.objects.filter(id=stock_point).annotate(stock_point_id=F('id')).values(
                        'stock_point_id', 'layout_frontend_data', 'layout_backend_data', 'layout_temp', 'app_data'))

                return JsonResponse({"status": 200, "success": True, 'result': layout[0]['layout_temp'],
                                     'layout_frontend_data': layout[0]['layout_frontend_data'],
                                     'layout_backend_data': layout[0]['layout_backend_data'],'app_data':layout[0]['app_data'], "errors": ''})
            else:
                if stock_point is None:
                    raise Exception('Stock Point is Empty')
        except Exception as e:
            return JsonResponse({"status": 500, "success": False, "errors": str(e)})

    @staticmethod
    def post(request):
        global stock_point_obj, layout_backend_data, layout_frontend_data
        try:
            stock_point = request.data['stock_point_id'] if 'stock_point_id' in request.data else None
            layout_frontend_data = request.data[
                'layout_frontend_data'] if 'layout_frontend_data' in request.data else None
            layout_backend_data = request.data['layout_backend_data'] if 'layout_backend_data' in request.data else {}
            layout_frontend_v2_data = request.data['layout_frontend_v2_data'] if 'layout_frontend_v2_data' in request.data else {}
            stock_point_obj = StockPoint.objects.get(id=stock_point)
            layout_bay_list = []
            if stock_point_obj is not None or layout_backend_data is not None:
                for key, outer_row in layout_backend_data.items():
                    if outer_row['type'] == 'grid-box-bay':
                        layout_bay_list.append(outer_row['name'])
                        if 'previous_name' in outer_row:
                            try:
                                bay_obj = Bay.objects.get(name=outer_row['previous_name'], stock_point=stock_point_obj)
                                bay_obj.name = outer_row['name']
                                bay_obj.save()
                                del outer_row['previous_name']
                            except Bay.DoesNotExist:
                                del outer_row['previous_name']
                                pass
                        else:
                            if Bay.objects.filter(name=outer_row['name'], stock_point=stock_point_obj,
                                                  type=Bay.BayType.PHYSICAL).exists():
                                pass
                            else:
                                Bay.objects.create(name=outer_row['name'], stock_point=stock_point_obj,
                                                   type=Bay.BayType.PHYSICAL)

                    '''
                    before deletion checking for any serials mapped to deleted bays
                    if exist will not allow to delete
                    else we will allow for deletion
                    '''
                deleted_bays_in_layout = Bay.objects.filter(type=Bay.BayType.PHYSICAL,
                                                            stock_point=stock_point_obj).exclude(
                    name__in=layout_bay_list)
                if deleted_bays_in_layout.exists():
                    serials = SerialNumber.objects.filter(
                        shelf__bay__id__in=deleted_bays_in_layout.values_list('id', flat=True))
                    if serials.exists():
                        raise Exception('bay cannot be deleted since it is mapped to serial')
                else:
                    deleted_bays_in_layout.delete()
                stock_point_obj.layout_frontend_data = layout_frontend_data
                stock_point_obj.layout_backend_data = layout_backend_data
                stock_point_obj.app_data = layout_frontend_v2_data
                stock_point_obj.save()
            else:
                if stock_point is None:
                    raise Exception('Stock Point is Empty')
                if layout_frontend_data is None or layout_backend_data is None:
                    raise Exception('Layout is Empty')
            return JsonResponse(
                {"status": 200, "success": True, 'layout_backend_data': stock_point_obj.layout_backend_data,
                 'layout_frontend_data': stock_point_obj.layout_frontend_data, 'layout_frontend_v2_data': request.data['layout_frontend_v2_data'], "errors": ''})
        except Exception as e:
            return JsonResponse({"status": 500, "success": False, 'layout_backend_data': layout_backend_data,
                                 'layout_frontend_data': layout_frontend_data,'layout_frontend_v2_data': request.data['layout_frontend_v2_data'], "errors": str(e)})


class StockPointApi(JSONApi):
    @staticmethod
    def get(request):
        stock_points = list(StockPoint.objects.annotate(value=F('id'), label=F('name')).values('value', 'label'))
        return JsonResponse({"status": 200, "success": True, 'stock_points': stock_points, "errors": ''})


class ValidateUsn(JSONApi):
    @staticmethod
    def get(request, usn):
        try:
            data = request.GET
            serial_obj = SerialNumber.objects.get(hash=usn)
            dc = []
            if data.get('for_returns', '') == 'true':
                dc = StockMovementInward.objects.filter(serials__hash=usn,
                                                        adjustment_type=StockAdjustmentType.DC).order_by(
                    '-created_at').values('remarks')[:1]
                if str(serial_obj.shelf.virtualshelf.type) != Shelf.VirtualShelfTypes.DISPATCHED.value:
                    # or len(dc) == 0 //commenting for sale return purpose
                    raise Exception(f'Usn {usn} is not dispatched to proceed return')

            else:
                if serial_obj and serial_obj.shelf and serial_obj.shelf.name == 'Dispatched':
                    return JsonResponse(
                        {"success": False, "status": 500,
                         "result": {"message": f" {usn} is dispatched"}})

            attributes_list = Item.objects.filter(sku_code=serial_obj.item.sku_code).values('attributes__key__name',
                                                                                            'attributes__value',
                                                                                            'image')
            attribute_dict = {}
            for attribute in attributes_list:
                attribute_dict[attribute['attributes__key__name']] = attribute['attributes__value']
            last_scanned_dict = {
                "size": attribute_dict.get('size', ''),
                "shade": attribute_dict.get('shade', ''),
                "design": attribute_dict.get("design", ""),
                "description": attribute_dict.get("description"),
                "image": attributes_list[0]['image'], "sku_code": serial_obj.item.sku_code,
                "dc": dc[0]['remarks'] if len(dc) > 0 else ''
            }
            return JsonResponse(
                {"success": True, "status": 200,
                 "result": {"message": f"Usn {usn} Validated Successfully", "last_scanned": last_scanned_dict}})
        except Exception as e:
            if str(e) == f'Usn {usn} is not dispatched to proceed return':
                return JsonResponse(
                    {"success": False, "status": 500,
                     "result": {"message": str(e), "last_scanned": {}}})
            return JsonResponse(
                {"success": False, "status": 500,
                 "result": {"message": f"USN {usn} Cannot be Validated", "last_scanned": {}}})


class ImportShelf(MultipartApi):
    @staticmethod
    def post(request):
        file = request.FILES['excel']
        data = excel_extraction(file)
        line = 0
        count = 0
        exceptions = list()
        try:
            with transaction.atomic():
                for row in data:
                    line += 1
                    warehouse = Warehouse.objects.get(name=row[3])
                    stock_point = StockPoint.objects.get(name=row[2], warehouse=warehouse)
                    bay = Bay.objects.get(stock_point=stock_point, name=row[4], type=Bay.BayType.PHYSICAL)
                    PhysicalShelf.objects.create(name=f'{warehouse.slug}-{stock_point.slug}-{bay.name}-{row[0]}',
                                                 bay=bay, capacity=row[1])
                    count += 1
        except Exception as e:
            exceptions.append('Records could not be imported !!! Exception raised on line no-{} {}'.format(line, e))
            count = 0

        if len(exceptions) > 0:
            return JsonResponse(
                {"success": False, "status": 500,
                 "message": "Excel File Upload Not Success", "exceptions": exceptions, 'count': count})
        else:
            return JsonResponse(
                {"success": True, "status": 200,
                 "message": "Excel File Success", "exceptions": exceptions, 'count': count})


class ShelfSerials(JSONApi):
    @staticmethod
    def get(request):
        serials = SerialNumber.objects.filter(shelf_id=request.GET.get('shelf_id')).annotate(
            serials=ArrayAgg('hash')).values('shelf__name')
        return JsonResponse({'status': 200, 'result': serials})


class SerialHistory(JSONApi):
    @staticmethod
    def get(request):
        serials = SerialNumber
        return JsonResponse({'status': 200, 'result': serials})


class StockSyncToClient(JSONApi):
    @staticmethod
    def post(request):
        response = sync_stock_to_client()
        if response:
            return JsonResponse({'success': True, 'status': 200, 'message': 'Sync Ok'})
        else:
            return JsonResponse({'success': True, 'status': 500, 'message': 'Something Wrong'})


class MarkWIP(JSONApi):
    @staticmethod
    def post(request):
        try:
            sku_serial_mapping_dict = request.data['sku_serial_mapping_dict']
            stock_point = StockPoint.objects.get(client_pk_id=request.data['stock_point_id'])
            mark_as_transit = request.data['mark_as_transit']
            if mark_as_transit == 'true' or mark_as_transit == True:
                destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.TRANSIT,
                                                      bay__stock_point=stock_point)
            else:
                destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                                      bay__stock_point=stock_point)
            transaction_id = request.data['transaction_id']
            shelf_vs_items = {}
            for item_detail in sku_serial_mapping_dict:
                for usn in item_detail['usns']:
                    source_shelf = SerialNumber.objects.get(hash=usn).shelf.id
                    if source_shelf not in shelf_vs_items:
                        shelf_vs_items[source_shelf] = [{'sku_code': item_detail['sku_code'], 'usns': [usn]}]
                    else:
                        is_item_already_exists, obj = is_item_already_exist_in_shelf_func(
                            sku_code=item_detail['sku_code'],
                            items=shelf_vs_items[source_shelf])
                        if not is_item_already_exists:
                            shelf_vs_items[source_shelf].append({'sku_code': item_detail['sku_code'], 'usns': [usn]})
                        else:
                            obj['usns'].append(usn)
            for source_shelf, items in shelf_vs_items.items():
                source_shelf_obj = Shelf.objects.get(id=source_shelf)
                success, exception = create_stock_movement_without_reservation(request=request, items=items,
                                                                               movement_type="both",
                                                                               adjustment_type=StockAdjustmentType.DC,
                                                                               source_shelf=source_shelf_obj,
                                                                               destination_shelf=destination_shelf,
                                                                               content_model=None,
                                                                               remarks=transaction_id)

                if success == 0:
                    raise Exception(str(exception))
            return JsonResponse({"success": True, "created": True, "message": "USNS Moved to DC Shelf Successfully"})
        except Exception as e:
            return JsonResponse({"success": False, "created": False, "message": str(e)})


class ValidateUsnForGrn(JSONApi):

    @staticmethod
    def get(request):
        try:
            usn = request.GET.get('usn', None)
            sku_code = request.GET.get('sku_code', None)
            serial_obj = SerialNumber.objects.filter(hash=usn, item__sku_code=sku_code)
            if serial_obj.exists():
                if serial_obj.filter(shelf=None).exists():
                    return JsonResponse({'success': True, 'status': 200, 'message': 'Validation Successful'})
                else:
                    return JsonResponse({'success': True, 'status': 500, 'message': 'USN Already In Inventory'})
            else:
                return JsonResponse({'success': False, 'status': 500, 'message': 'Item and SKU Mapping Invalid'})
        except Exception as e:
            return JsonResponse({'success': False, 'status': 500, 'message': str(e)})


class GRN(JSONApi):
    @staticmethod
    def post(request):
        try:
            items_list = request.data['item_list']
            stock_point = StockPoint.objects.get(client_pk_id=request.data['stock_point_id'])
            destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.STAGING_AREA,
                                                  bay__stock_point=stock_point)
            transaction_id = request.data['transaction_id']
            success, exception = create_stock_movement_without_reservation(request=request, items=items_list,
                                                                           movement_type="inward",
                                                                           adjustment_type=StockAdjustmentType.GRN,
                                                                           destination_shelf=destination_shelf,
                                                                           content_model=None,
                                                                           remarks=transaction_id)
            if success == 0:
                raise Exception(str(exception))
            return JsonResponse({"success": True, "created": True, "message": "GRN  Successful"})
        except Exception as e:
            return JsonResponse({"success": False, "created": False, "message": "GRN Unsuccessful"})


def movement_func(request, adjustment_type, movement_type, items=None, source_shelf=None,
                  destination_shelf=None,
                  content_model=None, remarks=None):
    with transaction.atomic():
        try:
            stock_movement_outward_obj = None
            stock_movement_inward_obj = None
            content_type = None
            if content_model is not None:
                content_type = ContentType.objects.get_for_model(model=content_model, for_concrete_model=False)
            for item_detail in items:
                item = Item.objects.get(sku_code=item_detail['sku_code'])
                if SerialNumber.objects.filter(Q(hash__in=item_detail['usns'])).exists():
                    adjusted_qty = len(item_detail['usns'])
                    if movement_type == 'outward' or movement_type == 'both':
                        stock_source_obj = Stock.objects.get(
                            item_id=item.id,
                            shelf=source_shelf)
                        before_quantity = stock_source_obj.quantity
                        stock_source_obj.quantity -= adjusted_qty
                        stock_source_obj.save()
                        adjustment_data_out = {
                            "item": item.sku_code, "created_by": CustomUser.objects.get(id=2),
                            "associated_transaction_id": '',
                            "associated_transaction": content_type,
                            "quantity_before_adjust": before_quantity,
                            "quantity_after_adjust": stock_source_obj.quantity,
                            "adjustment_type": adjustment_type,
                            "from_shelf": source_shelf,
                            "to_shelf": destination_shelf, "created_at": datetime.datetime.now(),
                            "quantity": adjusted_qty, "remarks": remarks if remarks is None else ''}

                        stock_movement_outward_obj = StockMovementOutward.objects.create(
                            **adjustment_data_out)
                    if movement_type == 'inward' or movement_type == 'both':
                        stock_destination_obj, created = Stock.objects.get_or_create(
                            item_id=item.id,
                            shelf=destination_shelf)
                        if created:
                            stock_destination_obj.quantity = 0
                        before_quantity = stock_destination_obj.quantity
                        stock_destination_obj.quantity += adjusted_qty
                        stock_destination_obj.save()
                        adjustment_data_in = {"item": item.sku_code,
                                              "created_by": CustomUser.objects.get(id=2),
                                              "associated_transaction_id": '',
                                              "associated_transaction": content_type,
                                              "quantity_before_adjust": before_quantity,
                                              "quantity_after_adjust": stock_destination_obj.quantity,
                                              "adjustment_type": adjustment_type,
                                              "from_shelf": source_shelf, "to_shelf": destination_shelf,
                                              "created_at": datetime.datetime.now(), "quantity": adjusted_qty,
                                              "remarks": remarks if remarks is None else ''}
                        stock_movement_inward_obj = StockMovementInward.objects.create(**adjustment_data_in)
                    for usn in item_detail['usns']:
                        serial_obj = SerialNumber.objects.get(hash=usn, item=item)
                        serial_obj.shelf = destination_shelf
                        serial_obj.save()
                        if movement_type == 'both' or movement_type == 'inward':
                            stock_movement_inward_obj.serials.add(serial_obj)
                        if movement_type == 'both' or movement_type == 'outward':
                            stock_movement_outward_obj.serials.add(serial_obj)
            return 1, ''
        except SerialNumber.DoesNotExist:
            transaction.set_rollback(True)
            return 0, 'No Lines are created scan unsuccessful'
        except Exception as e:
            transaction.set_rollback(True)
            return 0, str(e)


class WIPMovement(MultipartApi):
    @staticmethod
    def post(request):
        file = request.FILES['excel']
        data = excel_extraction(file)
        line = 0
        count = 0
        exceptions = list()
        try:
            with transaction.atomic():
                for row in data:
                    temp_dict = {}
                    for serial in SerialNumber.objects.filter(hash=row[0]).exclude(
                            Q(shelf=None) & Q(
                                shelf__name__in=VirtualShelf.objects.exclude(name='Staging Area').values_list('name',
                                                                                                              flat=True))).values(
                        'hash', 'item__sku_code',
                        'shelf_id'):
                        barcode = serial['item__sku_code']
                        shelf = serial['shelf_id']
                        if barcode in temp_dict:
                            if shelf in temp_dict[barcode]:
                                temp_dict[barcode][shelf].append(serial['hash'])
                            else:
                                temp_dict[barcode][shelf] = [serial['hash']]
                        else:
                            temp_dict[barcode] = {shelf: [serial['hash']]}
                    for barcode, shelf_details in temp_dict.items():
                        try:
                            for shelf, hash in shelf_details.items():
                                item = Item.objects.get(sku_code=barcode)
                                source_shelf = Shelf.objects.get(id=shelf)
                                shelf_obj = Shelf.objects.get(
                                    VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                    bay__stock_point_id=33434)
                                usns = SerialNumber.objects.filter(item=item, shelf=source_shelf,
                                                                   hash__in=hash).values_list('hash', flat=True)
                                move_items = [{'sku_code': barcode, 'usns': usns}]
                                created, exception = movement_func(
                                    request=CustomUser.objects.get(id=2),
                                    items=move_items,
                                    movement_type="both",
                                    adjustment_type=StockAdjustmentType.MANUAL,
                                    source_shelf=source_shelf,
                                    destination_shelf=shelf_obj,
                                    content_model=None,
                                    remarks='')
                                if created == 0:
                                    print(exception)
                        except Exception as e:
                            exceptions.append(
                                'Records could not be imported !!! Exception raised on line no-{} {}'.format(line, e))
                            count = 0
        except Exception as e:
            exceptions.append('Records could not be imported !!! Exception raised on line no-{} {}'.format(line, e))
            count = 0
        if len(exceptions) > 0:
            return JsonResponse(
                {"success": False, "status": 500,
                 "message": "Excel File Upload Not Success", "exceptions": exceptions, 'count': count})
        else:
            return JsonResponse(
                {"success": True, "status": 200,
                 "message": "Excel File Success", "exceptions": exceptions, 'count': count})


class ReplacementUsn(JSONApi):
    @staticmethod
    def post(request):
        with transaction.atomic():
            try:
                data = request.data
                task_id = ''
                pick_slip_id = data['pick_slip_id']
                usns = data['usns']
                ##checking for wip
                for usn in usns:
                    if not SerialNumber.objects.filter(hash=usn, shelf__name='Work In Progress').exists():
                        raise Exception(f'{usn} is not in wip to proceed replacement')
                transformed_items = transform_serials_to_item_with_serials(usns)
                stock_point_id = StockPoint.objects.get(client_pk_id=request.data['stock_point_id']).id
                order_id = pick_slip_id
                task_id = task_id
                remarks = f'{pick_slip_id}'
                put_away_task = Task.objects.create(type=Task.Type.PUT_AWAY,
                                                    remarks=remarks, valid=True,
                                                    item_details=transformed_items,
                                                    operation=Task.Operation.REPLACEMENT_TASK,
                                                    associated_transaction=pick_slip_id)
                if put_away_task:
                    picking_obj = WarehouseProcessType(transformed_items, request, order_id=order_id,
                                                       task_id=task_id, shelf_id='',
                                                       remarks=remarks, force_allocate=False,
                                                       stock_point_id=stock_point_id).get_instance(
                        'picking',
                        'pick_slip')
                    picking_result, picking_exceptions = picking_obj.create_reservation()
                    if len(picking_exceptions) > 0:
                        transaction.set_rollback(True)
                        raise Exception(picking_exceptions)
                    return JsonResponse(
                        {"success": True, "status": 200,
                         "message": {'picking_id': picking_result, 'put_away_id': put_away_task.id}})

                else:
                    transaction.set_rollback(True)
                    raise Exception('Something Wrong')

            except Exception as e:
                transaction.set_rollback(True)
                return JsonResponse(
                    {"success": False, "status": 500,
                     "message": str(e)})


class DesignTransfer(JSONApi):
    @staticmethod
    @transaction.atomic()
    def post(request):
        usn_s = request.data['usns']
        remarks = request.data['remarks']
        virtual_shelfs = VirtualShelf.objects.values_list('shelf_ptr_id', flat=True)
        stock_point_id = StockPoint.objects.get(client_pk_id=request.data['stock_point_id'])
        for usn_detail in usn_s:
            try:
                with transaction.atomic():
                    serial = SerialNumber.objects.get(hash=usn_detail['hash'], shelf__bay__stock_point=stock_point_id)
                    if serial.item == None:
                        raise Exception(
                            f'Item Not Yet Mapped to proceed design transfer for Serial {usn_detail["hash"]}')
                    if serial.shelf.id in virtual_shelfs:
                        raise Exception(
                            f'Serial {usn_detail["hash"]} Should Be In Physical Shelf To Proceed Design Transfer')
                    previous_item = Item.objects.get(sku_code=usn_detail['previous_sku_code'])
                    next_item = Item.objects.get(sku_code=usn_detail['changed_sku_code'])
                    if serial.item == next_item:
                        raise Exception(f'Design Transfer Not possible for the same item {serial.item.sku_code}')
                    # outward
                    outward_stock = Stock.objects.get(item=previous_item, shelf=serial.shelf)
                    before_quantity = outward_stock.quantity
                    if outward_stock.quantity > 0:
                        outward_stock.quantity -= 1
                    else:
                        raise Stock.DoesNotExist
                    outward_stock.save()
                    adjustment_data_out = {
                        "item": previous_item, "created_by": request.user,
                        "associated_transaction_id": '',
                        "quantity_before_adjust": before_quantity,
                        "quantity_after_adjust": outward_stock.quantity,
                        "adjustment_type": StockAdjustmentType.DESIGN_TRANSFER,
                        "from_shelf": serial.shelf,
                        "to_shelf": serial.shelf, "created_at": datetime.datetime.now(),
                        "quantity": 1, "remarks": remarks}

                    stock_movement_outward_obj = StockMovementOutward.objects.create(
                        **adjustment_data_out)

                    # inward
                    inward_stock = Stock.objects.get_or_create(item=next_item, shelf=serial.shelf)[0]
                    before_quantity = inward_stock.quantity
                    inward_stock.quantity += 1
                    inward_stock.save()
                    adjustment_data_in = {
                        "item": next_item, "created_by": request.user,
                        "associated_transaction_id": '',
                        "quantity_before_adjust": before_quantity,
                        "quantity_after_adjust": inward_stock.quantity,
                        "adjustment_type": StockAdjustmentType.DESIGN_TRANSFER,
                        "from_shelf": serial.shelf,
                        "to_shelf": serial.shelf, "created_at": datetime.datetime.now(),
                        "quantity": 1, "remarks": remarks}

                    stock_movement_inward_obj = StockMovementInward.objects.create(
                        **adjustment_data_in)

                    serial.item = next_item
                    serial.save()
                    stock_movement_outward_obj.serials.add(serial)
                    stock_movement_inward_obj.serials.add(serial)
            except SerialNumber.DoesNotExist:
                transaction.set_rollback(True)
                return JsonResponse(
                    {"success": False, "status": 500,
                     "message": 'Check the USN and Stock Point mapped'})
            except Exception as e:
                transaction.set_rollback(True)
                return JsonResponse(
                    {"success": False, "status": 500,
                     "message": str(e)})
        return JsonResponse(
            {"success": True, "status": 200,
             "message": 'Design Transfer Ok'})


#
# class ShelfTree(JSONApi):
#     @staticmethod
#     def get(request):
#         stock_point_id = 33434
#         shelves = list(
#             Shelf.objects.filter(bay__type=Bay.BayType.PHYSICAL, bay__stock_point_id=stock_point_id).values('id',
#                                                                                                             'name'))
#         return JsonResponse({'data': [{
#             'id': 'node_2',
#             'text': 'Root node with options',
#             'icon': 'false',
#             'children': [{'text': 'Child 1', 'icon': 'false'},
#                          {'text': 'Child 2', 'icon': 'false'}],
#
#         },
#             {
#                 'id': '',
#                 'text': 'Root node with options',
#                 'icon': 'false',
#                 'children': [{'text': 'Child 1', 'icon': 'false'},
#                              {'text': 'Child 2', 'icon': 'false'}, ],
#             }]})
#
#
class GetStockDetails(JSONApi):
    @staticmethod
    def get(request):
        barcode = request.GET.get('barcode', None)
        stock_point_id = request.GET.get('stock_point_id', None)
        exclude_shelfs = []
        for shelf_obj in Shelf.VirtualShelfTypes:
            if shelf_obj.name != 'STAGING_AREA':
                exclude_shelfs.append(shelf_obj)
        if barcode is None:
            return JsonResponse({'Please Provide Barcode Details'})
        if stock_point_id is None:
            return JsonResponse({'Please Provide StockPoint Details'})
        else:
            base_reserve_query = StockReservation.objects.filter(task__valid=True,
                                                                 item__sku_code=barcode).annotate(
                taskid=Cast('task__id', output_field=CharField()))
            task_ids = base_reserve_query.values_list('taskid', flat=True).distinct('taskid')
            picked_quantity = StockMovementInward.objects.filter(associated_transaction_id__in=task_ids,
                                                                 adjustment_type=StockAdjustmentType.PICKING,
                                                                 item=barcode).values(
                'quantity').aggregate(total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))['total']

            total_reserved_stock = \
                base_reserve_query.aggregate(total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))[
                    'total']
            base_stock_obj = Stock.objects.filter(Q(item__sku_code=barcode) &
                                                  Q(shelf__bay__stock_point_id=stock_point_id)
                                                  ).values('quantity').aggregate(
                current_stock=Coalesce(Sum('quantity', filter=Q(
                    shelf__is_picking_blocked=False) & ~Q(shelf__virtualshelf__type__in=exclude_shelfs)), 0,
                                       output_field=DecimalField()))
            current_stock = base_stock_obj['current_stock']
            '''
            formula applied current_stock-effective_reserved_stock
            '''
            stock_available = (float(current_stock) - float(total_reserved_stock - picked_quantity))
            return JsonResponse(
                {'current_stock': current_stock, 'wip_stock': picked_quantity,
                 'reserved_stock': total_reserved_stock - picked_quantity,
                 'available': stock_available})


class ShelfTree(JSONApi):
    @staticmethod
    def get(request, warehouse_id):
        '''
        todo
        need to add excess filter code
        '''
        warehouse_obj = Warehouse.objects.get(id=warehouse_id)
        stock_points = StockPoint.objects.filter(warehouse=warehouse_obj).values('id', 'name')
        shelves = \
            Shelf.objects.filter(bay__type=Bay.BayType.PHYSICAL, bay__stock_point__warehouse=warehouse_obj).annotate(
                bay_name=F('bay__name'), warehouse_name=F('bay__stock_point__warehouse__name'),
                stock_point_id=F('bay__stock_point_id'), stock_point_name=F('bay__stock_point__name')).values('id',
                                                                                                              'name',
                                                                                                              'bay_name',
                                                                                                              'warehouse_name',
                                                                                                              'stock_point_id',
                                                                                                              'stock_point_name').order_by(
                'warehouse_name', 'stock_point_name', 'bay_name', 'name')
        final_data = [{'id': '',
                       'text': warehouse_obj.name,
                       'icon': 'false',
                       'children': []}]
        for stock_point in stock_points:
            grouping_bay_shelf = {}
            for shelf_detail in shelves.filter(stock_point_id=stock_point['id']):
                if shelf_detail['bay_name'] in grouping_bay_shelf:
                    grouping_bay_shelf[shelf_detail['bay_name']].append(
                        {'text': shelf_detail['name'], 'icon': 'false', 'id': shelf_detail['id']})
                else:
                    grouping_bay_shelf[shelf_detail['bay_name']] = [
                        {'text': shelf_detail['name'], 'icon': 'false', 'id': shelf_detail['id']}]

            if len(grouping_bay_shelf):
                final_data[0]['children'].append({'id': '',
                                                  'text': stock_point['name'],
                                                  'icon': 'false',
                                                  'children': [{'id': '',
                                                                'text': bay,
                                                                'icon': 'false',
                                                                'children': shelf_details} for bay, shelf_details in
                                                               grouping_bay_shelf.items()]})

        return JsonResponse({'data': final_data, 'grouped_bay_shelf_data': list(shelves)
                             })
