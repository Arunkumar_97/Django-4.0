from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from .models import StockPoint, Shelf, PhysicalShelf, VirtualShelf, Stock, Warehouse


class WarehouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Warehouse
        fields = '__all__'


class StockPointSerializer(serializers.ModelSerializer):
    warehouse_id = serializers.CharField(source='warehouse.id')
    warehouse_name = serializers.CharField(source='warehouse.name')

    class Meta:
        model = StockPoint
        fields = ('name', 'warehouse_id', 'id', 'warehouse_name')


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = '__all__'


class ShelfSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shelf
        fields = ('name', 'stock_point')


class VirtualShelfSerializer(serializers.ModelSerializer):
    class Meta:
        model = VirtualShelf
        fields = ('name', 'stock_point')


class PhysicalShelfSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhysicalShelf
        fields = ('name', 'stock_point', 'capacity')


class ShelfPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Shelf: ShelfSerializer,
        VirtualShelf: VirtualShelfSerializer,
        PhysicalShelf: PhysicalShelfSerializer
    }
