from django.contrib import admin
from django.db.models import Count, IntegerField
from polymorphic.admin import PolymorphicChildModelAdmin, PolymorphicParentModelAdmin, PolymorphicChildModelFilter
from rangefilter.filters import DateRangeFilter

from inventory.models import *
from import_export.admin import ExportActionMixin, ExportMixin, ImportExportModelAdmin
from import_export import resources
from import_export.fields import Field


# Register your models here.


def sku_code(obj):
    if obj.item:
        return obj.item.sku_code
    else:
        return ''


class SerialResource(resources.ModelResource):
    list_display = (
        'id', 'name', 'type', 'stock_point', 'get_warehouse', 'bay_no', 'distance_factor', 'max_limit', 'filled',
        'balance_qty', 'available_percentage', 'is_healthy')
    search_fields = ('name', 'stock_point__name', 'id')
    list_filter = ('stock_point', 'stock_point__warehouse')

    usn = Field(attribute='hash', column_name='hash')
    skucode = Field(attribute='item__sku_code', column_name='skucode')
    shelf_name = Field(attribute='shelf__name', column_name='shelfName')
    bay = Field(attribute='shelf__bay__name', column_name='bay')
    stock_point = Field(attribute='shelf__bay__stock_point__name', column_name='stockPoint')


@admin.register(SerialNumber)
class SerialNumberAdmin(ExportActionMixin, admin.ModelAdmin):
    list_display = ('id', 'hash', sku_code, 'batch', 'shelf', 'created_at')
    search_fields = ('hash', 'item__sku_code',)
    raw_id_fields = ('item',)
    list_filter = ('shelf',)
    resource_class = SerialResource


@admin.register(SerialNumberAudit)
class SerialNumberAuditAdmin(admin.ModelAdmin):
    list_display = ('id',)


class ModelAChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Shelf  # Optional, explicitly set here.

    # By using these `base_...` attributes instead of the regular ModelAdmin `form` and `fieldsets`,
    # the additional fields of the child models are automatically added to the admin form.


@admin.register(VirtualShelf)
class ModelBAdmin(ModelAChildAdmin):
    base_model = VirtualShelf  # Explicitly set here!
    # define custom features here


# class ShelfResource(resources.ModelResource):
#     shelf_id = Field(attribute='id', column_name='id')
#     shelf_name = Field(attribute='name', column_name='shelf')
#     quantity = Field(attribute='capacity', column_name='capacity')
#     bay = Field(attribute='bay__name', column_name='bay')


class PhysicalShelfResource(resources.ModelResource):
    filled = Field()
    balance_qty = Field()
    available_percentage = Field()

    class Meta:
        model = Shelf
        fields = ('id', 'name', "capacity", "filled", "balance_qty", "available_percentage")
        export_order = ('id', 'name', "capacity", "filled", "balance_qty", "available_percentage")

    def dehydrate_balance_qty(self, shelf):
        return '%s' % shelf.balance_qty

    def dehydrate_filled(self, shelf):
        return '%s' % shelf.filled

    def dehydrate_available_percentage(self, shelf):
        return '%s' % shelf.available_percentage


@admin.register(PhysicalShelf)
class PhysicalShelfAdmin(ExportActionMixin, admin.ModelAdmin):

    def get_warehouse(self, instance):
        return instance.bay.stock_point.warehouse

    def max_limit(self, instance):
        return instance.capacity

    def filled(self, instance):
        stock_node = Shelf.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf=stock_node).count()
        return items_count

    def balance_qty(self, instance):
        stock_node = Shelf.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf=stock_node).count()
        return instance.capacity - items_count

    def available_percentage(self, instance):
        stock_node = Shelf.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf=stock_node).count()
        return round(items_count / instance.capacity * 100, 0)

    list_display = ('id', 'name', 'get_warehouse', 'max_limit', 'filled', 'balance_qty', 'available_percentage',)
    search_fields = ('name', 'id')
    list_filter = ('bay__stock_point', 'bay__stock_point__warehouse')
    resources = PhysicalShelfResource


@admin.register(Shelf)
class ModelAParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Shelf  # Optional, explicitly set here.
    child_models = (VirtualShelf, PhysicalShelf)
    search_fields = ('name',)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.


class StockResource(resources.ModelResource):
    item = Field(attribute='item__sku_code', column_name='SkuCode')
    shelf_name = Field(attribute='shelf__name', column_name='shelfName')
    stock_point = Field(attribute='shelf__bay__stock_point__name', column_name='stockPoint')
    quantity = Field(attribute='quantity', column_name='quantity')


@admin.register(Stock)
class StockAdmin(ExportActionMixin, admin.ModelAdmin):
    search_fields = ('item__sku_code',)
    list_display = ('item', 'shelf', 'quantity')
    raw_id_fields = ('item',)
    list_filter = ('shelf__bay__stock_point',)


@admin.register(PickerConfig)
class PickerConfigModel(admin.ModelAdmin):
    list_display = ('type',)


class StockMovementInwardResource(resources.ModelResource):
    class Meta:
        def username(self, instance):
            return instance.created_by.username

        model = StockMovementInward
        fields = ('item',
                  'created_at', 'created_by__username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
                  'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'remarks')
        export_order = (
            'created_at', 'created_by__username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
            'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'remarks')


@admin.register(StockMovementInward)
class StockMovementInward(ExportMixin, admin.ModelAdmin):

    def created_at(self, instance):
        return instance.created_at

    def username(self, instance):
        return instance.created_by.username

    def show_usns(self, instance):
        return ','.join(instance.serials.values_list('hash', flat=True))

    list_display = ('item',
                    'created_at', 'username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
                    'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'show_usns', 'remarks')
    search_fields = ('serials__item__sku_code', 'associated_transaction_id', 'serials__hash', 'remarks')
    list_filter = (('created_at', DateRangeFilter), 'adjustment_type', 'from_shelf', 'to_shelf',)
    list_display_links = ('item',)
    resources = StockMovementInwardResource


class StockMovementOutwardesource(resources.ModelResource):
    class Meta:
        model = StockMovementOutward
        fields = ('item',
                  'created_at', 'created_by__username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
                  'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'remarks')
        export_order = (
            'created_at', 'created_by__username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
            'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'remarks')


@admin.register(StockMovementOutward)
class StockMovementOutward(ExportMixin, admin.ModelAdmin):
    def created_at(self, instance):
        return instance.created_at

    def username(self, instance):
        return instance.created_by.username

    def show_usns(self, instance):
        return ','.join(instance.serials.values_list('hash', flat=True))

    list_display = ('item',
                    'created_at', 'username', 'quantity_before_adjust', 'quantity', 'quantity_after_adjust',
                    'from_shelf', 'to_shelf', 'associated_transaction_id', 'adjustment_type', 'show_usns', 'remarks')
    search_fields = ('serials__item__sku_code', 'associated_transaction_id', 'serials__hash', 'remarks')
    list_filter = (('created_at', DateRangeFilter), 'adjustment_type', 'from_shelf', 'to_shelf',)
    list_display_links = ('item',)
    resource_class = StockMovementOutwardesource


@admin.register(StockPoint)
class StockPointModel(admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(Warehouse)
class WarehouseAdmin(admin.ModelAdmin):
    def filled(self, instance):
        ware_house = Warehouse.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf__bay__stock_point__warehouse=ware_house).count()
        return items_count

    def balance_qty(self, instance):
        ware_house = Warehouse.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf__bay__stock_point__warehouse=ware_house).count()
        shelf_capacity = Shelf.objects.filter(bay__stock_point__warehouse=ware_house). \
            aggregate(total_capacity=Coalesce(Sum('physicalshelf__capacity'), 0, output_field=IntegerField()))['total_capacity']
        return shelf_capacity - items_count

    def available_percentage(self, instance):
        ware_house = Warehouse.objects.get(id=instance.id)
        items_count = SerialNumber.objects.filter(shelf__bay__stock_point__warehouse=ware_house).count()
        shelf_capacity = Shelf.objects.filter(bay__stock_point__warehouse=ware_house). \
            aggregate(total_capacity=Coalesce(Sum('physicalshelf__capacity'), 0, output_field=IntegerField()))['total_capacity']
        return round(items_count / shelf_capacity * 100, 0)

    list_display = ('id', 'name', 'filled', 'balance_qty', 'available_percentage',)
    search_fields = ('name', 'id')
    list_filter = ('id', 'name')


admin.site.register(Bay)
admin.site.register(Batch)
admin.site.register(StockAudit)
