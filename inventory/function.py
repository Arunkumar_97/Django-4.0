import datetime
import json
import time
import pytz
import requests
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Sum, F, DecimalField
from django.db.models.functions import Coalesce

from WMS.settings import client_machine_url

from dispatch.models import StockReservation, StockSerial, Task, Picker, NotFoundAudit
from inventory.models import SerialNumber, StockMovementInward, \
    StockMovementOutward, Stock, Shelf, Bay, StockAudit

from master.models import Item
from django.db import transaction

import logging

from sync.models import MasterSyncHistory, Modules, Servers

logger = logging.getLogger(__name__)


def shelf_check(source_shelf, usns: list):
    for usn in usns:
        serial_shelf = SerialNumber.objects.get(hash=usn)
        if serial_shelf.shelf == source_shelf and serial_shelf.shelf is not None:
            usns.remove(usn)
    return usns


def create_stock_movement(self, adjustment_type=None, source_shelf=None,
                          destination_shelf=None, movement_type=None, remarks=None,shelf_valid=True):
    with transaction.atomic():
        try:
            stock_movement_outward_obj = None
            stock_movement_inward_obj = None
            task = Task.objects.get(id=self.task_id)
            content_type = ContentType.objects.get_for_model(task,
                                                             for_concrete_model=False)
            for item_detail in self.items:
                if shelf_valid:
                    item_detail['usns'] = shelf_check(destination_shelf, item_detail['usns'])
                    if len(item_detail['usns']) == 0:
                        break
                item = Item.objects.get(sku_code=item_detail['sku_code'])
                stock_reservation = StockReservation.objects.get(task_id=self.task_id, item=item,
                                                                 shelf=source_shelf)
                reserve_qty = StockReservation.objects.filter(task_id=self.task_id, item=item).aggregate(
                    total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                outward_usns = []
                inward_usns = []
                picked = StockMovementInward.objects.filter(associated_transaction_id=self.task_id,
                                                            item=item.sku_code, to_shelf=destination_shelf) \
                    .aggregate(total_count=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                if reserve_qty['total'] <= picked['total_count']:
                    raise Exception('Picking completed for the reservation')
                if stock_reservation.quantity > 0:
                    if SerialNumber.objects.filter(
                            Q(hash__in=item_detail['usns']) & Q(shelf=source_shelf)).exists():
                        if int(item_detail['quantity']) == len(item_detail['usns']) and len(
                                item_detail['usns']) <= stock_reservation.quantity:
                            for usn in item_detail['usns']:
                                serial_obj = SerialNumber.objects.get(hash=usn, item=item)
                                picker = Picker.objects.get(picker__id=self.request.user.id)
                                StockSerial.objects.create(usn=serial_obj, stock_reservation=stock_reservation,
                                                           picker=picker,
                                                           picked_time=datetime.datetime.now())
                                serial_obj.shelf = destination_shelf
                                serial_obj.save()
                                if movement_type == 'both' or movement_type == 'inward':
                                    inward_usns.append(serial_obj)
                                if movement_type == 'both' or movement_type == 'outward':
                                    outward_usns.append(serial_obj)
                            adjusted_qty = 1
                            if movement_type == 'outward' or movement_type == 'both':
                                stock_source_obj = Stock.objects.get(item_id=stock_reservation.item_id,
                                                                     shelf=source_shelf)
                                before_quantity = stock_source_obj.quantity
                                stock_source_obj.quantity -= adjusted_qty
                                stock_source_obj.save()
                                adjustment_data_out = {"item": item.sku_code,
                                                       "created_by": self.request.user,
                                                       "associated_transaction_id": self.task_id,
                                                       "content_type": content_type,
                                                       "quantity_before_adjust": before_quantity,
                                                       "quantity_after_adjust": stock_source_obj.quantity,
                                                       "adjustment_type": adjustment_type,
                                                       "from_shelf": source_shelf,
                                                       "to_shelf": destination_shelf,
                                                       "created_at": datetime.datetime.now(),
                                                       "quantity": adjusted_qty,
                                                       "remarks": remarks if remarks is None else ''
                                                       }
                                stock_movement_outward_obj = StockMovementOutward.objects.create(
                                    **adjustment_data_out)
                            if movement_type == 'inward' or movement_type == 'both':
                                stock_destination_obj, created = Stock.objects.get_or_create(
                                    item_id=stock_reservation.item_id,
                                    shelf=destination_shelf)
                                before_quantity = stock_destination_obj.quantity
                                stock_destination_obj.quantity += adjusted_qty
                                stock_destination_obj.save()
                                adjustment_data_in = {
                                    "item": item.sku_code,
                                    "created_by": self.request.user, "associated_transaction_id": self.task_id,
                                    "content_type": content_type,
                                    "quantity_before_adjust": before_quantity,
                                    "quantity_after_adjust": stock_destination_obj.quantity,
                                    "adjustment_type": adjustment_type,
                                    "from_shelf": source_shelf, "to_shelf": destination_shelf,
                                    "created_at": datetime.datetime.now(), "quantity": adjusted_qty,
                                    "remarks": remarks if remarks is None else ''
                                }
                                stock_movement_inward_obj = StockMovementInward.objects.create(**adjustment_data_in)
                            if movement_type == 'both' or movement_type == 'inward':
                                for serial in inward_usns:
                                    stock_movement_inward_obj.serials.add(serial)
                            if movement_type == 'both' or movement_type == 'outward':
                                for serial in outward_usns:
                                    stock_movement_outward_obj.serials.add(serial)
                            task = Task.objects.get(id=stock_reservation.task_id)
                            reserved_qty = StockReservation.objects.filter(task_id=self.task_id,
                                                                           item=item).aggregate(
                                total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                            total_picked = StockMovementInward.objects.filter(
                                associated_transaction_id=self.task_id,
                                item=item.sku_code) \
                                .aggregate(total_count=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                            if reserved_qty['total'] == total_picked['total_count']:
                                StockReservation.objects.filter(task_id=self.task_id, item=item).update(valid=False)
                            if task.is_reservation_completed:
                                task.picked_time = datetime.datetime.now()
                                task.task_reserve.all().update(valid=False)
                                task.valid = False
                                task.save()
                                self.assign_picker()
                        return 1, ''
                else:
                    raise StockReservation.DoesNotExist
        except StockReservation.DoesNotExist:
            logger.debug(f'Reserved Quantity 0 for task {task.id}')
            transaction.set_rollback(True)
            return 0, 'Reservation is full'
        except Exception as e:
            logger.debug(f'Issue during Movement for task {task.id} {str(e)}')
            transaction.set_rollback(True)
            return 0, str(e)


def create_stock_movement_without_reservation(request, adjustment_type, movement_type, items=None, source_shelf=None,
                                              destination_shelf=None,
                                              content_model=None, remarks=None, is_return=False, task=None,
                                              shelf_valid=True):
    with transaction.atomic():
        try:
            stock_movement_outward_obj = None
            stock_movement_inward_obj = None
            associated_transaction = ''
            content_type = None
            if task is not None:
                associated_transaction = task
            if content_model is not None:
                content_type = ContentType.objects.get_for_model(model=content_model, for_concrete_model=False)
            for item_detail in items:
                if shelf_valid:
                    item_detail['usns'] = shelf_check(destination_shelf, item_detail['usns'])
                    if len(item_detail['usns']) == 0:
                        break
                item = Item.objects.get(sku_code=item_detail['sku_code'])
                if SerialNumber.objects.filter(Q(hash__in=item_detail['usns'])).exists():
                    adjusted_qty = len(item_detail['usns'])
                    if movement_type == 'outward' or movement_type == 'both':
                        if not is_return and source_shelf.name == 'Dispatched':
                            raise Exception('Outward Movement cannot be done for Dispatched Items')
                        stock_source_obj = Stock.objects.get(
                            item_id=item.id,
                            shelf=source_shelf)
                        before_quantity = stock_source_obj.quantity
                        stock_source_obj.quantity -= adjusted_qty
                        stock_source_obj.save()
                        adjustment_data_out = {
                            "item": item.sku_code, "created_by": request.user,
                            "associated_transaction_id": associated_transaction,
                            "content_type": content_type,
                            "quantity_before_adjust": before_quantity,
                            "quantity_after_adjust": stock_source_obj.quantity,
                            "adjustment_type": adjustment_type,
                            "from_shelf": source_shelf,
                            "to_shelf": destination_shelf, "created_at": datetime.datetime.now(),
                            "quantity": adjusted_qty, "remarks": remarks if remarks is not None else ''}

                        stock_movement_outward_obj = StockMovementOutward.objects.create(
                            **adjustment_data_out)
                    if movement_type == 'inward' or movement_type == 'both':
                        stock_destination_obj, created = Stock.objects.get_or_create(
                            item_id=item.id,
                            shelf=destination_shelf)
                        if created:
                            stock_destination_obj.quantity = 0
                        before_quantity = stock_destination_obj.quantity
                        stock_destination_obj.quantity += adjusted_qty
                        stock_destination_obj.save()
                        adjustment_data_in = {"item": item.sku_code,
                                              "created_by": request.user,
                                              "associated_transaction_id": associated_transaction,
                                              "content_type": content_type,
                                              "quantity_before_adjust": before_quantity,
                                              "quantity_after_adjust": stock_destination_obj.quantity,
                                              "adjustment_type": adjustment_type,
                                              "from_shelf": source_shelf, "to_shelf": destination_shelf,
                                              "created_at": datetime.datetime.now(), "quantity": adjusted_qty,
                                              "remarks": remarks if remarks is not None else ''}
                        stock_movement_inward_obj = StockMovementInward.objects.create(**adjustment_data_in)
                    for usn in item_detail['usns']:
                        serial_obj = SerialNumber.objects.get(hash=usn, item=item)
                        serial_obj.shelf = destination_shelf
                        serial_obj.save()
                        if movement_type == 'both' or movement_type == 'inward':
                            stock_movement_inward_obj.serials.add(serial_obj)
                        if movement_type == 'both' or movement_type == 'outward':
                            stock_movement_outward_obj.serials.add(serial_obj)
                else:
                    raise SerialNumber.DoesNotExist
            return 1, ''
        except SerialNumber.DoesNotExist:
            logger.debug(f'Issue during Movement without reservation {remarks} serials does not exist {items}')
            transaction.set_rollback(True)
            return 0, 'No Lines are created scan unsuccessful'
        except Exception as e:
            logger.debug(f'Issue during Movement without reservation {remarks} {str(e)}')
            transaction.set_rollback(True)
            return 0, str(e)


def create_new_item_reservation(self, items=None, source_shelf=None, destination_shelf=None):
    response_list = list()
    try:
        for item_detail in items:
            item = Item.objects.get(sku_code=item_detail['sku_code'])
            required_quantity = item_detail['quantity']
            if required_quantity > 0:
                reserved_dict = {data['shelf_id']: data['quantity'] for data in
                                 (StockReservation.objects.filter(item=item, valid=True).exclude(shelf=source_shelf)
                                  .values('shelf_id', 'item__sku_code').annotate(quantity=Sum('quantity')))}
                available_physical_shelves = Stock.objects \
                    .filter(~Q(shelf=source_shelf) &
                            Q(item=item, quantity__gt=0) & Q(shelf__bay__type=Bay.BayType.PHYSICAL)
                            & Q(shelf__is_picking_blocked=False) & Q(shelf__bay__stock_point_id=self.stock_point_id)) \
                    .values('shelf_id', 'quantity') \
                    .distinct().order_by('-quantity')
                available_virtual_shelves = Stock.objects \
                    .filter(~Q(shelf=destination_shelf) & Q(shelf__name='Staging Area') &
                            Q(item=item, quantity__gt=0) & Q(shelf__bay__type=Bay.BayType.VIRTUAL)
                            & Q(shelf__bay__stock_point_id=self.stock_point_id)) \
                    .values('shelf_id', 'quantity') \
                    .distinct().order_by('-quantity')
                available_shelves = available_physical_shelves.union(available_virtual_shelves, all=True)
                nf_audit = NotFoundAudit(item=Item.objects.get(sku_code=item_detail['sku_code']),
                                         shelf=source_shelf,
                                         associated_task_id=self.task_id, quantity=required_quantity,
                                         picker=Picker.objects.get(picker=self.request.user))
                nf_audit.save()
                for new_shelf in available_shelves:
                    skip = False
                    if required_quantity > 0:
                        if reserved_dict and new_shelf['shelf_id'] in reserved_dict:
                            shelf_quantity = int(new_shelf['quantity']) - int(
                                reserved_dict[new_shelf['shelf_id']])
                        else:
                            shelf_quantity = int(new_shelf['quantity'])
                        if shelf_quantity >= required_quantity:
                            # create new reservation for new required quantity
                            allocation = StockReservation(task_id=self.task_id,
                                                          item=Item.objects.get(sku_code=item_detail['sku_code']),
                                                          quantity=required_quantity,
                                                          shelf_id=new_shelf['shelf_id'])
                            allocation.save()
                        elif shelf_quantity > 0:
                            # partial reservation from multiple shelf
                            allocation = StockReservation(task_id=self.task_id,
                                                          item=Item.objects.get(sku_code=item_detail['sku_code']),
                                                          quantity=shelf_quantity,
                                                          shelf_id=new_shelf['shelf_id'])
                            allocation.save()
                        else:
                            skip = True
                            pass
                        required_quantity -= shelf_quantity
                    if not skip:
                        reservation = StockReservation.objects.filter(
                            item=Item.objects.get(sku_code=item_detail['sku_code']), task_id=self.task_id,
                            valid=True, shelf_id=new_shelf['shelf_id']) \
                            .values('item__sku_code',
                                    'shelf__name',
                                    'quantity')
                        for item in reservation:
                            response = dict()
                            response['status'] = 'Item Reserved Successfully'
                            response['sku_code'] = item['item__sku_code']
                            response['shelf'] = item['shelf__name']
                            response['quantity'] = int(item['quantity'])
                            response_list.append(response)
        return response_list, ''
    except Exception as e:
        logger.debug(f'Issue during Movement without reservation {self.task_id} {str(e)}')
        transaction.set_rollback(True)
        return [], str(e)


def get_current_shelf_items(shelf):
    item = list()
    current_shelf = SerialNumber.objects.filter(shelf_id=shelf).values('hash')
    for usn in current_shelf:
        item.append(usn)
    return item




def is_item_already_exist_in_shelf_func(sku_code, items):
    for item in items:
        if sku_code == item['sku_code']:
            return True, item
    return False, []


def sync_stock_to_client():
    try:

        filters = {'shelf__is_picking_blocked': False}
        # stock_sync_config = SyncConfig.objects.filter(name='Stock')
        # if stock_sync_config.exists():
        #     stock_ids=StockAudit.objects.filter(updated_at__gte=stock_sync_config.first('updated_at')).value_list('stock_id')
        #     filters['id__in'] = stock_ids
        exclude_shelfs = []
        # excluding wip shelf and grn to consider in stock sync
        '''
        sync_stock_count_formula
        physicalstock+grn+wip
        '''
        for shelf_obj in Shelf.VirtualShelfTypes:
            if shelf_obj.name not in ['WORK_IN_PROGRESS', 'STAGING_AREA']:
                exclude_shelfs.append(shelf_obj)
        stocks = Stock.objects.filter(**filters).exclude(
            shelf__virtualshelf__type__in=exclude_shelfs).annotate(
            stock_point_id=F('shelf__bay__stock_point__client_pk_id'), sku_code=F('item__sku_code')).exclude(
            stock_point_id=None).values('stock_point_id', 'sku_code').order_by('stock_point_id',
                                                                               'sku_code').annotate(
            total_quantity=Sum('quantity'))
        client_data = {}
        for stock in stocks:
            if stock['stock_point_id'] not in client_data:
                client_data[stock['stock_point_id']] = [
                    {'sku_code': stock['sku_code'], 'quantity': float(stock['total_quantity'])}]
            else:
                client_data[stock['stock_point_id']].append(
                    {'sku_code': stock['sku_code'], 'quantity': float(stock['total_quantity'])})

        source_data = [{data: client_data[data]} for data in client_data]
        for i in range(0, len(source_data), 100):
            raw_data = source_data[i:i + 100]
            final_json_data = json.dumps(
                {"raw_data": raw_data})
            client_machine_stock_sync = requests.post(f'{client_machine_url}/api/inventory/sync-stock-from-wms/',
                                                      headers={'content-type': 'application/json',
                                                               'Authorization': "Token 75eecfd544b74a400044a427641734bfa605ad25"},
                                                      json=final_json_data)
            content = json.loads(client_machine_stock_sync.content.decode('utf-8'))
            if client_machine_stock_sync.status_code == 200 and not content['success']:
                print(content)
                time.sleep(1)
            else:
                time.sleep(1)
            return True
    except Exception as e:
        logger.critical(msg=f'Stock Sync Failure \n {str(e)}')
        return False


#

def is_item_exist(items, sku_code):
    for item in items:
        if item['sku_code'] == sku_code:
            return True, items.index(item)
    return False, None


def transfer_serials_to_item(ShelfvsUsn: dict):
    item_usn_dict = dict()
    final_list = list()
    for shelf, usns in ShelfvsUsn.items():
        for usn in usns:
            serial_obj = SerialNumber.objects.get(hash=usn)
            sku_code = serial_obj.item.sku_code
            if shelf in item_usn_dict:
                for shelf, item in item_usn_dict.items():
                    exists, item_index = is_item_exist(item, sku_code)
                    if exists:
                        item_usn_dict[shelf][item_index]['usns'].append(usn)
                    else:
                        item_usn_dict[shelf].append({'sku_code': sku_code, 'usns': [usn]})
            else:
                item_usn_dict[shelf] = [{'sku_code': sku_code, 'usns': [usn]}]
    final_list.append(item_usn_dict)
    return final_list


def get_shelf_usn(shelf, item):
    serials = SerialNumber.objects.filter(shelf_id=shelf, item__sku_code=item).values('hash', 'item__sku_code')
    itemvsusn = dict()
    for serial in serials:
        sku_code = serial['item__sku_code']
        usn = serial['hash']
        if sku_code in itemvsusn:
            itemvsusn[sku_code].append(serial['hash'])
        else:
            itemvsusn[sku_code] = [usn]
    return [{'sku_code': sku_code, 'usns': usns} for sku_code, usns in itemvsusn.items()]
