from WMS.settings import MEDIA_ROOT
from inventory.models import SerialNumber
from master.models import Item
from utils.excel.extraction import  excel_extraction

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Update Child Barcode To Master in Usns'

    def handle(self, *args, **options):
        print('Script Started to Change Child to master in serials')
        data = excel_extraction(MEDIA_ROOT + '/master_child_barcode_mapping.csv.xlsx')
        master_barcodes = []
        child_barcodes=[]
        for row in data:
            if row[0] is not None and row[0] not in master_barcodes:
                master_barcodes.append(row[0])

        child_barcode_mapping = {}
        for row in data:
            count = 0
            current_master = None
            for column in row:
                if count == 0 and column is not None:
                    current_master = column
                    count += 1
                elif count == 1 and column is not None:
                    child_barcode_mapping[column] = current_master

        for barcode in child_barcode_mapping:
            child_barcodes.append(barcode)


        serial_ids = SerialNumber.objects.filter(item__sku_code__in=child_barcodes).values_list('id', flat=True)

        for serial_id in serial_ids:
            serial = SerialNumber.objects.get(id=serial_id)
            barcode = serial.item.sku_code
            if barcode not in master_barcodes:
                if barcode in child_barcode_mapping:
                    serial.item = Item.objects.get(sku_code=child_barcode_mapping[barcode])
                    serial.save()

        print('Script Completed to Change Child to master in serials')
