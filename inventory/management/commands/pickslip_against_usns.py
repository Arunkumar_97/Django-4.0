from django.db.models import CharField
from django.db.models.functions import Cast

from dispatch.functions import transform_serials_to_item_with_serials
from dispatch.models import StockSerial, Task
from inventory.function import create_stock_movement_without_reservation
from inventory.models import *
from django.core.management.base import BaseCommand
import datetime

from core.models import CustomUser

from inventory.models import SerialNumber, StockMovementInward, \
    StockMovementOutward, Stock, Shelf

from master.models import Item


class Command(BaseCommand):
    help = 'PickSlip'

    def add_arguments(self, parser):
        parser.add_argument('--pick_slip_id', nargs='+', type=int)

    def handle(self, *args, **options):
        class dotdict(dict):
            """dot.notation access to dictionary attributes"""
            __getattr__ = dict.get
            __setattr__ = dict.__setitem__
            __delattr__ = dict.__delitem__

        pick_slip_id = options['pick_slip_id']
        filters = {"associated_transaction__in": pick_slip_id}
        task_list = Task.objects.filter(**filters).annotate(task=Cast('id', output_field=CharField())).values_list('task',
                                                                                                                 flat=True)
        serials = StockMovementInward.objects.filter(associated_transaction_id__in=task_list,
                                                     adjustment_type=StockAdjustmentType.PICKING).values_list(
            'serials__hash', flat=True)

        usns = SerialNumber.objects.filter(hash__in=serials,
                                           shelf__virtualshelf__type=Shelf.VirtualShelfTypes.LOST).values_list('hash',flat=True)

        if usns.exists():
            transformed_items = transform_serials_to_item_with_serials(usns)

            lost_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.LOST,
                                           bay__stock_point_id=33434)
            wip_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                          bay__stock_point_id=33434)
            request = dotdict({'user': CustomUser.objects.get(id=1)})
            success, exception = create_stock_movement_without_reservation(request=request,
                                                                           items=transformed_items,
                                                                           movement_type="both",
                                                                           adjustment_type=StockAdjustmentType.MANUAL,
                                                                           source_shelf=lost_shelf,
                                                                           destination_shelf=wip_shelf,
                                                                           content_model=None,
                                                                           remarks=f'Reverting to Work in Progress which are in Lost {task_list}',
                                                                           is_return=True)

            print('Script Completed to Movement to Wip')
