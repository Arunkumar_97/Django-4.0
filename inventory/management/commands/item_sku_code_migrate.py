from WMS.settings import MEDIA_ROOT
from inventory.models import SerialNumber, StockMovementInward, StockMovementOutward
from master.models import Item
from utils.excel.extraction import excel_extraction

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Update  Barcode To MovementLines'

    def handle(self, *args, **options):
        print('Script Started ')

        inwards = StockMovementInward.objects.filter().values('serials__item__sku_code', 'id')
        outwards = StockMovementOutward.objects.filter().values('serials__item__sku_code', 'id')

        for inward in inwards:
            StockMovementInward.objects.filter(id=inward['id']).update(item=inward['serials__item__sku_code'])
        for outward in outwards:
            StockMovementOutward.objects.filter(id=outward['id']).update(item=outward['serials__item__sku_code'])
        print('Item sku_code updated')
