from inventory.function import *
from inventory.models import *
from django.core.management.base import BaseCommand
import datetime

from core.models import CustomUser

from inventory.models import SerialNumber, StockMovementInward, \
    StockMovementOutward, Stock, Shelf

from master.models import Item


class Command(BaseCommand):
    help = 'Stock Fix Manual'

    def handle(self, *args, **options):
        print('Script Started ')
        serial_item_dict = {}
        stock_item_dict = {}
        ##Serial Wise Grouping
        for serial in SerialNumber.objects.exclude(shelf=None).values('hash', 'item__sku_code', 'shelf_id'):
            barcode = serial['item__sku_code']
            shelf = serial['shelf_id']
            if barcode in serial_item_dict:
                if shelf in serial_item_dict[barcode]:
                    serial_item_dict[barcode][shelf] += 1
                else:
                    serial_item_dict[barcode][shelf] = 1
            else:
                serial_item_dict[barcode] = {shelf: 1}

                ##Stock Wise Grouping
        for stock_detail in Stock.objects.values('item__sku_code', 'shelf_id', 'quantity'):
            barcode = stock_detail['item__sku_code']
            shelf = stock_detail['shelf_id']
            if barcode in stock_item_dict:
                if shelf in stock_item_dict[barcode]:
                    pass
                else:
                    stock_item_dict[barcode][shelf] = stock_detail['quantity']
            else:
                stock_item_dict[barcode] = {shelf: stock_detail['quantity']}

        for barcode, shelf_details in stock_item_dict.items():
            try:
                for shelf, stock_quantity in shelf_details.items():
                    item = Item.objects.get(sku_code=barcode)
                    # usns = SerialNumber.objects.filter(item=item, shelf=shelf_obj).values_list('hash', flat=True)
                    serial_quantity = 0

                    if barcode in serial_item_dict and shelf in serial_item_dict[barcode]:
                        serial_quantity = serial_item_dict[barcode][shelf]
                    if stock_quantity < serial_quantity:
                        calculated_quantity = serial_quantity - stock_quantity
                        source_shelf = Shelf.objects.get(id=shelf)
                        stock_source_obj = Stock.objects.get(
                            item_id=item.id,
                            shelf=source_shelf)
                        before_quantity = stock_source_obj.quantity

                        stock_source_obj.quantity += calculated_quantity
                        stock_source_obj.save()
                        adjustment_data_out = {
                            "item": item.sku_code, "created_by": CustomUser.objects.get(id=2),
                            "associated_transaction_id": '',
                            "quantity_before_adjust": before_quantity,
                            "quantity_after_adjust": stock_source_obj.quantity,
                            "adjustment_type": StockAdjustmentType.MANUAL,
                            "from_shelf": source_shelf,
                            "to_shelf": None, "created_at": datetime.datetime.now(),
                            "quantity": calculated_quantity, "remarks": 'creating for manual stock fix'}

                        stock_movement_inward = StockMovementInward.objects.create(
                            **adjustment_data_out)
                        # if len(usns)>0:
                        #     for serial in usns:
                        #         stock_movement_inward.serials.add(SerialNumber.objects.get(hash=serial))

                    elif stock_quantity > serial_quantity:
                        calculated_quantity = stock_quantity - serial_quantity
                        destination_shelf = Shelf.objects.get(id=shelf)
                        stock_source_obj = Stock.objects.get(
                            item_id=item.id,
                            shelf=destination_shelf)
                        before_quantity = stock_source_obj.quantity
                        stock_source_obj.quantity -= calculated_quantity
                        stock_source_obj.save()
                        adjustment_data_out = {
                            "item": item.sku_code, "created_by": CustomUser.objects.get(id=2),
                            "associated_transaction_id": '',
                            "quantity_before_adjust": before_quantity,
                            "quantity_after_adjust": stock_source_obj.quantity,
                            "adjustment_type": StockAdjustmentType.MANUAL,
                            "from_shelf": destination_shelf,
                            "to_shelf": None, "created_at": datetime.datetime.now(),
                            "quantity": calculated_quantity, "remarks": 'creating for manual stock fix'}

                        stock_movement_outward = StockMovementOutward.objects.create(
                            **adjustment_data_out)
                        # if len(usns) > 0:
                        #     for serial in usns:
                        #         stock_movement_outward.serials.add(SerialNumber.objects.get(hash=serial))
            except Exception as e:
                print(e)
        print('finished')
