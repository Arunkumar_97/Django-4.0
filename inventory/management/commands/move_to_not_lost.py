from WMS.settings import MEDIA_ROOT
from core.models import CustomUser
from dispatch.functions import transform_serials_to_item_with_serials
from inventory.function import create_stock_movement_without_reservation
from inventory.models import SerialNumber, Shelf, StockMovementInward, VirtualShelf, StockAdjustmentType
from master.models import Item
from utils.excel.extraction import  excel_extraction

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Update Child Barcode To Master in Usns'

    def handle(self, *args, **options):
        class dotdict(dict):
            """dot.notation access to dictionary attributes"""
            __getattr__ = dict.get
            __setattr__ = dict.__setitem__
            __delattr__ = dict.__delitem__

        print('Script Started to Change Child to master in serials')
        wip_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                      bay__stock_point_id=33434)
        wip_usns = ['15Z99CXQI', '19ZNGA5US', '1EHRXIZ12', '1KLYYTJ5N', '1RAMJHOGX', '1YT7198PH', '26YYZVU7', '2KXT3JFTL', '31L8DTYNU', '3JCM6ZGJV', '3QXPGSFNM', '3SLZEUKG2', '3XXMLHRCW', '3YM395ITF', '3ZEAVCRG', '493KAGPZF', '4HLBDNFEW', '4HNG5V7SA', '4JT81Y2TM', '4MGQT9CYQ', '4QG8ZP3UY', '4TEYIPA5P', '516INJUHW', '55P87SS4R', '563757G9O', '58KHOQ4XU', '5KA2NFOI1', '64JT2XSVL', '6B6SNVEHN', '6TEDLMTN6', '6UQ7KCGZM', '6Z4QAKNWP', '6ZN8QOBAS', '77B99E66I', '7C6ZPQPC1', '7K3729239', '7OFKGIR4I', '7PGOPK7HF', '8BC3L5GDD', '8INWXGWII', '8LU94HZ1X', '8T34ZNVNP', '8ZSUH1PWI', '96XUFRSSZ', '9BDX75X85', '9LZANKGQ2', '9OH5I3SAA', '9TU3X4GX7', '9UFNF1FAK', '9XVBYMWO8', 'A287U5ZD6', 'A3F5PHR38', 'A7CJJ8J1V', 'AF86T1EJQ', 'AUXXPR1HC', 'AZL9WZTXL', 'B37WNU5AA', 'BLE5YMR5P', 'BQA2B2GWX', 'BVZKBDX8K', 'BZKWAZKVD', 'CF1O1IVY6', 'CH8RZ8B7O', 'CKOLVAEFY', 'CQRJ2OG5G', 'D5SJ6YYKW', 'D82PNTPOJ', 'DA5DL32LL', 'DXPE1AAYJ', 'E4GZ847IW', 'E9UCTONZW', 'EB46NT4XE', 'EKCCNVEAW', 'EMJEF2OBO', 'ESPL8GH7B', 'F1VOQ4G7P', 'FPM5Q3PYP', 'G3XM5WVOS', 'GBUZ5WYK', 'GDXZDPKFF', 'GGZEHVO9Z', 'GZ2R2VSM9', 'H6KSZUT5H', 'HDH16BCBV', 'I8YJGF3NX', 'JHD8UDD8S', 'JR6PTZZO8', 'K1813HEPR', 'K54AWNGDY', 'K5SJUEKTT', 'KXL1N8Y6S', 'L35N5MU2P', 'L4YHLWNW7', 'L5T52Y33Q', 'LA4JMDN7U', 'LAO58HA78', 'LY5EZQ9MA', 'M145E4YAE', 'M4BDC5L2U', 'MB67BHYSX', 'MBD7I2THY', 'MMDJELYM', 'MVIKWD3Q', 'N7FTZPKNQ', 'NDYFXJTP', 'NGKXB9NNQ', 'OA5UOLT1', 'ODDD84PGY', 'P81N2UIP5', 'P9S7KPEZV', 'PHOFMB2LF', 'PR3K5C5KC', 'PWVS7PLO4', 'Q8SDWRDTH', 'QRSTEPTMO', 'QVQAIWU9C', 'QX46A2XX', 'RI3XTEFGG', 'RLQ5MCACH', 'RM8S81C53', 'RQ7L9N2YO', 'SBHNV1D66', 'SCKJHREUR', 'SQ9B7VLR6', 'TBBKNPYNT', 'TL4NGFLAU', 'TVI23F4SF', 'TY2R3DXH3', 'UAAW7IZ9F', 'V63UZ7UFE', 'V9K1DKOYQ', 'VE9UYCAG9', 'VGYI5LZSO', 'VONMS6SEU', 'VVR6OXEWV', 'W69M6DJXU', 'W72NAEB35', 'WEG94AVRR', 'WGR1MUTM5', 'WRIQ2F19', 'WV1KO222L', 'X4ZJHWYFY', 'X556Z189P', 'X95PV798A', 'XCZX8VD9E', 'XDTDLGS5B', 'XORPM8DPF', 'XPU4IYMYX', 'XQZKVBFF', 'Y78XZZ1OH', 'YETGBO9RJ', 'YH5BN2TSG', 'YQ9EB72NE', 'YSCSKUMBA', 'Z2VUWXH1C', 'ZBS5SCQCG', 'ZDAZ66EKS', 'ZKYCX6GKQ', 'ZTHNX4XKD']

        if wip_usns:
            transformed_items = transform_serials_to_item_with_serials(wip_usns)
            lost_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.LOST,
                                           bay__stock_point_id=33434)
            request = dotdict({'user': CustomUser.objects.get(id=1)})
            success, exception = create_stock_movement_without_reservation(request=request,
                                                                           items=transformed_items,
                                                                           movement_type="both",
                                                                           adjustment_type=StockAdjustmentType.MANUAL,
                                                                           source_shelf=wip_shelf,
                                                                           destination_shelf=lost_shelf,
                                                                           content_model=None,
                                                                           remarks='Reverting old pickslips',
                                                                           is_return=True)

        print('Script Completed to Change Child to master in serials')
