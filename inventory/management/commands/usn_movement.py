from WMS.settings import MEDIA_ROOT
from inventory.function import *
from inventory.models import *
from django.core.management.base import BaseCommand
import datetime

from utils.excel.extraction import excel_extraction


class Command(BaseCommand):
    help = 'Update  Barcode To MovementLines'

    def movement_func(self, request, adjustment_type, movement_type, items=None, source_shelf=None,
                      destination_shelf=None,
                      content_model=None, remarks=None):

        with transaction.atomic():
            try:
                stock_movement_outward_obj = None
                stock_movement_inward_obj = None
                content_type = None
                if content_model is not None:
                    content_type = ContentType.objects.get_for_model(model=content_model, for_concrete_model=False)
                for item_detail in items:
                    item = Item.objects.get(sku_code=item_detail['sku_code'])
                    if SerialNumber.objects.filter(Q(hash__in=item_detail['usns'])).exists():
                        adjusted_qty = len(item_detail['usns'])
                        if movement_type == 'outward' or movement_type == 'both':
                            stock_source_obj = Stock.objects.get(
                                item_id=item.id,
                                shelf=source_shelf)
                            before_quantity = stock_source_obj.quantity
                            stock_source_obj.quantity -= adjusted_qty
                            stock_source_obj.save()
                            adjustment_data_out = {
                                "item": item.sku_code, "created_by": CustomUser.objects.get(id=2),
                                "associated_transaction_id": '',
                                "associated_transaction": content_type,
                                "quantity_before_adjust": before_quantity,
                                "quantity_after_adjust": stock_source_obj.quantity,
                                "adjustment_type": adjustment_type,
                                "from_shelf": source_shelf,
                                "to_shelf": destination_shelf, "created_at": datetime.datetime.now(),
                                "quantity": adjusted_qty, "remarks": remarks if remarks is None else ''}

                            stock_movement_outward_obj = StockMovementOutward.objects.create(
                                **adjustment_data_out)
                        if movement_type == 'inward' or movement_type == 'both':
                            stock_destination_obj, created = Stock.objects.get_or_create(
                                item_id=item.id,
                                shelf=destination_shelf)
                            if created:
                                stock_destination_obj.quantity = 0
                            before_quantity = stock_destination_obj.quantity
                            stock_destination_obj.quantity += adjusted_qty
                            stock_destination_obj.save()
                            adjustment_data_in = {"item": item.sku_code,
                                                  "created_by": CustomUser.objects.get(id=2),
                                                  "associated_transaction_id": '',
                                                  "associated_transaction": content_type,
                                                  "quantity_before_adjust": before_quantity,
                                                  "quantity_after_adjust": stock_destination_obj.quantity,
                                                  "adjustment_type": adjustment_type,
                                                  "from_shelf": source_shelf, "to_shelf": destination_shelf,
                                                  "created_at": datetime.datetime.now(), "quantity": adjusted_qty,
                                                  "remarks": remarks if remarks is None else ''}
                            stock_movement_inward_obj = StockMovementInward.objects.create(**adjustment_data_in)
                        for usn in item_detail['usns']:
                            serial_obj = SerialNumber.objects.get(hash=usn, item=item)
                            serial_obj.shelf = destination_shelf
                            serial_obj.save()
                            if movement_type == 'both' or movement_type == 'inward':
                                stock_movement_inward_obj.serials.add(serial_obj)
                            if movement_type == 'both' or movement_type == 'outward':
                                stock_movement_outward_obj.serials.add(serial_obj)
                return 1, ''
            except SerialNumber.DoesNotExist:
                transaction.set_rollback(True)
                return 0, 'No Lines are created scan unsuccessful'
            except Exception as e:
                transaction.set_rollback(True)
                return 0, str(e)

    def handle(self, *args, **options):
        data = excel_extraction(MEDIA_ROOT + '/usns.xlsx')
        for row in data:
            temp_dict = {}
            for serial in SerialNumber.objects.filter(hash=row[0]).exclude(Q(shelf=None) | Q(shelf__name='WIP')) \
                    .values('hash', 'item__sku_code', 'shelf_id'):
                barcode = serial['item__sku_code']
                shelf = serial['shelf_id']
                if barcode in temp_dict:
                    if shelf in temp_dict[barcode]:
                        temp_dict[barcode][shelf].append(serial['hash'])
                    else:
                        temp_dict[barcode][shelf] = [serial['hash']]
                else:
                    temp_dict[barcode] = {shelf: [serial['hash']]}
            for barcode, shelf_details in temp_dict.items():
                try:
                    for shelf, hash in shelf_details.items():
                        item = Item.objects.get(sku_code=barcode)
                        source_shelf = Shelf.objects.get(id=shelf)
                        shelf_obj = Shelf.objects.get(bay_id=3492,id=7025)
                        usns = SerialNumber.objects.filter(item=item, shelf=source_shelf, hash__in=hash).values_list(
                            'hash', flat=True)
                        move_items = [{'sku_code': barcode, 'usns': usns}]
                        created, exception = self.movement_func(
                            request=CustomUser.objects.get(id=2),
                            items=move_items,
                            movement_type="both",
                            adjustment_type=StockAdjustmentType.MANUAL,
                            source_shelf=source_shelf,
                            destination_shelf=shelf_obj,
                            content_model=None,
                            remarks='')
                        if created == 0:
                            print(exception)
                except Exception as e:
                    print(e)
        print('finished')
