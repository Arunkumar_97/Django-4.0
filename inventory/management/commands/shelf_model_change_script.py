from django.core.management import BaseCommand
from django.db.models import Q, OuterRef, Case, When,Value,BigAutoField,Subquery,F

from inventory.models import Bay, VirtualShelf, Shelf, StockMovementInward, StockMovementOutward, SerialNumber, Stock


class Command(BaseCommand):
    help = 'Post Script for Shelf Model Change'
    def handle(self, *args, **options):
        stock_point_id = 33434
        print('Script Started')
        print('Performing Operation to change bay types')
        # Move bays to physical and virtual respectively
        bay_obj, created = Bay.objects.get_or_create(name='Virtual Bay', stock_point_id=stock_point_id)
        physical_bays = Bay.objects.filter(stock_point_id=stock_point_id).exclude(
            name__in=['LOST', 'DC Return Bay', 'Virtual Bay', 'Transit Bay', 'Not Found Bay',
                      'WIP', 'WIP Bay', 'Dispatched Bay']).update(type=Bay.BayType.PHYSICAL)
        virtual_bays = Bay.objects.filter(stock_point_id=stock_point_id,
                                          name__in=['LOST', 'DC Return Bay', 'Virtual Bay', 'Transit Bay',
                                                    'Not Found Bay',
                                                    'WIP', 'WIP Bay', 'Dispatched Bay']).update(
            type=Bay.BayType.VIRTUAL)
        print('Performing Operation to change bay types completed....')
        print('Performing Operation to change virtual shelfs to particular bay....')
        # Move virtual shelfs to virtual bay
        virtual_shelf_s = VirtualShelf.objects.filter(shelf_ptr_id__bay__stock_point_id=stock_point_id).exclude(
            name='WIP Shelf').all().values('shelf_ptr_id', 'id', 'name')
        virtual_shelf_ptr_ids = []
        virtual_shelf_ids = []
        for virtual_shelf_obj in virtual_shelf_s:
            virtual_shelf_ptr_ids.append(virtual_shelf_obj['shelf_ptr_id'])
            virtual_shelf_ids.append(virtual_shelf_obj['id'])

        Shelf.objects.filter(id__in=virtual_shelf_ptr_ids).update(bay=bay_obj)

        choices = Shelf.VirtualShelfTypes.choices
        for choice in choices:
            VirtualShelf.objects.get_or_create(bay=bay_obj, name=choice[1])
        subquery = Shelf.objects.filter(id=OuterRef('shelve_id')).annotate(
            final_shelf_name=Case(When(name='Dispatch Shelf', then=Value(Shelf.objects.get(name='Dispatched',

                                                                                           bay__stock_point_id=stock_point_id).id,
                                                                         output_field=BigAutoField())),
                                  When(name='Return Shelf', then=Value(Shelf.objects.get(name='Returns',

                                                                                         bay__stock_point_id=stock_point_id).id,
                                                                       output_field=BigAutoField())),
                                  When(name='NOT FOUND', then=Value(Shelf.objects.get(name='Not Found',

                                                                                      bay__stock_point_id=stock_point_id).id,
                                                                    output_field=BigAutoField())),
                                  When(name='Transit Shelf', then=Value(Shelf.objects.get(name='Transit',

                                                                                          bay__stock_point_id=stock_point_id).id,
                                                                        output_field=BigAutoField())),
                                  When(name='WIP', then=Value(Shelf.objects.get(name='Work In Progress',

                                                                                bay__stock_point_id=stock_point_id).id,
                                                              output_field=BigAutoField())),
                                  When(name='WIP Shelf', then=Value(Shelf.objects.get(name='Work In Progress',

                                                                                bay__stock_point_id=stock_point_id).id,
                                                              output_field=BigAutoField())),
                                  When(name='LOST', then=Value(Shelf.objects.get(name='Lost',

                                                                                 bay__stock_point_id=stock_point_id).id,
                                                               output_field=BigAutoField())),
                                  When(name='Inward Shelf', then=Value(Shelf.objects.get(name='Staging Area',

                                                                                         bay__stock_point_id=stock_point_id).id,
                                                                       output_field=BigAutoField())), default=F('id')))
        stock_adjustment_inwards_from = StockMovementInward.objects.filter(
            from_shelf__name__in=['Dispatch Shelf', 'Return Shelf',
                                    'NOT FOUND', 'Transit Shelf', 'WIP','WIP Shelf', 'LOST', 'Inward Shelf']).annotate(shelve_id=F('from_shelf__id')).values(
            'shelve_id').update(from_shelf_id=Subquery(subquery.values('final_shelf_name')[0:1]))
        stock_adjustment_inwards_to = StockMovementInward.objects.filter(
                to_shelf__name__in=['Dispatch Shelf', 'Return Shelf',
                                    'NOT FOUND', 'Transit Shelf', 'WIP','WIP Shelf', 'LOST', 'Inward Shelf']).annotate(
            shelve_id=F('to_shelf__id')).values(
            'shelve_id').update(to_shelf_id=Subquery(subquery.values('final_shelf_name')[0:1]))
        stock_adjustment_outwards_from = StockMovementOutward.objects.filter(
            from_shelf__name__in=['Dispatch Shelf', 'Return Shelf',
                                    'NOT FOUND', 'Transit Shelf', 'WIP','WIP Shelf', 'LOST', 'Inward Shelf'] ).annotate(
            shelve_id=F('from_shelf__id')).values(
            'shelve_id').update(from_shelf_id=Subquery(subquery.values('final_shelf_name')[0:1]))
        stock_adjustment_outwards_to = StockMovementOutward.objects.filter(

                to_shelf__name__in=['Dispatch Shelf', 'Return Shelf',
                                    'NOT FOUND', 'Transit Shelf', 'WIP', 'WIP Shelf','LOST', 'Inward Shelf']).annotate(
            shelve_id=F('to_shelf__id')).values(
            'shelve_id').update(to_shelf_id=Subquery(subquery.values('final_shelf_name')[0:1]))
        SerialNumber.objects.filter(
            shelf__name__in=['Dispatch Shelf', 'Return Shelf',
                             'NOT FOUND', 'Transit Shelf', 'WIP','WIP Shelf', 'LOST', 'Inward Shelf']).annotate(shelve_id=F('shelf__id')).values('shelve_id').update(
            shelf_id=Subquery(subquery.values('final_shelf_name')[0:1]))
        items = SerialNumber.objects.exclude(shelf=None).values_list('item__id', flat=True).distinct()
        available_shelfs = Shelf.objects.filter(bay__name='Virtual Bay',
                                                name__in=['Dispatched', 'Staging Area', 'Returns', 'Not Found',
                                                          'Transit', 'Work In Progress', 'Lost'],
                                                bay__stock_point_id=stock_point_id).values_list('id',
                                                                                                flat=True)
        for item in items:
            for shelf in available_shelfs:
                try:
                    Stock.objects.get_or_create(item_id=item, shelf_id=shelf, quantity=0)
                except Exception as e:
                    print(str(e))

        print('Performing Operation to change virtual shelfs to particular bay....')
        print('finished')
        print('setting default values for all shelfs')
        Shelf.objects.filter(bay__stock_point_id=stock_point_id, VirtualShelf___type=Shelf.VirtualShelfTypes.DISPATCHED).update(is_picking_blocked=True, is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.DISPATCHED).update(is_picking_blocked=True,
                                                                                            is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.RETURNS).update(is_picking_blocked=False,
                                                                                            is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.NOT_FOUND).update(is_picking_blocked=False,
                                                                                            is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.LOST).update(is_picking_blocked=False,
                                                                                            is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.TRANSIT).update(is_picking_blocked=False,
                                                                                            is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS).update(is_picking_blocked=False,
                                                                                         is_put_away_blocked=False)
        Shelf.objects.filter(bay__stock_point_id=stock_point_id,
                             VirtualShelf___type=Shelf.VirtualShelfTypes.STAGING_AREA).update(
            is_picking_blocked=False,
            is_put_away_blocked=True)


        Shelf.objects.filter(name__in=['MS-SH-01-R1-QC-7031']).update(is_picking_blocked=True,
            is_put_away_blocked=True)




