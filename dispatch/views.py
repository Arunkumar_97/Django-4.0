import datetime
import json
import logging

from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import F, Sum, IntegerField, Count, OuterRef, Subquery, Q, DecimalField
from django.db.models.functions import Coalesce, Concat, Extract
from django.db.models import CharField, Value
from django.http import JsonResponse
from rest_framework import status, viewsets
from rest_framework.response import Response
from core.base.views import JSONApi
from dispatch.functions import get_task_quantity_details, get_task_status_by_id, generate_picker_image_dict, \
    transform_serials_to_item_with_serials, assign_picker_shelf, transform_serial_dict
from dispatch.interface.operation import WarehouseProcessType
from dispatch.models import *
from dispatch.serializers import PickerSerializer
from inventory.models import Stock, StockPoint, Warehouse, StockMovementInward, StockAdjustmentType, SerialNumber, \
    StockMovementOutward
from inventory.function import transfer_serials_to_item, create_stock_movement_without_reservation, \
    get_current_shelf_items
from inventory.models import Stock, StockPoint, Warehouse, StockMovementInward, StockAdjustmentType, Bay, Shelf, \
    VirtualShelf, SerialNumber
from django_celery_beat.models import PeriodicTask, IntervalSchedule, CrontabSchedule

logger = logging.getLogger(__name__)


class AllocationAPI(JSONApi):
    @staticmethod
    def post(request):
        try:
            result, exceptions = '', ''
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = request.data['items'] if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            warehouse = request.data['warehouse'] if 'warehouse' in request.data else None
            stock_point = request.data['stock_point'] if 'stock_point' in request.data else None
            requested_from = request.data['requested_from'] if 'requested_from' in request.data else 'Wms'
            if requested_from is not None and requested_from == 'bluefish':
                stock_point = StockPoint.objects.get(client_pk_id=stock_point).id
            shelf_id = request.data['shelf_name'] if 'shelf_name' in request.data else None
            hash = request.data['hash'] if 'hash' in request.data else None
            remarks = request.data['remarks'] if 'remarks' in request.data else ''
            bay = request.data['bay'] if 'bay' in request.data else None
            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, stock_point_id=stock_point, shelf_id=shelf_id,
                                       hash=hash, remarks=remarks, bay_id=bay).get_instance(process_type,
                                                                                            operation_type)
            sub_operation_type = request.data['sub_operation_type']
            if sub_operation_type == 'create_reservation':
                result, exceptions = obj.create_reservation()
            if sub_operation_type == 'get_reservation':
                result, exceptions = obj.get_reservation()
            if sub_operation_type == 'get_pending_stock':
                result, exceptions = obj.get_pending_stock()
            if sub_operation_type == 'scanning':
                result, exceptions = obj.on_scan_usn()
            if sub_operation_type == 'get_available_shelf_quantity':
                result, exceptions = obj.get_available_shelf_quantity()
            if sub_operation_type == 'not_found':
                result, exceptions = obj.not_found_usn()
            if sub_operation_type == 'get_wip_stock':
                result, exceptions = obj.get_wip_stock()
            if sub_operation_type == 'cycle_count':
                result, exceptions = obj.submit_cycle_count()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, "data": {"result": json.dumps(str(e))}})


class CreateTask(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = request.data['items'] if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            warehouse = request.data['warehouse'] if 'warehouse' in request.data else None
            stock_point = request.data['stock_point'] if 'stock_point' in request.data else None
            requested_from = request.data['requested_from'] if 'requested_from' in request.data else 'Wms'
            if requested_from is not None and requested_from == 'bluefish':
                stock_point = StockPoint.objects.get(client_pk_id=stock_point).id

            remarks = request.data['remarks'] if 'remarks' in request.data else ''
            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, stock_point_id=stock_point, remarks=remarks).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.create_reservation()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                logger.error(result)
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            logger.error(str(e))
            print(e)
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class TaskList(JSONApi):
    @staticmethod
    def get(request):
        try:
            filters = Task.apply_filters(request, {})
            task_status = None
            if 'status_id' in request.GET and request.GET.get('status_id') != 'null':
                task_status = get_task_status_by_id(request.GET.get('status_id'))
            total_picked_per_day = 0
            total_picked_per_month = 0
            efficiency = 0
            total_picked_per_day_percent = 0
            if not request.user.is_superuser:
                # per day
                total_picked_per_day = StockSerial.objects.filter(picker__picker_id=request.user.id,
                                                                  picked_time__date=datetime.datetime.today().date()) \
                    .aggregate(total_count=Coalesce(Count('id'), 0, output_field=IntegerField()))['total_count']
                total_tasks_per_day = Task.objects.filter(picker__picker__id=request.user.id,
                                                          created_at__date=datetime.datetime.today().date()).aggregate(
                    total_quantity=Coalesce(Sum('task_reserve__quantity', ), 0, output_field=IntegerField()))[
                    'total_quantity']
                total_picked_per_day_percent = round((total_picked_per_day / total_tasks_per_day) * 100) \
                    if total_tasks_per_day > 0 else 0

                # per month
                total_picked_per_month = StockSerial.objects.filter(picker__picker_id=request.user.id,
                                                                    picked_time__date__range=[
                                                                        datetime.datetime.today().replace(day=1).date(),
                                                                        datetime.datetime.today().date()]) \
                    .aggregate(total_count=Coalesce(Count('id'), 0, output_field=IntegerField()))['total_count']

                temp_pending = Task.objects.filter(picker__picker__id=request.user.id, created_at__date__range=[
                    datetime.datetime.today().replace(day=1).date(), datetime.datetime.today().date()]).aggregate(
                    total_quantity=Coalesce(Sum('task_reserve__quantity'), 0, output_field=IntegerField()))[
                    'total_quantity']

                efficiency = round((total_picked_per_month / temp_pending) * 100) if temp_pending > 0 else 0
                filters['picker__picker_id'] = request.user.id
            filters['valid'] = True
            task_list = list(Task.objects.filter(**filters).annotate(
                created_date=Concat(Extract('created_at', 'day'), Value('-'), Extract('created_at', 'month'),
                                    Value('-'), Extract('created_at', 'year'), output_field=CharField()),
                pickers=ArrayAgg('picker__id')).values('id',
                                                       'operation',
                                                       'type',
                                                       'created_date',
                                                       'associated_transaction',
                                                       'pickers',
                                                       'remarks',
                                                       'valid',
                                                       'created_at').order_by(
                "-created_at"))
            return_list = []
            for task_list_row in task_list:
                current_status = CycleCount.objects.get(task_id=task_list_row['id']).get_cycle_count_status() if \
                    task_list_row['operation'] == 'CC' else Task.objects.get(
                    id=task_list_row['id']).get_current_status()
                if task_status is not None:
                    if current_status == task_status:
                        stock_reservations = StockReservation.objects.filter(task_id=task_list_row['id'], )
                        reserved_quantity = stock_reservations.aggregate(
                            total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total_quantity']
                        total_quantity = len(CycleCount.objects.get(task_id=task_list_row['id']).shelves.all()) if \
                            task_list_row['operation'] == 'CC' else reserved_quantity
                        row = {"task_id": task_list_row['id'], 'operation': task_list_row['operation'],
                               'type': '',
                               'created_time': task_list_row['created_date'], 'quantity': total_quantity,
                               'associated_transaction': task_list_row['associated_transaction'],
                               'picker': [generate_picker_image_dict(picker) for picker in task_list_row['pickers']] if
                               task_list_row['pickers'][0] is not None else [],
                               'task_status': current_status, 'remarks': task_list_row['remarks']}
                        for task_type in Task.Type.choices:
                            if task_type[0] == task_list_row['type']:
                                row['type'] = task_type[1]
                                break
                        return_list.append(row)
                else:
                    stock_reservations = StockReservation.objects.filter(task_id=task_list_row['id'], )
                    reserved_quantity = stock_reservations.aggregate(
                        total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total_quantity']

                    total_quantity = len(CycleCount.objects.get(task_id=task_list_row['id']).shelves.all()) if \
                        task_list_row['operation'] == 'CC' else reserved_quantity
                    row = {"task_id": task_list_row['id'], 'operation': task_list_row['operation'],
                           'type': '',
                           'created_time': task_list_row['created_date'], 'quantity': total_quantity,
                           'associated_transaction': task_list_row['associated_transaction'],
                           'picker': [generate_picker_image_dict(picker) for picker in task_list_row['pickers']] if
                           task_list_row['pickers'][0] is not None else [],
                           'task_status': current_status, 'remarks': task_list_row['remarks']}
                    for task_type in Task.Type.choices:
                        if task_type[0] == task_list_row['type']:
                            row['type'] = task_type[1]
                            break
                    return_list.append(row)

            return JsonResponse(status=status.HTTP_200_OK,
                                data={'result': return_list, 'total_picked_per_day': total_picked_per_day,
                                      'total_picked_per_month': total_picked_per_month,
                                      'efficiency': efficiency,
                                      'total_picked_per_day_percent': total_picked_per_day_percent,
                                      'total_picked_per_month_percent': efficiency})
        except Exception as e:
            return JsonResponse(status=status.HTTP_400_BAD_REQUEST, data={
                "message": str(e)
            })


class TaskInfo(JSONApi):
    @staticmethod
    def get(request):
        try:
            task_id = request.GET["task_id"]
            type = request.GET["type"]
            filters = dict()
            filters["task_id"] = task_id
            if type == "picked":
                filters["not_found"] = False
            if type == "not_found":
                filters["not_found"] = True
            if not type == 'total':

                result = list(StockReservation.objects.filter(**filters)
                              .values(sku_code=F("item__sku_code"))
                              .annotate(serials=ArrayAgg('usns__hash')).exclude(
                    stockserial__usn__shelf_id__isnull=True).values('sku_code', 'serials'))
            else:
                result = list(
                    Task.objects.filter(id=task_id).annotate(
                        sku_code=F('task_reserve__item__sku_code')).values(
                        'sku_code').annotate(quantity=Sum('task_reserve__quantity')))
            return JsonResponse(data={"data": result, "status": 200})
        except Exception as e:
            return JsonResponse(data={"error": str(e), "status": 500})


class GetQuantityDetailsForTask(JSONApi):
    @staticmethod
    def get(request, task_id):
        return JsonResponse({'data': get_task_quantity_details(task_id)})


class CreatePicker(JSONApi):
    @staticmethod
    def post(request):
        try:
            data = request.data
            custom_user, created = CustomUser.objects.get_or_create(username=data['data']['username'],
                                                                    user_type=UserType.PICKER,
                                                                    password=data['data']['password'],
                                                                    first_name=data['data']['first_name'],
                                                                    last_name=data['data']['last_name'])
            if created:
                picker = Picker.objects.create(picker=custom_user, valid=data['data']['valid'])
            else:
                Picker.objects.filter(picker=custom_user).update(valid=data['data']['valid'])
            return JsonResponse({"success": True, "created": True, "message": "Picker Created Successfully"})
        except Exception as e:
            return JsonResponse({"success": False, "created": False, "message": str(e)})


class AllocatePicker(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            picker_id = request.data['picker_id']
            task_id = request.data['task_id']
            obj = WarehouseProcessType([], request, order_id='', force_allocate='False',
                                       task_id=task_id, stock_point_id=None, picker_id=picker_id).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.assign_picker()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class PickerCheckInApi(JSONApi):
    @staticmethod
    def post(request):
        try:
            if not request.user.is_superuser:
                picker = Picker.objects.get(picker_id=request.user.id)
                exists = PickerCheckIn.objects.filter(picker=picker,
                                                      created_at__date=datetime.datetime.today().date()).exists()
                if not exists:
                    PickerCheckIn.objects.create(picker=picker,
                                                 created_at=datetime.datetime.today())

                    WarehouseProcessType([], request, order_id='', force_allocate='False',
                                         task_id='', stock_point_id=None, picker_id='').get_instance(
                        'picking',
                        'pick_slip').assign_picker()
            return JsonResponse({"status": 200, 'success': True, "data": {"result": 'checked in'}})

        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class CreateTaskWithoutReservation(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = request.data['items'] if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            stock_point = request.data['stock_point'] if 'stock_point' in request.data else None
            remarks = request.data['remarks'] if 'remarks' in request.data else ''

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, stock_point_id=stock_point, remarks=remarks).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.create_task()
            if len(exceptions) == 0:
                print(result)
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                print(result)
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})

        except Exception as e:
            print(str(e))
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class PutAwaySubmit(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = request.data['items'] if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            remarks = request.data['remarks'] if 'remarks' in request.data else ''
            shelf_id = request.data['shelf_name'] if 'shelf_name' in request.data else ''

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, shelf_id=shelf_id,
                                       remarks=remarks).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.simple_put_away()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class PutAwayList(JSONApi):
    @staticmethod
    def get(request):
        try:
            filters = StockMovementInward.apply_filters(request, {})
            filters['adjustment_type'] = StockAdjustmentType.PUT_AWAY
            result = list(StockMovementInward.objects.filter(**filters) \
                          .values('id', 'associated_transaction_id', 'created_at',
                                  'created_by__username', 'to_shelf__name',
                                  'quantity', 'remarks',
                                  ).annotate(
                transaction=F('associated_transaction_id'))
                          .annotate(shelf=F('to_shelf__name')).annotate(total=Sum('quantity')).annotate(
                created=F('created_at')).annotate(user=F('created_by__username')).annotate(
                transaction_id=F('id')).order_by('transaction_id'))
            return JsonResponse({"status": 200, 'success': True, "data": {"result": result}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class GetShelfQuantityDetails(JSONApi):
    @staticmethod
    def get(request):
        try:
            process_type = request.GET.get('process_type')
            operation_type = request.GET.get('operation_type')
            items = request.GET.get('items', None)
            remarks = request.GET.get('remarks', None)
            shelf_id = request.GET.get('shelf_id', None)
            obj = WarehouseProcessType(items, request, order_id='', force_allocate=False,
                                       task_id=None, remarks=remarks,
                                       shelf_id=shelf_id).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.get_available_shelf_quantity()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "result": json.dumps(result)})
            else:
                return JsonResponse({"status": 500, 'success': False, "result": json.dumps(exceptions)})

        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "result": json.dumps(str(e))})


class ScanUSN(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = request.data['items'] if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            warehouse = request.data['warehouse'] if 'warehouse' in request.data else None
            stock_point = request.data['stock_point'] if 'stock_point' in request.data else None
            requested_from = request.data['requested_from'] if 'requested_from' in request.data else 'Wms'
            if requested_from is not None and requested_from == 'bluefish':
                stock_point = StockPoint.objects.get(client_pk_id=stock_point).id
            shelf_id = request.data['shelf_name'] if 'shelf_name' in request.data else None
            hash = request.data['hash'] if 'hash' in request.data else None
            remarks = request.data['remarks'] if 'remarks' in request.data else ''
            bay = request.data['bay'] if 'bay' in request.data else None

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, stock_point_id=stock_point, shelf_id=shelf_id,
                                       hash=hash, remarks=remarks, bay_id=bay).get_instance(process_type,
                                                                                            operation_type)
            result, exceptions = obj.on_scan_usn()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, "data": {"result": json.dumps(str(e))}})


class TaskDetail(JSONApi):
    @staticmethod
    def get(request):
        global task_info
        try:
            task_id = request.GET.get('task_id')
            itemList = list()
            final_data = dict()
            reservation = StockReservation.objects.filter(task_id=task_id) \
                .values('id', 'item__sku_code', 'shelf__name',
                        'quantity')
            for item in reservation:
                item_details = dict()
                item_details['sku_code'] = item['item__sku_code']
                item_details['shelf'] = item['shelf__name']
                item_details['quantity'] = item['quantity']
                item_details['picked_quantity'] = StockSerial.objects.filter(
                    stock_reservation_id=item['id']).aggregate(
                    total_count=Coalesce(Count('id'), 0))['total_count']
                itemList.append(item_details)
            task_list = list(Task.objects.filter(id=task_id).annotate(
                created_date=Concat(Extract('created_at', 'day'), Value('-'), Extract('created_at', 'month'),
                                    Value('-'), Extract('created_at', 'year'), output_field=CharField()),
                pickers=ArrayAgg('picker__id')).values('id',
                                                       'operation',
                                                       'type',
                                                       'created_date',
                                                       'associated_transaction',
                                                       'pickers',
                                                       'remarks',
                                                       'valid',
                                                       'created_at').order_by(
                "created_at"))
            for task_list_row in task_list:
                stock_reservations = StockReservation.objects.filter(task_id=task_list_row['id'])
                reserved_quantity = stock_reservations.aggregate(
                    total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total_quantity']
                task_info = {"task_id": task_list_row['id'], 'operation': task_list_row['operation'],
                             'type': 'Picking' if task_list_row['type'] == 'PC' else 'Put Away',
                             'created_time': task_list_row['created_date'], 'quantity': reserved_quantity,
                             'associated_transaction': task_list_row['associated_transaction'],
                             'picker': [generate_picker_image_dict(picker) for picker in task_list_row['pickers']],
                             'remarks': task_list_row['remarks'], "picked_quantity": StockMovementInward.objects
                    .filter(associated_transaction_id=task_list_row['id'], adjustment_type=StockAdjustmentType.PICKING)
                    .aggregate(total_count=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))}
            final_data["stock_point"] = list(StockReservation.objects.filter(task_id=task_id) \
                                             .values('shelf__bay__stock_point_id', 'shelf__bay__stock_point__name',
                                                     ) \
                                             .distinct() \
                                             .annotate(value=F('shelf__bay__stock_point_id'),
                                                       label=F('shelf__bay__stock_point__name'),
                                                       ) \
                                             .order_by('value', 'label'))
            final_data['item'] = itemList
            final_data['task_info'] = task_info
            return JsonResponse({"status": 200, 'success': True, "result": final_data})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "result": json.dumps(str(e))})


class Taskprogress(JSONApi):
    @staticmethod
    def post(request):
        try:
            created_at = request.data['created_at']
            task_id = request.data['task_id']
            picking = TaskProgress(created_at=created_at, picker_id=request.user.id, task_id=task_id)
            picking.save()
            return JsonResponse({"status": 200, 'success': True, "message": 'Picking Started'})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "result": json.dumps(str(e))})

    @staticmethod
    def put(request):
        try:
            completed_at = request.data['completed_at']
            task_id = request.data['task_id']
            TaskProgress.objects.filter(picker_id=request.user.id, task_id=task_id).update(
                completed_at=completed_at)
            return JsonResponse({"status": 200, 'success': True, "message": 'Picking completed'})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "result": json.dumps(str(e))})


class AdhocPicking(JSONApi):
    @staticmethod
    def post(request):
        pass


class RevertPickSlip(JSONApi):
    @staticmethod
    def post(request):
        try:
            task_id = request.data['task_id']
            if Task.objects.get(id=task_id):
                # todo need to receive stockpoint id from request
                wip_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.WORK_IN_PROGRESS,
                                              bay__stock_point_id=33434)
                wip_usns = SerialNumber.objects.filter(
                    hash__in=StockMovementInward.objects.filter(associated_transaction_id=task_id).values_list(
                        'serials__hash', flat=True).distinct(),
                    shelf__virtualshelf__type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS).values_list('hash', flat=True)
                if wip_usns.exists():
                    transformed_items = transform_serials_to_item_with_serials(wip_usns)
                    lost_shelf = Shelf.objects.get(VirtualShelf___type=VirtualShelf.VirtualShelfTypes.LOST,
                                                   bay__stock_point_id=33434)
                    success, exception = create_stock_movement_without_reservation(request=request,
                                                                                   items=transformed_items,
                                                                                   movement_type="both",
                                                                                   adjustment_type=StockAdjustmentType.MANUAL,
                                                                                   source_shelf=wip_shelf,
                                                                                   destination_shelf=lost_shelf,
                                                                                   content_model=None,
                                                                                   remarks=f'Reverting pickslip {task_id}',
                                                                                   is_return=True)

                StockReservation.objects.filter(task_id=task_id, valid=True).update(valid=False)
                Task.objects.filter(id=task_id).update(valid=False)
                return JsonResponse(
                    {"status": 200, 'success': True, "data": {"result": json.dumps('Reverted Successfully')}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class PutAwayApp(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = transfer_serials_to_item(request.data['items']) if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            remarks = request.data['remarks'] if 'remarks' in request.data else ''

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id,
                                       remarks=remarks).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.simple_put_away()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result['status']),
                                                                              "shelf_info": result['shelf_info']}})
            else:
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class PickingAPP(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = transform_serials_to_item_with_serials(request.data['items']) if 'items' in request.data else []
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            remarks = request.data['remarks'] if 'remarks' in request.data else ''

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id,
                                       remarks=remarks).get_instance(
                process_type,
                operation_type)
            result, exceptions = obj.on_scan_usn()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class Returns(JSONApi):
    @staticmethod
    def post(request):
        try:
            usns = request.data['usns']
            if request.data.get("is_from_bluefish", None):

                stock_point = StockPoint.objects.get(client_pk_id=request.data['stock_point_id'])
            else:
                stock_point = StockPoint.objects.get(id=request.data['stock_point_id'])

            return_shelf = Shelf.objects.get(bay__stock_point=stock_point,
                                             VirtualShelf___type=Shelf.VirtualShelfTypes.RETURNS)
            transaction_id = request.data['remarks']
            dispatch_shelf = Shelf.objects.get(
                VirtualShelf___type=Shelf.VirtualShelfTypes.DISPATCHED, bay__stock_point=stock_point)
            for usn in usns:
                if not SerialNumber.objects.filter(hash=usn, shelf=dispatch_shelf).exists():
                    raise Exception(f'{usn} is not Dispatched  to proceed DC Return')
            transformed_items = transform_serials_to_item_with_serials(usns)
            success, exception = create_stock_movement_without_reservation(request=request,
                                                                           items=transformed_items,
                                                                           movement_type="both",
                                                                           adjustment_type=StockAdjustmentType.DC_RETURN,
                                                                           source_shelf=dispatch_shelf,
                                                                           destination_shelf=return_shelf,
                                                                           content_model=None,
                                                                           remarks=transaction_id, is_return=True)

            if success == 0:
                raise Exception(str(exception))
            return JsonResponse(status=201, data=
            {"message": "Return Done Successfully", "data": request.data})
        except Exception as e:
            return JsonResponse(status=500, data={"message": str(e)})


class PickerApi(JSONApi):
    def get(self, request):
        search_params = request.GET.get('query', None)
        filters = {}
        if search_params != None:
            filters['picker__username__icontains'] = search_params
        pickers = Picker.objects.filter(**filters).annotate(name=F('picker__username')).values('id', 'name')
        return JsonResponse({'results': list(pickers)})


class NotFoundItem(JSONApi):
    @staticmethod
    def post(request):
        try:
            process_type = request.data['process_type']
            operation_type = request.data['operation_type']
            items = transform_serial_dict(request.data['shelf_id'], request.data['quantity'], request.data['sku_code'])
            order_id = request.data['order_id'] if 'order_id' in request.data else ''
            stock_point = request.data['stock_point'] if 'stock_point' in request.data else None
            shelf_id = request.data['shelf_id'] if 'shelf_id' in request.data else None
            task_id = request.data['task_id'] if 'task_id' in request.data else 0
            force_allocate = request.data['force_allocate'] if 'force_allocate' in request.data else 'False'
            remarks = request.data['remarks'] if 'remarks' in request.data else ''

            obj = WarehouseProcessType(items, request, order_id=order_id, force_allocate=force_allocate,
                                       task_id=task_id, shelf_id=shelf_id,
                                       remarks=remarks, stock_point_id=stock_point).get_instance(process_type,
                                                                                                 operation_type)
            result, exceptions = obj.not_found_usn()
            if len(exceptions) == 0:
                return JsonResponse({"status": 200, 'success': True, "data": {"result": json.dumps(result)}})
            else:
                return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(exceptions)}})
        except Exception as e:
            return JsonResponse({"status": 500, 'success': False, "data": {"result": json.dumps(str(e))}})


class CreateCycleCountTask(JSONApi):
    @staticmethod
    def post(request):
        try:
            response_data = dict()
            pickervsquantity = dict()
            pickervsshelf = dict()
            temp = 0
            shelves = request.data['shelves']
            task = Task.objects.create(type=Task.Type.CYCLE_COUNT, operation=Task.Operation.CYCLE_COUNT,
                                       request=request.data,
                                       remarks=request.data['remarks'], )
            for picker in request.data['pickers']:
                task.picker.add(picker)
            cycle_count = CycleCount.objects.create(task=task)
            if len(shelves) % 2 == 0:
                temp_shelf = len(shelves)
                assign_per_picker = temp_shelf / len(request.data['pickers'])
                for picker in request.data['pickers']:
                    pickervsquantity[picker] = assign_per_picker
                pickervsquantity[request.data['pickers'][0]] += 1
                for picker, quantity in pickervsquantity.items():
                    if temp == 0:
                        pickervsshelf[picker] = shelves[temp:int(quantity)]
                        temp += int(quantity)
                    else:
                        pickervsshelf[picker] = shelves[temp:temp + int(quantity)]
                        temp += int(quantity)
            else:
                temp_shelf = len(shelves) - 1
                assign_per_picker = temp_shelf / len(request.data['pickers'])
                for picker in request.data['pickers']:
                    pickervsquantity[picker] = assign_per_picker
                pickervsquantity[request.data['pickers'][0]] += 1
                for picker, quantity in pickervsquantity.items():
                    if temp == 0:
                        pickervsshelf[picker] = shelves[temp:int(quantity)]
                        temp += int(quantity)
                    else:
                        pickervsshelf[picker] = shelves[temp:temp + int(quantity)]
                        temp += int(quantity)
            assign_picker_shelf(pickervsshelf, cycle_count)

            response_data['success'] = True
            response_data['id'] = task.id
            response_data['shelves'] = request.data['shelves']
            response_data['pickers'] = request.data['pickers']
            response_data['remarks'] = request.data['remarks']
            return JsonResponse(response_data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return JsonResponse({'success': False, "data": {"result": json.dumps(str(e))}},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class SubmitCycleCount(JSONApi):
    @staticmethod
    def post(request, task_id, shelf_id):
        try:
            usn_list = request.data['scanned_items']
            cc = CycleCount.objects.get(task_id=task_id)
            cycle_count_shelves = CycleCountShelves.objects.get(cycle_count_shelf__task_id=task_id, shelf_id=shelf_id)
            for usn in usn_list:
                current_shelf = Shelf.objects.get(id=shelf_id)
                serial_number = SerialNumber.objects.get(hash=usn)
                if serial_number.shelf.id == current_shelf.id:
                    exc = ExactCycleCountLine(cycle_count=cycle_count_shelves, shelf=current_shelf, usn=serial_number)
                    exc.save()

                elif serial_number.shelf.name == 'NOT FOUND':
                    adjust_inward = StockMovementInward.objects.filter(
                        to_shelf__name='NOT_FOUND', serials__hash=usn).values(
                        'serials__hash',
                        'created_at')[0:1]
                    for created in adjust_inward:
                        end_time = created['created_at'] + datetime.timedelta(minutes=1)
                        start_time = created['created_at'] - datetime.timedelta(minutes=1)
                        outwarded_shelf = StockMovementOutward.objects.filter(
                            created_at__range=(start_time, end_time),
                            serials__hash=created['serials__hash']).values_list(
                            'from_shelf', flat=True).first()
                        adjust_inward_picker = StockMovementInward.objects.filter(
                            to_shelf__name='NOT_FOUND', serials__hash=usn).annotate(hash=F('serials__hash'),
                                                                                    created_at_time=F(
                                                                                        'transaction__created_at')).order_by(
                            'hash', '-created_at_time').distinct('hash').values('hash', 'created_by__username')
                        if outwarded_shelf == current_shelf:
                            exc = MisPlacedCycleCountLine(cycle_count=cycle_count_shelves, shelf=current_shelf,
                                                          usn=serial_number
                                                          , picker_anomaly__picker__picker__username
                                                          =adjust_inward_picker['created_by__username'])
                            exc.save()
                else:
                    exc = ExcessCycleCountLine(cycle_count=cycle_count_shelves, shelf=current_shelf,
                                               previous_shelf=serial_number.shelf
                                               , usn=serial_number)
                    exc.save()
                transformed_items = transform_serials_to_item_with_serials([serial_number.hash])
                create_stock_movement_without_reservation(request=request,
                                                          items=transformed_items,
                                                          movement_type="both",
                                                          adjustment_type=StockAdjustmentType.CYCLE_COUNT,
                                                          source_shelf=serial_number.shelf,
                                                          destination_shelf=current_shelf,
                                                          content_model=None,
                                                          remarks='Inward while Cycle Count',
                                                          shelf_valid=False,task=task_id)
            not_found_usns = list(SerialNumber.objects.filter(Q(shelf=current_shelf) & ~Q(hash__in=usn_list)) \
                                  .values_list('hash', flat=True))
            destination_shelf = Shelf.objects.get(VirtualShelf___type=Shelf.VirtualShelfTypes.NOT_FOUND,
                                                  bay__stock_point_id=current_shelf.bay.stock_point.id)
            transformed_items = transform_serials_to_item_with_serials(not_found_usns)
            create_stock_movement_without_reservation(request=request,
                                                      items=transformed_items,
                                                      movement_type="both",
                                                      adjustment_type=StockAdjustmentType.NOT_FOUND,
                                                      source_shelf=current_shelf,
                                                      destination_shelf=destination_shelf,
                                                      content_model=None,
                                                      remarks='Moving Not Found while Cycle Count',
                                                      is_return=True, shelf_valid=False,task=task_id)
            for usn in not_found_usns:
                exc = NotFoundCycleCountLine(cycle_count=cycle_count_shelves, shelf=current_shelf,
                                             usn=SerialNumber.objects.get(hash=usn))
                exc.save()
            cc_shelf = CycleCountShelves.objects.get(cycle_count_shelf=cc, shelf=current_shelf)
            cc_shelf.status = cc_shelf.Status.COMPLETED
            cc_shelf.save()
            return JsonResponse(
                {'success': True, 'id': cc.id, "data": request.data}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({'success': False, "data": {"result": json.dumps(str(e))}},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CycleCountShelf(JSONApi):
    @staticmethod
    def get(request, task_id, stock_point_id):
        try:
            response_data = dict()
            process_type = 'put_away'
            operation_type = 'cycle_count'
            items = request.data['items'] if 'items' in request.data else []
            obj = WarehouseProcessType(items, request, task_id=task_id, stock_point_id=stock_point_id,
                                       order_id='', force_allocate=False).get_instance(process_type, operation_type)
            result, exceptions = obj.get_cycle_count_shelves()
            if len(exceptions) == 0:
                response_data['success'] = True
                response_data['shelves'] = result
                return JsonResponse(response_data, status=status.HTTP_200_OK)
            else:
                return JsonResponse({'success': False, "data": exceptions},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            return JsonResponse({'success': False, "data": e},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class CycleCountDetails(JSONApi):
    @staticmethod
    def get(request, task_id):
        return_list = list()
        response_data = dict()
        row = dict()
        try:
            cc_detail = CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id).values('shelf_id',
                                                                                                    'shelf__name',
                                                                                                    'picker_id',
                                                                                                    'picker__picker__username',
                                                                                                    'status',
                                                                                                    'cycle_lines__exactcyclecountline__usn__hash',
                                                                                                    'cycle_lines__misplacedcyclecountline__usn__hash',
                                                                                                    'cycle_lines__notfoundcyclecountline__usn__hash',
                                                                                                    'cycle_lines__excesscyclecountline__usn__hash',
                                                                                                    'cycle_lines__polymorphic_ctype_id',
                                                                                                    'cycle_lines__misplacedcyclecountline__picker_anomaly__picker__username',
                                                                                                    'cycle_lines__excesscyclecountline__previous_shelf__name',
                                                                                                    'cycle_count_shelf_id',
                                                                                                    'cycle_count_shelf__task__operation',
                                                                                                    )
            for cc in cc_detail:
                if cc['shelf_id'] in row:
                    if cc['cycle_lines__exactcyclecountline__usn__hash']:
                        row[cc['shelf_id']]['usnDetails']['inplace'].append(
                            cc['cycle_lines__exactcyclecountline__usn__hash'])
                    if cc['cycle_lines__excesscyclecountline__usn__hash']:
                        row[cc['shelf_id']]['usnDetails']['excess'].append({
                            "usn": cc['cycle_lines__excesscyclecountline__usn__hash'],
                            "previousShelf": cc[
                                'cycle_lines__excesscyclecountline__previous_shelf__name']
                        })
                    if cc['cycle_lines__notfoundcyclecountline__usn__hash']:
                        row[cc['shelf_id']]['usnDetails']['not_found'].append({
                            "usn": cc['cycle_lines__notfoundcyclecountline__usn__hash'],
                            "previousShelf": ''
                        })
                    if cc['cycle_lines__misplacedcyclecountline__usn__hash']:
                        row[cc['shelf_id']]['usnDetails']['not_found'].append({
                            "usn": cc['cycle_lines__misplacedcyclecountline__usn__hash'],
                            "previousShelf": ''
                        })
                else:
                    row[cc['shelf_id']] = {
                        "picker": cc['picker__picker__username'],
                        'id': cc['shelf_id'],
                        'bin': cc['shelf__name'],
                        "usnDetails": {"inplace": [cc['cycle_lines__exactcyclecountline__usn__hash']] if cc[
                            'cycle_lines__exactcyclecountline__usn__hash'] else [],
                                       "excess": [{
                                           "usn": cc['cycle_lines__excesscyclecountline__usn__hash'],
                                           "previousShelf": cc[
                                               'cycle_lines__excesscyclecountline__previous_shelf__name']
                                       }] if cc['cycle_lines__excesscyclecountline__usn__hash'] else [],
                                       "not_found": [{
                                           "usn": cc['cycle_lines__notfoundcyclecountline__usn__hash'],
                                           "previousShelf": ""
                                       }] if cc['cycle_lines__notfoundcyclecountline__usn__hash'] else [],
                                       "misplaced": [{
                                           "usn": cc['cycle_lines__misplacedcyclecountline__usn__hash'],
                                           "previousShelf": ""
                                       }] if cc['cycle_lines__misplacedcyclecountline__usn__hash'] else []
                                       },
                        'progress': 'Completed' if cc['status'] == 'C' else 'Not Started',
                    }
            for i, j in row.items():
                return_list.append(j)
            response_data['success'] = True
            response_data['data'] = return_list
            response_data['stock_points'] = list(
                StockPoint.objects.annotate(value=F('id'), label=F('name')).values('value', 'label'))
            response_data['progressChartData'] = [{
                "label": "Work In Progress",
                "count": CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id,
                                                          status=CycleCountShelves.Status.NOT_STARTED)
                .aggregate(
                    completed=Coalesce(Count('id'), Value(0)))['completed'],
                "part": float(CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id,
                                                               status=CycleCountShelves.Status.NOT_STARTED)
                              .aggregate(
                    completed=Coalesce(Count('id'), Value(0)))['completed'] /
                              len(CycleCount.objects.get(task_id=task_id).shelves.all())) * 100,
                "color": "#8F928A"
            },
                {
                    "label": "Completed",

                    "count": CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id,
                                                              status=CycleCountShelves.Status.COMPLETED)
                    .aggregate(
                        completed=Coalesce(Count('id'), Value(0)))['completed'],
                    "part": float(CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id,
                                                                   status=CycleCountShelves.Status.COMPLETED)
                                  .aggregate(
                        completed=Coalesce(Count('id'), Value(0)))['completed'] /
                                  len(CycleCount.objects.get(task_id=task_id).shelves.all())) * 100,
                    "color": "#12B886"
                },

            ]
            response_data['total_shelves'] = CycleCountShelves.objects.filter(cycle_count_shelf__task_id=task_id,
                                                                              ).aggregate(
                total=Coalesce(Count('id'), Value(0)))['total']

            excess_count = ExcessCycleCountLine.objects.filter(cycle_count__cycle_count_shelf__task_id=
                                                               task_id).aggregate(
                excess=Coalesce(Count('id'), Value(0)))['excess']
            not_found = NotFoundCycleCountLine.objects.filter(cycle_count__cycle_count_shelf__task_id=
                                                              task_id).aggregate(
                not_found=Coalesce(Count('id'), Value(0)))['not_found']
            total_scanned = CycleCountLines.objects.filter(cycle_count__cycle_count_shelf__task_id=
                                                           task_id).aggregate(
                total=Coalesce(Count('id'), Value(0)))['total'] - not_found
            total_inventory = excess_count + not_found

            inventory_accuracy = 0 if total_inventory == 0 else 1 - (total_inventory / total_scanned) * 100

            excess_accuracy = 0 if excess_count == 0 else (excess_count / total_scanned) * 100

            not_found_accuracy = 0 if not_found == 0 else (not_found / total_scanned) * 100

            response_data["statsRingData"] = [
                {"label": "Inventory Record Accuracy", "stats": inventory_accuracy, "percentage": inventory_accuracy,
                 "color": "",
                 "icon": "average"},
                {"label": "Effective Misplaced Ratio", "stats": excess_count, "percentage": excess_accuracy,
                 "color": "",
                 "icon": "average"},
                {"label": "Effective Not Found Ratio", "stats": not_found, "percentage": not_found_accuracy,
                 "color": "",
                 "icon": "average"}]
            return JsonResponse(response_data, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"data": {"result": json.dumps(str(e))}}, status=500)
