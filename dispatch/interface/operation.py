import datetime
from abc import ABCMeta, abstractmethod, ABC
import pytz
from django.contrib.postgres.aggregates import ArrayAgg
from django.db import transaction, connection
from django.db.models import Sum, DecimalField, Q, Count, IntegerField, F, OuterRef, CharField, Subquery, Case, Exists, \
    When, Value
from django.db.models.functions import Coalesce, Cast

from dispatch.models import *
from inventory.function import create_stock_movement, create_stock_movement_without_reservation, \
    create_new_item_reservation
from inventory.models import Stock, PickerConfig, SerialNumber, Shelf, PhysicalShelf, \
    StockAdjustmentType, VirtualShelf, Bay, StockMovementInward
from master.models import Item
import logging

# from silk.profiling.profiler import silk_profile
logger = logging.getLogger(__name__)

utc = pytz.UTC


class WarehouseProcessType(metaclass=ABCMeta):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id=0, operation_type=None,
                 task_type=None, stock_point_id=None,
                 shelf_id=None, hash=None, bay_id=None, picker_id=None, remarks=None):
        # Set All values here
        self.items = items
        self.request = request
        self.order_id = order_id
        self.task_id = task_id
        self.force_allocate = True if force_allocate in ['True', True] else False
        self.stock_point_id = stock_point_id
        self.operation_type = operation_type
        self.task_type = task_type
        self.shelf_id = shelf_id
        self.hash = hash
        self.picker_id = picker_id
        self.remarks = remarks
        self.bay_id = bay_id

    def get_instance(self, process_type, operation):
        # usage: returns instance for the type of operation.
        if process_type == 'picking':
            if operation in ['pick_slip', 'allocate_picker']:
                return PickSlipOperation(self.items, self.request, self.order_id, force_allocate=self.force_allocate,
                                         task_id=self.task_id, stock_point_id=self.stock_point_id,
                                         shelf_id=self.shelf_id,
                                         hash=self.hash, bay_id=self.bay_id, picker_id=self.picker_id,
                                         remarks=self.remarks, operation_type=self.operation_type,
                                         task_type=self.task_type)
        elif process_type == 'put_away':
            if operation == 'inward':
                return Inward(self.items, self.request, self.order_id, force_allocate=self.force_allocate,
                              task_id=self.task_id,
                              stock_point_id=self.stock_point_id, shelf_id=self.shelf_id,
                              hash=self.hash, bay_id=self.bay_id, picker_id=self.picker_id, remarks=self.remarks,
                              task_type=self.task_type, operation_type=self.operation_type
                              )
            if operation == 'cycle_count':
                return CycleCountTask(self.items, self.request, self.order_id, force_allocate=self.force_allocate,
                                      task_id=self.task_id,
                                      stock_point_id=self.stock_point_id, shelf_id=self.shelf_id,
                                      hash=self.hash, bay_id=self.bay_id, picker_id=self.picker_id,
                                      remarks=self.remarks,
                                      task_type=self.task_type, operation_type=self.operation_type
                                      )

        else:
            raise ValueError('operation does not exist')


class WarehouseProcessTypeInterface(WarehouseProcessType):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id=0, operation_type=None,
                 task_type=None, stock_point_id=None,
                 shelf_id=None, hash=None, bay_id=None, picker_id=None, remarks=None):
        super().__init__(items, request, order_id, force_allocate, task_id, operation_type, task_type, stock_point_id,
                         shelf_id, hash, bay_id,
                         picker_id,
                         remarks)
        self.exception_list = []
        self.result = {'bays': []}
        self.allocation = None
        self.temp_data = {}

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'pre_process') and callable(subclass.pre_process) and
                hasattr(subclass, 'create_reservation') and callable(subclass.create_reservation) and
                hasattr(subclass, 'get_reservation') and callable(subclass.get_reservation) and
                hasattr(subclass, 'post_process') and callable(subclass.post_process) and
                hasattr(subclass, 'on_scan_usn') and callable(subclass.on_scan_usn))

    @abstractmethod
    def pre_process(self):
        if len(self.items) == 0:
            raise ValueError('Items are not provided')
        if self.order_id == '':
            raise ValueError('Order Id is Invalid')
        self.excluded_shelfs = []
        for shelf_obj in Shelf.VirtualShelfTypes:
            if shelf_obj.name != 'STAGING_AREA': # Commenting as perclient advice they are not picking from Staging area
                self.excluded_shelfs.append(shelf_obj)

    @abstractmethod
    def post_process(self):
        return self.result, self.exception_list

    @abstractmethod
    def create_task(self):
        task = Task.objects.create(
            type=self.task_type, operation=self.operation_type,
            associated_transaction=self.order_id, request=self.request.data, remarks=self.remarks)
        return task

    @abstractmethod
    def create_reservation(self):
        pass

    @abstractmethod
    def on_scan_usn(self):
        pass

    @abstractmethod
    def get_reservation(self):
        self.post_process()

    @abstractmethod
    def check_usn_item_mapping(self):
        for item in self.items:
            serials = SerialNumber.objects.filter(item__sku_code=item['sku_code'], hash__in=item['usns']).values('id')
            if len(serials) != len(item['usns']):
                raise Exception(f'usns{item["usns"]} sku {item["sku_code"]} mapping invalid')


class Picking(WarehouseProcessTypeInterface, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id, operation_type, task_type,
                 stock_point_id, shelf_id, hash,
                 bay_id, picker_id, remarks):
        super(Picking, self).__init__(items=items, request=request, order_id=order_id, force_allocate=force_allocate,
                                      task_id=task_id, stock_point_id=stock_point_id, shelf_id=shelf_id, hash=hash,
                                      bay_id=bay_id, picker_id=picker_id, remarks=remarks, task_type=task_type,
                                      operation_type=operation_type)

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'get_pending_stock') and callable(subclass.get_pending_stock) and
                hasattr(subclass, 'assign_picker') and callable(subclass.assign_picker) and
                hasattr(subclass, 'get_wip_stock') and callable(subclass.get_wip_stock)
                )

    def pre_process(self):
        return super(Picking, self).pre_process()

    @abstractmethod
    def get_pending_stock(self):
        temp_data = {}
        raw_query_items_format = []
        for item in self.items:
            raw_query_items_format.append(f"('{item['sku_code']}')")
        raw_query_items_format = ','.join(raw_query_items_format)
        result = get_stock_by_raw_query(self, raw_query_items_format)
        '''
        do not get confused in below for loop on seeing index these are the explanation for indexes
        0 -itemid
        1-sku_code
        2-shelf_id
        3-total_stock
        4-reserved_stock
        5-picked quantity
        '''
        modified_result = {}
        for item_details in result:
            if item_details[1].strip() in modified_result:
                modified_result[item_details[1].strip()]['total_stock'] += item_details[3]
                modified_result[item_details[1].strip()]['total_reserved_stock'] += item_details[4]
                modified_result[item_details[1].strip()]['total_picked_quantity'] += item_details[5]
                stock_available_shelf = (float(item_details[3]) - float(item_details[4] - item_details[5]))
                modified_result[item_details[1].strip()]['shelves'].append(
                    {'shelf_id': item_details[2], 'original_stock': stock_available_shelf,
                     'reserved_stock': item_details[4],
                     'picked_quantity': item_details[5]})
            else:
                modified_result[item_details[1].strip()] = {'total_stock': item_details[3], 'item_id': item_details[0],
                                                            'total_reserved_stock': item_details[4],
                                                            'total_picked_quantity': item_details[5], 'shelves': [
                        {'shelf_id': item_details[2], 'original_stock':
                            (float(item_details[3]) - float(item_details[4] - item_details[5])),
                         'reserved_stock': item_details[4], 'picked_quantity': item_details[5]}]}
        for item_detail in self.items:
            try:
                # todo we need to fix in future like if a piece is picked and again if kept for some purpose this will create discrepancy.
                total_picked_quantity = modified_result[item_detail['sku_code']]['total_picked_quantity'] if \
                    item_detail['sku_code'] in modified_result else 0
                total_reserved_stock = modified_result[item_detail['sku_code']]['total_reserved_stock'] if item_detail[
                                                                                                               'sku_code'] in modified_result else 0
                current_stock = modified_result[item_detail['sku_code']]['total_stock'] if item_detail[
                                                                                               'sku_code'] in modified_result else 0
                '''
                formula applied current_stock-effective_reserved_stock
                '''
                stock_available = (float(current_stock) - float(total_reserved_stock - total_picked_quantity))
                if (stock_available > 0 and item_detail['quantity'] > 0) and (
                        item_detail['quantity'] <= stock_available):
                    temp_data[item_detail['sku_code']] = {'quantity': item_detail['quantity'],
                                                          'item_id': modified_result[item_detail['sku_code']][
                                                              'item_id'],
                                                          'available': stock_available,
                                                          'shelves': modified_result[item_detail['sku_code']][
                                                              'shelves'] if item_detail[
                                                                                'sku_code'] in modified_result else []}
                else:
                    raise Stock.DoesNotExist
            except Stock.DoesNotExist:
                self.exception_list.append(f'Item {item_detail["sku_code"]} Not Available in Stock')
            except Exception as e:
                self.exception_list.append(str(e))
        self.result = temp_data
        return self.result, self.exception_list

    @abstractmethod
    def get_wip_stock(self):
        temp_data = {}
        for item in self.items:
            try:
                wip_stock = Stock.objects.filter(Q(item__sku_code=item['sku_code']) &
                                                 Q(shelf__bay__stock_point_id=self.stock_point_id) &
                                                 Q(shelf__virtualshelf__type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS)
                                                 ).aggregate(
                    total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))['total']

                if wip_stock > 0:
                    temp_data[item['sku_code']] = {
                        'available_quantity': float(wip_stock)}
                else:
                    raise Stock.DoesNotExist
            except Stock.DoesNotExist:
                self.exception_list.append(f'Item {item["sku_code"]} Not Available in Stock')
            except Exception as e:
                self.exception_list.append(str(e))
        self.result = temp_data
        return self.result, self.exception_list

    def post_process(self):
        return super(Picking, self).post_process()

    def create_task(self):
        self.task_type = Task.Type.PICKING
        self.operation_type = Task.Operation.PICK_SLIP
        return super(Picking, self).create_task()

    def create_reservation(self):
        super(Picking, self).pre_process()
        total_quantity_requested = 0
        for item_details in self.items:
            total_quantity_requested += int(item_details['quantity'])
        result, exceptions = self.get_pending_stock()
        if len(exceptions) > 0:
            return self.result, self.exception_list
        self.task_type = Task.Type.PICKING
        self.operation_type = Task.Operation.PICK_SLIP
        self.exception_list = []
        with transaction.atomic():
            try:
                task = Task.objects.create(
                    type=self.task_type, operation=self.operation_type,
                    associated_transaction=self.order_id, request=self.request.data, remarks=self.remarks)
                for sku_code, sku_detail in self.result.items():
                    # logger.debug(f'Starting Allocation for {sku_code}')
                    allocated_qty = 0
                    required_qty = int(sku_detail['quantity'])
                    for stock in sku_detail['shelves']:
                        if required_qty == 0:
                            break
                        shelf_qty = stock['original_stock']
                        if allocated_qty < int(sku_detail['quantity']):
                            if shelf_qty > 0:
                                if shelf_qty >= required_qty:
                                    allocation = StockReservation(task=task, item_id=sku_detail['item_id'],
                                                                  quantity=required_qty,
                                                                  shelf_id=stock['shelf_id'])
                                    allocation.save()
                                    # logger.debug(
                                    #     f'Reservation created for {sku_code} with quantity {shelf_qty} with shelf name {Shelf.objects.get(id=stock["shelf_id"]).name}')
                                    allocated_qty += required_qty
                                    required_qty = 0
                                else:
                                    allocation = StockReservation(task=task, item_id=sku_detail['item_id'],
                                                                  quantity=shelf_qty,
                                                                  shelf_id=stock['shelf_id'])
                                    allocation.save()
                                    # logger.debug(
                                    #     f'Reservation created for {sku_code} with quantity {shelf_qty} with shelf name {Shelf.objects.get(id=stock["shelf_id"]).name}')
                                    required_qty -= shelf_qty
                                    allocated_qty += shelf_qty
                        else:
                            break
                if total_quantity_requested == task.task_reserve.aggregate(
                        total=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total']:
                    self.result = task.id
                    self.assign_picker()
                else:
                    logger_dict = {'title': 'Task Creation Failure', 'remarks': task.remarks,
                                   'pick_slip_id': task.associated_transaction,
                                   'items': []}
                    for item_detail in self.items:
                        stock_reserved_quantity = StockReservation.objects.filter(task_id=task.id,
                                                                                  item__sku_code=item_detail[
                                                                                      'sku_code']).aggregate(
                            total=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total']
                        if int(item_detail['quantity']) != stock_reserved_quantity:
                            logger_dict['items'].append(
                                f'Item {item_detail["sku_code"]} req Qty {item_detail["quantity"]} Alloc Qty {stock_reserved_quantity}')
                            self.exception_list.append(f'Item {item_detail["sku_code"]} Not Available in Stock')
                    print('PickSlip Requested and Allocated Quantity mismatch')
                    logger.error(logger_dict)
                    raise Exception('PickSlip Requested and Allocated Quantity mismatch')


            except Exception as e:
                transaction.set_rollback(True)
                self.exception_list.append(f'issue during reservation {str(e)}')
                return self.result, self.exception_list
        # else:
        #     self.exception_list.append('Allocation cannot be done since stock not available')

    def check_usn_item_mapping(self):
        super(Picking, self).check_usn_item_mapping()

    def get_reservation(self):
        picker_config = PickerConfig.objects.first().type
        result = []
        try:
            task_id = Task.objects.get(id=self.task_id)
            stock_reservations = StockReservation.objects.filter(task_id=task_id, valid=True).all()
            for stock_reservation in stock_reservations:
                if picker_config == 'AF':
                    exclude_shelfs = []
                    for shelf_obj in Shelf.VirtualShelfTypes:
                        if shelf_obj.name != 'STAGING_AREA':
                            exclude_shelfs.append(shelf_obj)
                    serials = list(SerialNumber.objects.filter(
                        item__sku_code=stock_reservation.item.sku_code,
                        shelf__bay__stock_point_id=self.stock_point_id,
                        shelf_id=stock_reservation.shelf.id, shelf__is_picking_blocked=False).exclude(
                        shelf__virtualshelf__type__in=exclude_shelfs).order_by(
                        'batch__created_at', 'item', 'item__sku_code', 'hash', 'shelf', 'shelf__bay__name',
                        'shelf__bay__stock_point__name').values('shelf__name', 'hash',
                                                                'item', 'item__image', 'item__sku_code',
                                                                'shelf__bay__name', 'shelf__bay_id', 'shelf__id',
                                                                'shelf__bay__stock_point__name'))
                    for serial in serials:
                        size_attributes = Item.objects.filter(sku_code=serial['item__sku_code']).annotate(
                            key=F('attributes__key__name'), value=F('attributes__value')).values('key', 'value')
                        final_data = get_attributes_for_item(size_attributes)

                        stock_point_exist, stock_point = is_stock_point_exist(serial['shelf__bay__stock_point__name'],
                                                                              result)
                        if not stock_point_exist:
                            result.append({'name': serial['shelf__bay__stock_point__name'], 'type': 'ST',
                                           'children': [
                                               {'name': serial['shelf__bay__name'], 'type': 'BAY',
                                                'id': serial['shelf__bay_id'],
                                                'children': [
                                                    {"name": serial['shelf__name'], 'type': "SH",
                                                     'id': serial['shelf__id'],
                                                     'data': [{'sku_code': serial['item__sku_code'],
                                                               'stock': int(stock_reservation.get_pending_usn_count),
                                                               'size': final_data['size'], 'shade': final_data['shade'],
                                                               'description': final_data['description'],
                                                               'image': serial['item__image'],
                                                               'design': final_data['design'],
                                                               'usns': [serial['hash']]}]

                                                     }]}]})
                        else:
                            exist_bay, bay = is_bay_exist(serial['shelf__bay__name'], stock_point['children'])
                            if not exist_bay:
                                stock_point['children'].append(
                                    {'name': serial['shelf__bay__name'], 'type': 'BAY', 'id': serial['shelf__bay_id'],
                                     'children': [
                                         {"name": serial['shelf__name'], 'type': "SH", 'id': serial['shelf__id'],
                                          'data': [{'sku_code': serial['item__sku_code'],
                                                    'stock': int(stock_reservation.get_pending_usn_count),
                                                    'size': final_data['size'], 'shade': final_data['shade'],
                                                    'image': serial['item__image'],
                                                    'design': final_data['design'],
                                                    'description': final_data['description'],
                                                    'usns': [serial['hash']]}]
                                          }]}
                                )
                            else:
                                exists, shelf = is_shelf_exists(serial['shelf__name'], bay['children'])
                                if not exists:
                                    bay['children'].append(
                                        {"name": serial['shelf__name'], 'type': "SH", 'id': serial['shelf__id'],
                                         'data': [{'sku_code': serial['item__sku_code'],
                                                   'stock': int(stock_reservation.get_pending_usn_count),
                                                   'size': final_data['size'],
                                                   'image': serial['item__image'], 'shade': final_data['shade'],
                                                   'design': final_data['design'],
                                                   'description': final_data['description'],
                                                   'usns': [serial['hash']]}]})
                                else:
                                    exists, item = is_item_exist_in_shelf(shelf['data'], serial['item__sku_code'])
                                    if exists:
                                        item['usns'].append(serial['hash'])
                                    else:
                                        shelf['data'].append({'sku_code': serial['item__sku_code'],
                                                              'stock': int(stock_reservation.get_pending_usn_count),
                                                              'size': final_data['size'], 'shade': final_data['shade'],
                                                              'design': final_data['design'],
                                                              'image': serial['item__image'],
                                                              'description': final_data['description'],
                                                              'usns': [serial['hash']]})

            self.result = result
        except Task.DoesNotExist:
            self.exception_list.append(f'Task {self.task_id} Does not exist ')

        except Exception as e:
            self.exception_list.append(f'Problem in retrieving Task id {str(e)}')

    def not_found_items(self, sku_code_usn, only_invalid=False):
        """[{'sku_code': sku_code, 'usns': usns, 'quantity': len(usns)"""
        for detail in sku_code_usn:
            item_obj = Item.objects.get(sku_code=detail['sku_code'])
            reservation = StockReservation.objects.get(task_id=self.task_id, shelf_id=self.shelf_id, valid=True,
                                                       item=item_obj)
            if not only_invalid:
                reservation.quantity -= detail['quantity']
            reservation.valid = False
            reservation.save()
            break
        return True

    @transaction.atomic()
    def on_scan_usn(self):
        super(Picking, self).check_usn_item_mapping()
        if self.check_stock_reservation():
            try:
                for item_details in self.items:
                    serial_number = SerialNumber.objects.get(hash__in=item_details['usns'],
                                                             item=Item.objects.get(sku_code=item_details['sku_code']))
                    destination_shelf = Shelf.objects.get(bay__stock_point_id=serial_number.shelf.bay.stock_point_id,
                                                          VirtualShelf___type=Shelf.VirtualShelfTypes.WORK_IN_PROGRESS)

                    success, exception = create_stock_movement(self=self, adjustment_type=StockAdjustmentType.PICKING,
                                                               source_shelf=serial_number.shelf,
                                                               destination_shelf=destination_shelf,
                                                               movement_type='both')
                    if success == 0:
                        raise Exception(exception)
                    self.result = 'scan ok'
            except Exception as e:
                transaction.set_rollback(True)
                self.exception_list.append(f'{str(e)}')
        else:
            try:
                dispatched_shelf = Shelf.VirtualShelfTypes.DISPATCHED.value
                wip_shelf = Shelf.VirtualShelfTypes.WORK_IN_PROGRESS.value
                for item_details in self.items:

                    reserve_qty = StockReservation.objects.filter(task_id=self.task_id,
                                                                  item__sku_code=item_details['sku_code']).aggregate(
                        total=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                    picked = StockMovementInward.objects.filter(associated_transaction_id=self.task_id,
                                                                item=item_details['sku_code'],
                                                                adjustment_type=StockAdjustmentType.PICKING) \
                        .aggregate(total_count=Coalesce(Sum('quantity'), 0, output_field=DecimalField()))
                    if reserve_qty['total'] <= picked['total_count']:
                        raise Exception('Picking completed for the Task')

                    if StockReservation.objects.filter(
                            Q(item__sku_code=item_details['sku_code']) & Q(task_id=self.task_id) &
                            Q(valid=True)).exists():

                        serial_number = SerialNumber.objects.get(hash__in=item_details['usns'],
                                                                 item=Item.objects.get(
                                                                     sku_code=item_details['sku_code']))
                        if serial_number.shelf and serial_number.shelf.polymorphic_ctype.model == 'virtualshelf' and \
                                str(serial_number.shelf.virtualshelf.type) in [dispatched_shelf, wip_shelf]:
                            raise Exception('Picking cannot be done for Dispatched Items or Already Picked Item')
                        destination_shelf = \
                            Shelf.objects.get(id=VirtualShelf.objects.get_or_create(
                                bay__stock_point_id=serial_number.shelf.bay.stock_point_id,
                                VirtualShelf___type=wip_shelf)[
                                0].shelf_ptr_id)
                        success, exception = create_stock_movement_without_reservation(items=self.items,
                                                                                       adjustment_type=StockAdjustmentType.PICKING,
                                                                                       source_shelf=serial_number.shelf,
                                                                                       destination_shelf=destination_shelf,
                                                                                       remarks='Adhoc Picking',
                                                                                       movement_type='both',
                                                                                       request=self.request,
                                                                                       task=self.task_id)

                    else:
                        raise Exception('Invalid Item')
                    if success == 0:
                        raise Exception(exception)
                    self.result = 'Item Picked'
            except Exception as e:
                transaction.set_rollback(True)
                self.exception_list.append(f'{str(e)}')

    @transaction.atomic()
    def not_found_usn(self):
        try:
            source_shelf = Shelf.objects.get(id=self.shelf_id)
            destination_shelf = Shelf.objects.get(bay__stock_point_id=self.stock_point_id,
                                                  VirtualShelf___type=Shelf.VirtualShelfTypes.NOT_FOUND)
            success, exception = create_stock_movement_without_reservation(request=self.request, items=self.items,
                                                                           movement_type='both',
                                                                           source_shelf=source_shelf,
                                                                           destination_shelf=destination_shelf,
                                                                           adjustment_type=
                                                                           StockAdjustmentType.NOT_FOUND,
                                                                           task=self.task_id)
            reservation, exception = create_new_item_reservation(self, items=self.items, source_shelf=source_shelf,
                                                                 destination_shelf=destination_shelf,
                                                                 )
            self.not_found_items(self.items, only_invalid=False if len(reservation) > 0 else True)
            if success and reservation == 0:
                raise Exception(exception)
            self.result = reservation
        except Exception as e:
            transaction.set_rollback(True)
            self.exception_list.append(f'{str(e)}')

    @transaction.atomic()
    def assign_picker(self):
        picker_list = list(PickerCheckIn.objects.filter(created_at__date=datetime.datetime.today().date()).distinct(
            'picker_id').values_list('picker_id', flat=True))
        override_auto_picker_assign = False
        provided_user_id = self.picker_id
        if self.request.user.is_superuser and provided_user_id is not None:
            override_auto_picker_assign = True
        pending_tasks = list(
            Task.objects.filter(Q(valid=True) & Q(Q(picker__isnull=True) | Q(picker=None))).order_by(
                'created_at').values_list('id', flat=True))
        if not override_auto_picker_assign:
            idle_pickers = list(
                Task.objects.filter(valid=False, created_at__date=datetime.datetime.today()).order_by(
                    'picked_time').values_list('picker__id', flat=True).order_by('picker__id').distinct('picker__id'))
            working_pickers = list(
                Task.objects.filter(Q(valid=True) & ~Q(Q(picker__isnull=True) | Q(picker=None))).order_by(
                    'associated_transaction').values_list('picker__id', flat=True).order_by('picker__id').distinct(
                    'picker__id'))
            idle_pickers = [picker for picker in idle_pickers if picker not in working_pickers]
            picker_dup_list = picker_list.copy()
            for picker in picker_list:
                if picker in idle_pickers:
                    picker_dup_list.remove(picker)
                if picker in working_pickers:
                    picker_dup_list.remove(picker)

            idle_pickers = picker_dup_list + idle_pickers
            for task in pending_tasks:
                for picker in idle_pickers:
                    task = Task.objects.get(id=task)
                    task.picker_id = picker
                    task.assigned_time = datetime.datetime.now()
                    task.save()
                    idle_pickers.remove(picker)
                    # self.result = 'Assign Success'
                    break

        else:
            task = Task.objects.get(id=self.task_id)
            for picker in self.picker_id:
                task.picker.add(Picker.objects.get(id=picker))
            task.assigned_time = datetime.datetime.now()
            task.save()
            # self.result = 'Assign Success'

        return 'Assign Success', self.exception_list

    def check_stock_reservation(self):
        for item in self.items:
            item_obj = Item.objects.get(sku_code=item['sku_code'])
            for usn in item['usns']:
                current_shelf = SerialNumber.objects.get(hash=usn)
                reservation = StockReservation.objects.filter(item=item_obj, task_id=self.task_id,
                                                              shelf_id=current_shelf.shelf_id).exists()
                if reservation:
                    return True


class PickSlipOperation(Picking, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id, operation_type, task_type,
                 stock_point_id, shelf_id, hash
                 , bay_id, picker_id, remarks):
        super(PickSlipOperation, self).__init__(items=items, request=request, order_id=order_id,
                                                force_allocate=force_allocate, task_id=task_id,
                                                stock_point_id=stock_point_id,
                                                shelf_id=shelf_id, hash=hash, bay_id=bay_id,
                                                picker_id=picker_id, remarks=remarks, operation_type=operation_type,
                                                task_type=task_type)

    def pre_process(self):
        self.operation_type = Task.Operation.PICK_SLIP
        return super(PickSlipOperation, self).pre_process()

    def get_pending_stock(self):
        super(PickSlipOperation, self).get_pending_stock()
        return super(PickSlipOperation, self).post_process()

    def post_process(self):
        return super(PickSlipOperation, self).post_process()

    def create_reservation(self):
        super(PickSlipOperation, self).create_reservation()
        return super(PickSlipOperation, self).post_process()

    def on_scan_usn(self):
        super(PickSlipOperation, self).on_scan_usn()
        return super(PickSlipOperation, self).post_process()

    def check_usn_item_mapping(self):
        return super(PickSlipOperation, self).check_usn_item_mapping()

    def check_stock_reservation(self):
        return super(PickSlipOperation, self).check_stock_reservation()

    def get_reservation(self):
        super(PickSlipOperation, self).get_reservation()
        return super(PickSlipOperation, self).post_process()

    def not_found_usn(self):
        super(PickSlipOperation, self).not_found_usn()
        return self.post_process()

    def create_task(self):
        super(PickSlipOperation, self).create_task()

    def get_wip_stock(self):
        return super(PickSlipOperation, self).get_wip_stock()


class BinConsolidation(Picking, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id, operation_type, task_type,
                 stock_point_id, shelf_id, hash
                 , bay_id, picker_id, remarks):
        super(BinConsolidation, self).__init__(items=items, request=request, order_id=order_id,
                                               force_allocate=force_allocate, task_id=task_id,
                                               stock_point_id=stock_point_id,
                                               shelf_id=shelf_id, hash=hash, bay_id=bay_id,
                                               picker_id=picker_id, remarks=remarks, operation_type=operation_type,
                                               task_type=task_type)

    def pre_process(self):
        self.operation_type = Task.Operation.BIN_CONSOLIDATION
        return super(BinConsolidation, self).pre_process()

    def post_process(self):
        return super(BinConsolidation, self).post_process()

    def not_found_usn(self):
        super(BinConsolidation, self).not_found_usn()

    def create_task(self):
        super(BinConsolidation, self).create_task()

    def get_wip_stock(self):
        return super(BinConsolidation, self).get_wip_stock()


class PutAway(WarehouseProcessTypeInterface, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id, operation_type, task_type,
                 stock_point_id, shelf_id, hash,
                 bay_id, picker_id, remarks):
        super(PutAway, self).__init__(items=items, request=request, order_id=order_id, force_allocate=force_allocate,
                                      task_id=task_id, stock_point_id=stock_point_id, shelf_id=shelf_id, hash=hash,
                                      bay_id=bay_id,
                                      picker_id=picker_id, remarks=remarks, task_type=task_type,
                                      operation_type=operation_type)

    @classmethod
    def __subclasshook__(cls, subclass):
        return hasattr(subclass, 'get_available_shelf_quantity') and callable(subclass.get_available_shelf_quantity)

    def pre_process(self):
        return super(PutAway, self).pre_process()

    def create_reservation(self):
        pass

    def create_task(self):
        pass

    def on_scan_usn(self):
        pass

    @abstractmethod
    def get_available_shelf_quantity(self):
        try:
            physical_shelf = PhysicalShelf.objects.get(shelf_ptr_id=self.shelf_id, is_put_away_blocked=False)
        except PhysicalShelf.DoesNotExist:
            raise Exception('Cannot Proceed since shelf is invalid or it is blocked for putaway')
        reserved_capacity = \
            SerialNumber.objects.filter(shelf_id=physical_shelf.shelf_ptr_id).aggregate(total_quantity=Count('hash'))[
                'total_quantity']
        self.result = {'capacity': physical_shelf.capacity, 'name': physical_shelf.name,
                       'remaining': physical_shelf.capacity - reserved_capacity}
        return self.post_process()

    def check_usn_item_mapping(self):
        super(PutAway, self).check_usn_item_mapping()

    def post_process(self):
        return super(PutAway, self).post_process()


class Inward(PutAway, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id, stock_point_id, shelf_id, hash,
                 bay_id, task_type, operation_type,
                 picker_id, remarks):
        super(Inward, self).__init__(items=items, request=request, order_id=order_id,
                                     force_allocate=force_allocate, task_id=task_id, stock_point_id=stock_point_id,
                                     shelf_id=shelf_id, hash=hash, bay_id=bay_id, picker_id=picker_id,
                                     remarks=remarks, task_type=task_type,
                                     operation_type=operation_type)

    def pre_process(self):
        super(Inward, self).pre_process()

    def get_reservation(self):
        pass

    def check_usn_item_mapping(self):
        super(Inward, self).check_usn_item_mapping()

    def post_process(self):
        return super(Inward, self).post_process()

    def on_scan_usn(self):
        for item in self.items:
            for shelf, j in item.items():
                for inner_detail in j:
                    if SerialNumber.objects.filter(hash__in=inner_detail['usns'],
                                                   shelf__virtualshelf__type=Shelf.VirtualShelfTypes.STAGING_AREA,
                                                   shelf__id=shelf).exists():
                        destination_shelf = Shelf.objects.get(id=shelf)
                        success, exception = create_stock_movement_without_reservation(request=self.request,
                                                                                       items=j,
                                                                                       movement_type='both',
                                                                                       destination_shelf=destination_shelf,
                                                                                       source_shelf=Shelf.objects.get(
                                                                                           bay_id=destination_shelf.bay.id,
                                                                                           VirtualShelf___type=Shelf.VirtualShelfTypes.STAGING_AREA),
                                                                                       adjustment_type=StockAdjustmentType.PUT_AWAY)
                        if success == 0:
                            raise Exception(exception)
                        self.result = 'Put away Done'
                    else:
                        self.result = 'Put away Done'
            return self.post_process()

    def simple_put_away(self):
        self.result = dict()
        self.result['shelf_info'] = list()
        for item in self.items:
            for shelf, item_details in item.items():
                try:
                    physical_shelf = PhysicalShelf.objects.get(shelf_ptr_id=shelf, is_put_away_blocked=False)
                    reserved_capacity = \
                        SerialNumber.objects.filter(shelf_id=physical_shelf.shelf_ptr_id).aggregate(
                            total_quantity=Count('hash'))[
                            'total_quantity']
                    self.result['shelf_info'].append({'capacity': physical_shelf.capacity, 'name': physical_shelf.name,
                                                      'remaining': physical_shelf.capacity - reserved_capacity})
                except PhysicalShelf.DoesNotExist:
                    raise Exception('Cannot Proceed since Shelf is invalid or it is blocked for putaway !!!')
                inward_list = {}
                both_list = {}
                destination_shelf = Shelf.objects.get(id=shelf)
                for details in item_details:
                    for usn in details['usns']:
                        if SerialNumber.objects.filter(hash=usn, shelf=None).exists():
                            current_shelf = destination_shelf
                            if current_shelf in inward_list:
                                if details['sku_code'] in inward_list[current_shelf]:
                                    if usn not in inward_list[current_shelf][details['sku_code']]['usns']:
                                        inward_list[current_shelf][details['sku_code']]['usns'].append(usn)
                                else:
                                    inward_list[current_shelf][details['sku_code']] = {'sku_code': details['sku_code'],
                                                                                       'usns': [usn]}
                            else:
                                inward_list[current_shelf] = {
                                    details['sku_code']: {'sku_code': details['sku_code'], 'usns': [usn]}}
                        else:
                            previous_shelf = SerialNumber.objects.get(hash=usn).shelf
                            if previous_shelf in both_list:
                                if details['sku_code'] in both_list[previous_shelf]:
                                    if usn not in both_list[previous_shelf][details['sku_code']]['usns']:
                                        both_list[previous_shelf][details['sku_code']]['usns'].append(usn)
                                else:
                                    both_list[previous_shelf][details['sku_code']] = {'sku_code': details['sku_code'],
                                                                                      'usns': [usn]}
                            else:
                                both_list[previous_shelf] = {
                                    details['sku_code']: {'sku_code': details['sku_code'], 'usns': [usn]}}

                if len(both_list):
                    for tempShelf, item_inner_details in both_list.items():
                        data = [temp_item_details for barcode, temp_item_details in item_inner_details.items()]

                        success, exception = create_stock_movement_without_reservation(request=self.request,
                                                                                       items=data,
                                                                                       movement_type='both',
                                                                                       source_shelf=tempShelf,
                                                                                       destination_shelf=destination_shelf,
                                                                                       adjustment_type=StockAdjustmentType.PUT_AWAY)
                        if success == 0:
                            raise Exception(exception)
                if len(inward_list):
                    for tempShelf, inner_details in inward_list.items():
                        data = [temp_item_details for barcode, temp_item_details in inner_details.items()]
                        success, exception = create_stock_movement_without_reservation(request=self.request,
                                                                                       items=data,
                                                                                       movement_type='inward',
                                                                                       destination_shelf=destination_shelf,
                                                                                       adjustment_type=StockAdjustmentType.PUT_AWAY)

                        if success == 0:
                            raise Exception(exception)
        self.result['status'] = 'Put away Done'
        return self.post_process()

    def get_available_shelf_quantity(self):
        return super(Inward, self).get_available_shelf_quantity()

    def create_task(self):
        self.task_type = Task.Type.PUT_AWAY
        self.operation_type = 'INWARD'
        super(PutAway, self).create_task()


class CycleCountTask(PutAway, ABC):
    def __init__(self, items: list, request, order_id: str, force_allocate, task_id=0, operation_type=None,
                 task_type=None, stock_point_id=None,
                 shelf_id=None, hash=None, bay_id=None, picker_id=None, remarks=None):
        super(CycleCountTask, self).__init__(items=items, request=request, order_id=order_id,
                                             force_allocate=force_allocate, task_id=task_id,
                                             stock_point_id=stock_point_id,
                                             shelf_id=shelf_id, hash=hash, task_type=task_type,
                                             operation_type=operation_type, bay_id=bay_id, picker_id=picker_id,
                                             remarks=remarks)

    def pre_process(self):
        super(CycleCountTask, self).pre_process()

    def get_reservation(self):
        pass

    def check_usn_item_mapping(self):
        super(CycleCountTask, self).check_usn_item_mapping()

    def post_process(self):
        return super(CycleCountTask, self).post_process()

    def get_available_shelf_quantity(self):
        return super(CycleCountTask, self).get_available_shelf_quantity()

    def get_cycle_count_shelves(self):
        result = []
        exception = []
        try:
            task_id = Task.objects.get(id=self.task_id)
            cycle_counts = CycleCount.objects.filter(task_id=task_id).all()
            for cycle_count in cycle_counts:
                shelves = list(CycleCountShelves.objects.filter(cycle_count_shelf=cycle_count,
                                                                shelf__bay__stock_point_id=self.stock_point_id,
                                                                status=CycleCountShelves.Status.NOT_STARTED).
                               values('shelf__id', 'shelf__name', 'picker__picker__username',
                                      'shelf__bay__stock_point__name', 'shelf__bay_id', 'shelf__bay__name'))
                for shelf in shelves:
                    stock_point_exist, stock_point = is_stock_point_exist(shelf['shelf__bay__stock_point__name'],
                                                                          result)
                    if not stock_point_exist:
                        result.append({'name': shelf['shelf__bay__stock_point__name'], 'type': 'ST',
                                       'children': [
                                           {'name': shelf['shelf__bay__name'], 'type': 'BAY',
                                            'id': shelf['shelf__bay_id'],
                                            'children': [
                                                {"name": shelf['shelf__name'], 'type': "SH",
                                                 'id': shelf['shelf__id'],
                                                 }]}]})
                    else:
                        exist_bay, bay = is_bay_exist(shelf['shelf__bay__name'], stock_point['children'])
                        if not exist_bay:
                            stock_point['children'].append(
                                {'name': shelf['shelf__bay__name'], 'type': 'BAY', 'id': shelf['shelf__bay_id'],
                                 'children': [
                                     {"name": shelf['shelf__name'], 'type': "SH", 'id': shelf['shelf__id'],
                                      }]}
                            )
                        else:
                            exists, shelfs = is_shelf_exists(shelf['shelf__name'], bay['children'])
                            if not exists:
                                bay['children'].append(
                                    {"name": shelf['shelf__name'], 'type': "SH", 'id': shelf['shelf__id'],
                                     })
            return result, exception
        except Task.DoesNotExist:
            self.exception_list.append(f'Task {self.task_id} Does not exist ')

        except Exception as e:
            self.exception_list.append(f'Problem in retrieving Task id {str(e)}')

    def create_task(self):
        self.task_type = Task.Type.PUT_AWAY
        self.operation_type = 'CYCLE_COUNT'
        super(PutAway, self).create_task()


def is_bay_exist(actual_bay, bays):
    for bay in bays:
        if bay['name'] == actual_bay:
            return True, bay
    return False, None


def is_shelf_exists(actual_shelf, shelf_s):
    for shelf in shelf_s:
        if shelf['name'] == actual_shelf:
            return True, shelf
    return False, None


def is_stock_point_exist(actual_stock_node, stock_nodes):
    for stock_point in stock_nodes:
        if stock_point['name'] == actual_stock_node:
            return True, stock_point
    return False, None


def is_item_exist_in_shelf(shelf_s, actual_sku_code):
    for item in shelf_s:
        if item['sku_code'] == actual_sku_code:
            return True, item
    return False, None


def get_attributes_for_item(attributes):
    final_dict = {'size': '', 'shade': '', 'design': '', 'description': ''}
    for attribute in attributes:
        if attribute['key'] in final_dict:
            final_dict[attribute['key']] = attribute['value']
    return final_dict


# @silk_profile('new func')

def get_stock_by_raw_query(self, barcode_in_sql_format):
    excluded_shelfs = set()
    for shelf_obj in Shelf.VirtualShelfTypes:
        if shelf_obj.name != 'STAGING_AREA': # Commenting as perclient advice they are not picking from Staging area
            excluded_shelfs.add(int(shelf_obj.value))
    excluded_shelfs = tuple(excluded_shelfs)
    with connection.cursor() as cursor:
        cursor.execute("drop table if exists TaskIds")
        cursor.execute("drop table if exists Barcodes")
        cursor.execute("drop table if exists CustomStocks")
        cursor.execute("drop table if exists PickedItemData")
        cursor.execute("create TEMP table Barcodes (sku_code char(50))")
        cursor.execute("create TEMP table TaskIds (id char(50))")
        cursor.execute(
            "create TEMP table CustomStocks (sku_id int, sku_code char(50),shelf_id int,original_stock int,reserved_stock int, task_ids int[] ) ")
        cursor.execute("create TEMP table PickedItemData (sku_code char(50),shelf_id int,picked_quantity int ) ")
        if len(barcode_in_sql_format):
            cursor.execute(f"insert into Barcodes values {barcode_in_sql_format}")
        cursor.execute(f"insert into CustomStocks \
                                SELECT mt.id,mt.sku_code, \
             ish.id AS shelf_id_from_stock, \
             coalesce(Sum(ist.quantity),0) AS current_stock, \
             subquery.reserved_quantity , \
             subquery.task_ids\
            FROM inventory_stock AS ist \
            INNER JOIN master_item AS mt ON mt.id=ist.item_id \
            INNER JOIN Barcodes AS ba ON ba.sku_code=mt.sku_code \
            INNER JOIN inventory_shelf AS ish ON ish.id=ist.shelf_id \
            INNER JOIN inventory_bay AS iba ON iba.id=ish.bay_id \
            LEFT JOIN \
             (SELECT mt.sku_code AS reserved_sku_code, \
             ish.id AS reserved_shelf_id, \
             Coalesce(SUM(sr.quantity), 0) AS reserved_quantity, \
             ARRAY_AGG(DISTINCT (dt.id)::int) FILTER ( \
             WHERE (dt.id IS NOT NULL)) AS task_ids \
             FROM dispatch_stockreservation AS sr \
             INNER JOIN dispatch_task AS dt ON dt.id=sr.task_id \
             INNER JOIN inventory_shelf AS ish ON ish.id=sr.shelf_id \
             INNER JOIN master_item AS mt ON mt.id=sr.item_id \
             WHERE dt.valid=TRUE \
             GROUP BY reserved_sku_code, \
             reserved_shelf_id) AS subquery ON subquery.reserved_sku_code=mt.sku_code \
            AND subquery.reserved_shelf_id=ish.id \
            LEFT JOIN inventory_virtualshelf AS ivsh ON (ish.id = ivsh.shelf_ptr_id) \
                    where \
                    iba.stock_point_id = {self.stock_point_id} \
                                         and NOT(ish.is_picking_blocked) AND NOT(ivsh.type IS NOT NULL and  ivsh.type IN {excluded_shelfs}) \
                            GROUP BY mt.id,mt.sku_code,subquery.reserved_sku_code,shelf_id_from_stock,subquery.reserved_shelf_id,subquery.task_ids,subquery.reserved_quantity")
        cursor.execute('select * from CustomStocks')
        reserved_result = cursor.fetchall()
        temp_list = set()
        for stock in reserved_result:
            if stock[5] is not None:
                temp_list.update([f"('{task_id}')" for task_id in stock[5]])
        task_ids = ','.join(temp_list)
        if len(task_ids):
            cursor.execute(f"insert into TaskIds values {task_ids}")
        cursor.execute(
            f"insert into PickedItemData "
            f"SELECT isi.item AS sku_code,iss.id AS shelf_id , COALESCE(SUM(isi.quantity), 0.0) AS picked_quantity "
            f"FROM inventory_stockmovementinward as isi "
            f"inner join Barcodes as sd on sd.sku_code =isi.item "
            f"inner join TaskIds as td on td.id =isi.associated_transaction_id "
            f"inner join inventory_shelf as iss on isi.to_shelf_id = iss.id "
            f"WHERE isi.adjustment_type = 'PICKING' GROUP BY isi.item,iss.id")

        cursor.execute(
            "select cs.sku_id,cs.sku_code,cs.shelf_id,cs.original_stock,coalesce(cs.reserved_stock,0),coalesce(pi.picked_quantity,0) from CustomStocks as cs "
            "left join PickedItemData as pi on pi.sku_code=cs.sku_code and pi.shelf_id=cs.shelf_id "
            "where cs.original_stock>0  order by cs.sku_code ,cs.original_stock desc")

        picked_result = cursor.fetchall()

        return picked_result
