import datetime
from datetime import datetime as dt
from dispatch.email import wip_bin_consolidation_email, not_found_moved_usns_alert
from dispatch.models import Task, StockReservation
from inventory.models import SerialNumber, Shelf


def get_wip_stock_node(node_type):
    today = datetime.date.today()
    wip_node_usns = list(SerialNumber.objects.filter(shelf__virtualshelf__type=node_type).values('updated_at', 'hash',
                                                                                        "item__sku_code"))
    current_wip_list = list()
    for usn in wip_node_usns:
        db_date = usn['updated_at']
        updated_date = datetime.datetime.strftime(db_date, '%Y-%m-%d')
        usn_date = dt.strptime(updated_date, '%Y-%m-%d').date()
        no_of_day = today - usn_date
        if node_type == Shelf.VirtualShelfTypes.WORK_IN_PROGRESS:
            day_limit = 3
        else:
            day_limit = 1
        if no_of_day.days >= day_limit:
            usn_barcode_list = list()
            usn_barcode_list.append(usn['hash'])
            usn_barcode_list.append(usn['item__sku_code'])
            usn_barcode_list.append(no_of_day.days)
            current_wip_list.append(usn_barcode_list)
    wip_bin_consolidation_email(current_wip_list, node_type)


def wip_bin_consolidation_cron():
    wip_name = Shelf.VirtualShelfTypes.WORK_IN_PROGRESS
    bc_name = 'BIN_CONSOLIDATION'
    get_wip_stock_node(wip_name)
    get_wip_stock_node(bc_name)


def picking_against_not_founds():
    today = datetime.date.today()
    yesterday = today - datetime.timedelta(days=1)
    task_list = list(Task.objects.filter(created_date__date=yesterday).values_list('id', flat=True).distinct())
    not_found_usns_list = list(StockReservation.objects.filter(task_id__in=task_list,
                                                               valid=True, not_found=True).values('item__sku_code',
                                                                                                  "usn", 'task', ))
    not_found_moved_usn = list()
    for barcode in not_found_usns_list:
        not_found_list = list()
        not_found_list.append(barcode['task'])
        not_found_list.append(barcode['item__sku_code'])
        not_found_list.append(barcode['usn'])
        not_found_moved_usn.append(not_found_list)
    if not_found_moved_usn:
        not_found_moved_usns_alert(not_found_moved_usn)
