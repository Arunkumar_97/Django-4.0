from django.contrib import admin
from django.db.models import IntegerField, Sum
from django.db.models.functions import Coalesce

from dispatch.models import *
from import_export.admin import ExportActionMixin
from import_export import resources
from import_export.fields import Field
from rangefilter.filters import DateRangeFilter, DateTimeRangeFilter


@admin.register(Task)
class TaskAdmin(ExportActionMixin, admin.ModelAdmin):

    def pickers(self, instance):
        return ','.join(instance.picker.values_list('picker__username', flat=True))

    def quantity(self, instance):
        return \
            instance.task_reserve.aggregate(total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))[
                'total_quantity']

    search_fields = ('id', 'associated_transaction', 'picker__picker__username',)
    list_display = (
        'id', 'type', 'operation', 'pickers', 'associated_transaction', 'quantity', 'valid', 'created_at',
        'assigned_time',
        'picked_time')
    list_filter = (('created_at', DateRangeFilter), 'valid', 'operation')


@admin.register(StockReservation)
class StockReservationAdmin(admin.ModelAdmin):

    def bluefish_pickslip(self, instance):
        return instance.task.associated_transaction

    search_fields = ('id','item__sku_code','task__id' ,'task__associated_transaction',)
    list_display = ('id',
        'task_id', 'bluefish_pickslip', 'valid', 'item', 'quantity', 'shelf', 'show_stock_point', 'show_usn',)
    list_filter = ('valid',)
    raw_id_fields = ('item',)

    def show_usn(self, obj):
        return "\n".join([usn.hash for usn in obj.usns.all()])

    def show_stock_point(self, obj):
        return obj.shelf.bay.stock_point.name

    show_usn.short_description = 'USN"S'
    show_stock_point.short_description = 'Stock Point'


@admin.register(PickerCheckIn)
class PickerCheckInAdmin(admin.ModelAdmin):
    list_display = ('picker', 'created_at',)


admin.site.register(StockSerial)
admin.site.register(Picker)


@admin.register(CycleCount)
class CycleCount(admin.ModelAdmin):
    list_display = ('task',)


@admin.register(CycleCountShelves)
class CycleCountShelves(admin.ModelAdmin):
    list_display = ('picker', 'shelf','status')

admin.site.register(AdhocPickConfig)
