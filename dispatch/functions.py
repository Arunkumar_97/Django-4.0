from django.db.models import Sum, F, Count, Q, IntegerField
from django.db.models.functions import Coalesce

from dispatch.models import *
from dispatch.interface.operation import is_item_exist_in_shelf, is_bay_exist, is_stock_point_exist, \
    get_attributes_for_item, is_shelf_exists
from inventory.models import Stock, SerialNumber, StockAdjustmentType, StockMovementInward
from master.models import Item


def get_task_quantity_details(task_id):
    context = dict()
    context["task_id"] = task_id
    stock_reservations = StockReservation.objects.filter(task_id=task_id)
    reserved_quantity = stock_reservations.aggregate(
        total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))['total_quantity']
    context["quantity_details"] = {
        "total_qty": int(reserved_quantity),
        "picked_qty": StockMovementInward.objects
        .filter(associated_transaction_id=task_id, adjustment_type=StockAdjustmentType.PICKING)
        .aggregate(total_count=Coalesce(Sum('quantity'), 0, output_field=IntegerField())),
        "notfound_qty": int(float(NotFoundAudit.objects
                                  .filter(associated_task_id=task_id)
                                  .aggregate(notfound_qty=Sum('quantity'))["notfound_qty"] or 0.0))
    }
    return context


def get_task_status_by_id(status_id):
    if status_id == '1':
        return 'Created'
    elif status_id == '2':
        return 'WIP'
    elif status_id == '3':
        return 'Completed'
    else:
        return 'no mapping found'


def generate_picker_image_dict(picker_id):
    picker = Picker.objects.get(id=picker_id)
    return {'name': picker.picker.username, 'image': picker.image.name,
            'initials': f'{picker.picker.first_name[0] if len(picker.picker.first_name) > 0 else ""}{picker.picker.last_name[0] if len(picker.picker.last_name) > 0 else ""}'}


def get_items(stock_point_id):
    result = []
    serials = list(SerialNumber.objects.filter(~Q(
        shelf__name__in=['dispatched shelf', 'WIP', 'NOT FOUND', 'LOST']),
                                               shelf__bay__stock_point_id=stock_point_id,
                                               ).order_by(
        'batch__created_at', 'item', 'item__sku_code', 'hash', 'shelf', 'shelf__bay__name',
        'shelf__bay__stock_point__name').values('shelf__name', 'hash',
                                                'item', 'item__image', 'item__sku_code',
                                                'shelf__bay__name', 'shelf__bay_id', 'shelf__id',
                                                'shelf__bay__stock_point__name'))
    for serial in serials:
        size_attributes = Item.objects.filter(sku_code=serial['item__sku_code']).annotate(
            key=F('attributes__key__name'), value=F('attributes__value')).values('key', 'value')

        final_data = get_attributes_for_item(size_attributes)

        stock_point_exist, stock_point = is_stock_point_exist(serial['shelf__bay__stock_point__name'],
                                                              result)
        if not stock_point_exist:
            result.append({'name': serial['shelf__bay__stock_point__name'], 'type': 'ST',
                           'children': [
                               {'name': serial['shelf__bay__name'], 'type': 'BAY',
                                'id': serial['shelf__bay_id'],
                                'children': [
                                    {"name": serial[''], 'type': "SH",
                                     'id': serial['shelf__id'],
                                     'data': [{'sku_code': serial['item__sku_coshelf__namede'],
                                               'stock': Stock.objects.get(shelf_id=serial['shelf__bay_id']).quantity,
                                               'size': final_data['size'], 'shade': final_data['shade'],
                                               'description': final_data['description'],
                                               'image': serial['item__image'],
                                               'design': final_data['design'],
                                               'usns': [serial['hash']]}]

                                     }]}]})
        else:
            exist_bay, bay = is_bay_exist(serial['shelf__bay__name'], stock_point['children'])
            if not exist_bay:
                stock_point['children'].append(
                    {'name': serial['shelf__bay__name'], 'type': 'BAY', 'id': serial['shelf__bay_id'],
                     'children': [
                         {"name": serial['shelf__name'], 'type': "SH", 'id': serial['shelf__id'],
                          'data': [{'sku_code': serial['item__sku_code'],
                                    'stock': Stock.objects.get(shelf_id=serial['shelf__bay_id']).quantity,
                                    'size': final_data['size'], 'shade': final_data['shade'],
                                    'image': serial['item__image'],
                                    'design': final_data['design'],
                                    'description': final_data['description'],
                                    'usns': [serial['hash']]}]
                          }]}
                )
            else:
                exists, shelf = is_shelf_exists(serial['shelf__name'], bay['children'])
                if not exists:
                    bay['children'].append(
                        {"name": serial['shelf__name'], 'type': "SH", 'id': serial['shelf__id'],
                         'data': [{'sku_code': serial['item__sku_code'],
                                   'stock': Stock.objects.get(shelf_id=serial['shelf__bay_id']).quantity,
                                   'size': final_data['size'],
                                   'image': serial['item__image'], 'shade': final_data['shade'],
                                   'design': final_data['design'],
                                   'description': final_data['description'],
                                   'usns': [serial['hash']]}]})
                else:
                    exists, item = is_item_exist_in_shelf(shelf['data'], serial['item__sku_code'])
                    if exists:
                        item['usns'].append(serial['hash'])
                    else:
                        shelf['data'].append({'sku_code': serial['item__sku_code'],
                                              'stock': Stock.objects.get(shelf_id=serial['shelf__bay_id']).quantity,
                                              'size': final_data['size'], 'shade': final_data['shade'],
                                              'design': final_data['design'],
                                              'image': serial['item__image'],
                                              'description': final_data['description'],
                                              'usns': [serial['hash']]})
    return result


def transform_serials_to_item_with_serials(usns):
    item_usn_list = {}
    for usn in usns:
        serial_obj = SerialNumber.objects.get(hash=usn)
        sku_code = serial_obj.item.sku_code
        if sku_code in item_usn_list:
            item_usn_list[sku_code].append(usn)
        else:
            item_usn_list[sku_code] = [usn]
    return [{'sku_code': sku_code, 'usns': usns, 'quantity': len(usns)} for sku_code, usns in item_usn_list.items()]


def print_mismatch():
    a = [{'task': '4891', 'total': 117.00},
         {'task': '4813', 'total': 25.00},
         {'task': '4913', 'total': 24.00},
         {'task': '4824', 'total': 14.00},
         {'task': '4843', 'total': 19.00},
         {'task': '4936', 'total': 115.00},
         {'task': '4796', 'total': 40.00},
         {'task': '4805', 'total': 16.00},
         {'task': '4944', 'total': 53.00},
         {'task': None, 'total': 144.00},
         {'task': '5003', 'total': 49.00},
         {'task': '4786', 'total': 15.00},
         {'task': '4837', 'total': 25.00},
         {'task': '4784', 'total': 32.00},
         {'task': '4797', 'total': 65.00},
         {'task': '4902', 'total': 28.00},
         {'task': '4864', 'total': 22.00},
         {'task': '4932', 'total': 23.00},
         {'task': '4809', 'total': 63.00},
         {'task': '4973', 'total': 26.00},
         {'task': '4939', 'total': 22.00},
         {'task': '4826', 'total': 33.00},
         {'task': '4879', 'total': 12.00},
         {'task': '4853', 'total': 35.00},
         {'task': '4782', 'total': 85.00},
         {'task': '4971', 'total': 64.00},
         {'task': '4790', 'total': 11.00},
         {'task': '5005', 'total': 20.00},
         {'task': '4975', 'total': 45.00},
         {'task': '4800', 'total': 29.00},
         {'task': '4881', 'total': 10.00},
         {'task': '4793', 'total': 97.00},
         {'task': '4778', 'total': 193.00},
         {'task': '4867', 'total': 16.00},
         {'task': '4823', 'total': 34.00},
         {'task': '4929', 'total': 17.00},
         {'task': '4896', 'total': 42.00},
         {'task': '4897', 'total': 27.00},
         {'task': '4799', 'total': 30.00},
         {'task': '4811', 'total': 38.00},
         {'task': '4806', 'total': 31.00},
         {'task': '4934', 'total': 14.00},
         {'task': '4776', 'total': 112.00},
         {'task': '4870', 'total': 40.00},
         {'task': '4852', 'total': 17.00},
         {'task': '4919', 'total': 24.00},
         {'task': '4803', 'total': 36.00},
         {'task': '4785', 'total': 25.00},
         {'task': '4855', 'total': 20.00},
         {'task': '4916', 'total': 34.00},
         {'task': '4801', 'total': 23.00},
         {'task': None, 'total': 36.00},
         {'task': '4817', 'total': 41.00},
         {'task': '4995', 'total': 37.00},
         {'task': None, 'total': 80.00},
         {'task': None, 'total': 96.00},
         {'task': '4965', 'total': 47.00},
         {'task': '4977', 'total': 25.00},
         {'task': '4941', 'total': 36.00},
         {'task': '4777', 'total': 86.00},
         {'task': '4858', 'total': 20.00},
         {'task': '4956', 'total': 16.00},
         {'task': '4871', 'total': 12.00},
         {'task': '4833', 'total': 88.00},
         {'task': '4779', 'total': 62.00},
         {'task': '4898', 'total': 21.00},
         {'task': '4821', 'total': 31.00},
         {'task': '5010', 'total': 31.00},
         {'task': '4927', 'total': 27.00},
         {'task': '4828', 'total': 84.00},
         {'task': '4874', 'total': 26.00},
         {'task': '4910', 'total': 36.00},
         {'task': '4953', 'total': 11.00},
         {'task': None, 'total': 128.00},
         {'task': '5001', 'total': 80.00},
         {'task': '5016', 'total': 20.00},
         {'task': '4886', 'total': 41.00},
         {'task': '5021', 'total': 30.00},
         {'task': '4893', 'total': 63.00},
         {'task': '4831', 'total': 49.00},
         {'task': '4863', 'total': 14.00},
         {'task': '4990', 'total': 32.00},
         {'task': '4798', 'total': 26.00},
         {'task': '4845', 'total': 108.00},
         {'task': '4780', 'total': 126.00},
         {'task': '4912', 'total': 29.00},
         {'task': '4903', 'total': 40.00},
         {'task': '4804', 'total': 31.00},
         {'task': '4848', 'total': 40.00},
         {'task': '4816', 'total': 69.00},
         {'task': '4819', 'total': 21.00},
         {'task': None, 'total': 288.00},
         {'task': '4842', 'total': 20.00},
         {'task': '4961', 'total': 39.00},
         {'task': '4908', 'total': 39.00},
         {'task': '4788', 'total': 10.00},
         {'task': '4866', 'total': 15.00},
         {'task': '4963', 'total': 171.00},
         {'task': '4829', 'total': 39.00},
         {'task': '4783', 'total': 100.00},
         {'task': '4835', 'total': 21.00},
         {'task': '4905', 'total': 35.00},
         {'task': '4836', 'total': 32.00},
         {'task': '4884', 'total': 61.00},
         {'task': '4781', 'total': 55.00},
         {'task': '4958', 'total': 14.00},
         {'task': '4980', 'total': 30.00},
         {'task': '4983', 'total': 36.00},
         {'task': '4794', 'total': 43.00},
         {'task': '4834', 'total': 65.00},
         {'task': '4787', 'total': 39.00},
         {'task': '4807', 'total': 29.00},
         {'task': '4815', 'total': 105.00},
         {'task': '5023', 'total': 18.00},
         {'task': '4876', 'total': 13.00},
         {'task': '4791', 'total': 22.00},
         {'task': '4969', 'total': 41.00},
         {'task': '4966', 'total': 106.00},
         {'task': '4999', 'total': 20.00},
         {'task': '4795', 'total': 20.00},
         {'task': '4996', 'total': 34.00},
         {'task': '4789', 'total': 56.00}]
    for details in a:
        if details['task'] != None:
            task_quantity = StockReservation.objects.filter(task__id=details['task']).aggregate(
                total=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))

            if task_quantity != int(details['total']):
                print(details['task'])


def assign_picker_shelf(picker, cycle_count):
    for picker_id, shelves in picker.items():
        picker_obj = Picker.objects.get(id=picker_id)
        for shelf in shelves:
            CycleCountShelves.objects.create(shelf_id=shelf, cycle_count_shelf=cycle_count,
                                             picker=picker_obj)


def transform_serial_dict(shelf_id, quantity, sku_code):
    item_usn_list = {}
    usns = list(SerialNumber.objects.filter(shelf_id=shelf_id, item__sku_code=sku_code).values_list('hash', flat=True))
    for usn in usns:
        serial_obj = SerialNumber.objects.get(hash=usn)
        sku_code = serial_obj.item.sku_code
        if sku_code in item_usn_list:
            item_usn_list[sku_code].append(usn)
        else:
            item_usn_list[sku_code] = [usn]
    return [{'sku_code': sku_code, 'usns': usns, 'quantity': quantity} for sku_code, usns in item_usn_list.items()]


