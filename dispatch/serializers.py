from rest_framework import serializers

from dispatch.models import Picker


class PickerSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='picker')

    class Meta:
        model = Picker
        fields = ['id', 'name']