from django.urls import path

from . import views

urlpatterns = [
    path('allocation/', views.AllocationAPI.as_view()),
    path('picking/', views.ScanUSN.as_view()),
    path('allocate-picker/', views.AllocatePicker.as_view()),
    path('picker-checkin/', views.PickerCheckInApi.as_view()),
    path('create-picker/', views.CreatePicker.as_view()),
    path('create-task/', views.CreateTask.as_view()),
    path('task-list/', views.TaskList.as_view()),
    path('task-details/<int:task_id>/', views.GetQuantityDetailsForTask.as_view()),
    path('task-info/', views.TaskInfo.as_view()),
    path('create-task-v2/', views.CreateTaskWithoutReservation.as_view()),
    path('submit-putaway/', views.PutAwaySubmit.as_view()),
    path('putaway-list/', views.PutAwayList.as_view()),
    path('shelf/details/', views.GetShelfQuantityDetails.as_view()),
    path('item-details/', views.TaskDetail.as_view()),
    path('task-progress/', views.Taskprogress.as_view()),
    path('adhoc-picking/', views.AdhocPicking.as_view()),
    path('revert-pickslip/', views.RevertPickSlip.as_view()),
    path('putaway-app/', views.PutAwayApp.as_view()),
    path('picking-app/', views.PickingAPP.as_view()),
    path('not-found/', views.NotFoundItem.as_view()),
    path('cycle-count/', views.CreateCycleCountTask.as_view()),
    path('cycle-count/<int:task_id>/stock_point/<int:stock_point_id>/shelves/', views.CycleCountShelf.as_view()),
    path('cycle-count/<int:task_id>/<int:shelf_id>/', views.SubmitCycleCount.as_view()),
    path('cycle-count/<int:task_id>/', views.CycleCountDetails.as_view()),
    path('returns/', views.Returns.as_view()),
    path('picker/', views.PickerApi.as_view()),
    # path('cycle-count/scheduler/', views.SchedulerTask.as_view()),
    # path('cycle-count/scheduler-list/',views.SchedulerList.as_view())

]
