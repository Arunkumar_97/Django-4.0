from django.core.files.storage import FileSystemStorage
from django.utils.translation import gettext_lazy as _
from django.db import models
from django.db.models import Count, Q, Sum, IntegerField, UniqueConstraint, DecimalField
from django.db.models.functions import Coalesce
from polymorphic.models import PolymorphicModel
from WMS import settings
from core.models import Audit, CustomUser, UserType
from core.utils.model_utils import transform_date_query
from inventory.models import SerialNumber, Warehouse, Shelf, StockMovementInward, StockAdjustmentType, \
    StockMovementOutward
from master.models import Item
from django.contrib.contenttypes.models import ContentType


class Picker(models.Model):
    picker = models.ForeignKey(CustomUser, on_delete=models.CASCADE, unique=True)
    valid = models.BooleanField(default=True)
    image = models.ImageField(null=True, storage=FileSystemStorage(location=settings.MEDIA_ROOT + '/picker/image/'))

    def __str__(self):
        return self.picker.username


class PickerCheckIn(models.Model):
    picker = models.ForeignKey(Picker, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        constraints = [
            UniqueConstraint(fields=['picker', 'created_at'], name='unique_picker_created'), ]


class Task(models.Model):
    class Operation(models.TextChoices):
        PICK_SLIP = 'PS', 'Pick Slip'
        INWARD = 'IN', 'INWARD'
        CYCLE_COUNT = 'CC', 'Cycle Count'
        BIN_CONSOLIDATION = 'BC', 'Bin Consolidation'
        REPLACEMENT_TASK = 'RT', 'REPLACEMENT TASK'

    class Type(models.TextChoices):
        PICKING = 'PC', 'Picking'
        PUT_AWAY = 'PA', 'Put Away'
        CYCLE_COUNT = 'CC', 'Cycle Count'

    type = models.CharField(max_length=2, choices=Type.choices)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    request = models.JSONField(null=True, blank=True)
    remarks = models.TextField()
    operation = models.CharField(max_length=2, choices=Operation.choices)
    associated_transaction = models.CharField(max_length=15, blank=False, null=False)
    valid = models.BooleanField(default=True)
    picker = models.ManyToManyField(Picker)
    picked_time = models.DateTimeField(default=None, blank=True, null=True)
    item_details = models.JSONField(null=True, blank=True)  # [{sku_code:MW123,quantity:50,usns:['awse','hjhjhj']}]
    assigned_time = models.DateTimeField(default=None, blank=True, null=True)

    @property
    def is_reservation_completed(self):
        obj = self.task_reserve.aggregate(total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))[
            'total_quantity']
        picked = StockMovementInward.objects.filter(associated_transaction_id=self.id
                                                    ,
                                                    adjustment_type=StockAdjustmentType.PICKING).aggregate(
            total_count=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))
        return obj == picked['total_count']

    def get_current_status(self):
        obj = self.task_reserve.aggregate(total_quantity=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))[
            'total_quantity']

        picked = StockMovementInward.objects.filter(associated_transaction_id=self.id
                                                    ,
                                                    adjustment_type=StockAdjustmentType.PICKING).aggregate(
            total_count=Coalesce(Sum('quantity'), 0, output_field=IntegerField()))
        if picked['total_count'] > 0 and picked['total_count'] != obj:
            return 'WIP'
        elif picked['total_count'] == obj:
            return 'Completed'
        else:
            return 'Created'

    @staticmethod
    def apply_filters(request, filter_params):
        if request.GET.get('start_date'):
            filter_params['created_at__range'] = transform_date_query(request)
            filter_params['valid'] = True
        if request.user.is_superuser:
            pass
        elif request.user.user_type == UserType.PICKER:
            filter_params['picker__picker__id'] = request.user.id
        return filter_params


class PutAwayReservation(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='task_items', blank=True, null=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    usns = models.ManyToManyField(SerialNumber, through='PutAwaySerial')
    quantity = models.DecimalField(decimal_places=2, max_digits=12, default=0)


class PutAwaySerial(models.Model):
    usn = models.ForeignKey(SerialNumber, on_delete=models.CASCADE)
    putaway_reservation = models.ForeignKey(PutAwayReservation, on_delete=models.CASCADE)
    picker = models.ForeignKey(Picker, on_delete=models.CASCADE)


class StockReservation(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='task_reserve', blank=True, null=True)
    valid = models.BooleanField(default=True, blank=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    usns = models.ManyToManyField(SerialNumber, through='StockSerial')
    quantity = models.DecimalField(decimal_places=2, max_digits=12, default=0)
    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE)
    not_found = models.BooleanField(default=False, null=False)

    @property
    def get_pending_usn_count(self):
        return self.quantity - len(self.usns.all())


class StockSerial(models.Model):
    usn = models.ForeignKey(SerialNumber, on_delete=models.CASCADE, related_name='stock_serial_usn')
    stock_reservation = models.ForeignKey(StockReservation, on_delete=models.CASCADE)
    picker = models.ForeignKey(Picker, on_delete=models.CASCADE)
    picked_time = models.DateTimeField(auto_created=True, auto_now_add=True)


class NotFoundAudit(models.Model):
    picker = models.ForeignKey(Picker, on_delete=models.CASCADE)
    associated_task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=True, null=True)
    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name="actual_shelf")
    created_at = models.DateTimeField(auto_created=True, auto_now_add=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="audit_item")
    quantity = models.PositiveIntegerField(null=False, blank=False)


class AdhocPickConfig(models.Model):
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    picker = models.ManyToManyField(Picker)


class CycleCount(models.Model):
    task = models.OneToOneField(Task, on_delete=models.CASCADE, related_name='task_cycle_count', blank=True, null=True)
    shelves = models.ManyToManyField(Shelf, through='CycleCountShelves')

    def __str__(self):
        return "{} ({})".format(self.id, self.task.id)

    def get_cycle_count_status(self):
        total_shelves = self.shelves.aggregate(total=Count('cyclecountshelves__id'))['total']
        completed_shelves = self.shelves.filter(cyclecountshelves__status=CycleCountShelves.Status.COMPLETED).aggregate(
            total=Count('cyclecountshelves__id'))['total']
        if completed_shelves == total_shelves:
            return 'Completed'
        elif completed_shelves >= 1:
            return 'WIP'
        else:
            return 'Created'


class CycleCountShelves(models.Model):
    class Status(models.TextChoices):
        COMPLETED = 'C', 'Completed',
        WORK_IN_PROGRESS = 'W', 'Work in Progress',
        NOT_STARTED = 'N', 'Not Started',

    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE)
    cycle_count_shelf = models.ForeignKey(CycleCount, on_delete=models.CASCADE, related_name="cycle_count_shelves")
    picker = models.ForeignKey(Picker, on_delete=models.CASCADE)
    status = models.CharField(max_length=2, choices=Status.choices, default=Status.NOT_STARTED)


class CycleCountLines(PolymorphicModel):
    cycle_count = models.ForeignKey(CycleCountShelves, on_delete=models.CASCADE, null=False, related_name="cycle_lines")
    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, null=False)
    usn = models.ForeignKey(SerialNumber, on_delete=models.CASCADE, null=False, related_name="cycle_lines_usn")
    created_at = models.DateTimeField(auto_created=True, auto_now_add=True)

    def save(self, *args, **kwargs):
        super(CycleCountLines, self).save(*args, **kwargs)


class ExactCycleCountLine(CycleCountLines):
    pass


class MisPlacedCycleCountLine(CycleCountLines):
    picker_anomaly = models.ForeignKey(Picker, on_delete=models.CASCADE)


class NotFoundCycleCountLine(CycleCountLines):
    pass


class ExcessCycleCountLine(CycleCountLines):
    previous_shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, related_name="excess_outward")


class BinConsolidation(models.Model):
    outward_task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='consolidation_inward_task',
                                     blank=True,
                                     null=True)
    inward_task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='consolidation_outward_task',
                                    blank=True,
                                    null=True)


class BcLines(models.Model):
    bin_consolidation = models.ForeignKey(BinConsolidation, on_delete=models.CASCADE, null=False,
                                          related_name="consolidation_lines")
    shelf = models.ForeignKey(Shelf, on_delete=models.CASCADE, null=False)
    serials = models.ManyToManyField(SerialNumber)


class TaskProgress(models.Model):
    created_at = models.DateTimeField(default=None, blank=True, null=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, null=False)
    picker = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
    completed_at = models.DateTimeField(default=None, blank=True, null=True)
