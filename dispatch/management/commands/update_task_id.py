from WMS.settings import client_machine_url
from dispatch.models import Task
from inventory.models import SerialNumber
from master.models import Item
from utils.excel.extraction import  excel_extraction
import datetime
import json
import requests
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Update Task ID in Bluefish'

    def handle(self, *args, **options):
        tasks = Task.objects.values()
        taskvspickslipid = dict()
        for task in tasks:
            test_string = task['remarks']
            split_string = 'Task-'
            res = test_string.split(split_string, 1)
            task_id = res[1]
            taskvspickslipid[task['id']] = task_id
        final_json_data = json.dumps(taskvspickslipid)
        client_machine_stock_sync = requests.post(f'{client_machine_url}/api/dispatch/update-task-id/',
                                                  headers={'content-type': 'application/json',
                                                           'Authorization': "Token 75eecfd544b74a400044a427641734bfa605ad25"},
                                                  json=final_json_data)
        content = json.loads(client_machine_stock_sync.content.decode('utf-8'))
        if client_machine_stock_sync.status_code == 200 and not content['success']:
            print(content['data'])
        else:
            pass
        print('Script Completed Task ID updated in Bluefish')
