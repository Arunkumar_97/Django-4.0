import datetime

from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from io import BytesIO
import openpyxl

from inventory.models import Shelf


def create_excel_attachment(head, body, file_name):
    excel = BytesIO()
    book = openpyxl.Workbook(excel)
    sheet = book.create_sheet('sheet')
    sheet.append(head)
    for row in body:
        sheet.append(row)
    book.save(excel)
    excel_attachment = {'name': "{}.xlsx".format(file_name), 'content': excel.getvalue(),
                        'content_type': 'application/vnd.ms-excel'}
    return excel_attachment


def wip_bin_consolidation_email(wip_list, stock_node):
    emails = list()
    connection = mail.get_connection()
    connection.open()
    to = ['ayyanar.m@ottoclothing.in']
    if stock_node == Shelf.VirtualShelfTypes.WORK_IN_PROGRESS:
        plain_message = strip_tags(
            '<h1>"Dear Sir,Here Attached USNs are more than 3 or more days Work In Progress."</h1>')
        message = EmailMultiAlternatives(f'Three or More in days WIP', plain_message, from_email=None, to=to)

    else:
        plain_message = strip_tags(
            '<h1>"Dear Sir,Here Attached USNs are more than 1 or more days BIN CONSOLIDATION."</h1>')
        message = EmailMultiAlternatives(f'One or More days in BIN CONSOLIDATION', plain_message, from_email=None,
                                         to=to)

    message.attach_alternative(plain_message, 'text/html')
    head = ["USN", "SkuCode", "No Of Days"]
    attachment = create_excel_attachment(head, wip_list,
                                         'USN_DETAILS({})'.format(datetime.date.today()))
    message.attach(attachment['name'], attachment['content'], attachment['content_type'])
    emails.append(message)
    connection.send_messages(emails)
    connection.close()


def not_found_moved_usns_alert(usn_list):
    emails = list()
    connection = mail.get_connection()
    connection.open()
    to = ["ayyanar.m@ottoclothing.in"]
    plain_message = strip_tags('<h1>"Dear Sir,The Following USNs are moved to NOT FOUND Yesterday."</h1>')
    message = EmailMultiAlternatives(f'NOT_FOUND MOVED USNS', plain_message, from_email=None, to=to)
    message.attach_alternative(plain_message, 'text/html')
    head = ["Task Id", 'SKUCODE', 'USN']
    attachment = create_excel_attachment(head, usn_list, 'not_found_moved')
    message.attach(attachment['name'], attachment['content'], attachment['content_type'])
    emails.append(message)
    connection.send_messages(emails)
    connection.close()
